import h5py
import time
import sys
import numpy as np
import maskLib

if __name__ == '__main__':
    start_time = time.time()
    print('Gatherer 48 files together for AGIPD-1M ')
    ########################## get the inputs from slurm ########################    
    argcnt = 0
    argcnt += 1
    scratchFolder = sys.argv[argcnt]
    argcnt += 1
    numModules = int(sys.argv[argcnt])
    argcnt += 1
    numMemCells = int(sys.argv[argcnt])
    argcnt += 1
    moduleRowPixN = int(sys.argv[argcnt])
    argcnt += 1
    moduleClmPixN = int(sys.argv[argcnt])
    argcnt += 1
    waterRun = int(sys.argv[argcnt])
    argcnt += 1
    maskFile = sys.argv[argcnt]
    
    ########################## gather 48 files for AGIPD three dark runs into one file ########################    
    #hotFlickMap, coldFlickMap  
    waterBasedMask = np.zeros((numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='uint8')
    maxBackMeanMap = np.zeros((numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='float32')
    analogMax_HighGain = np.zeros((numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='float32')
    #lineParam_Var_Mean = np.zeros((2, numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='float32')
    for moduleCnt in range(numModules):
        moduleCntStr = "%02d" % moduleCnt
        print('module ' +str(moduleCnt) + ' run ' +str(waterRun), flush=True)
        runNumberStr = "%04d" % waterRun
        readDIR = scratchFolder + '/r' + runNumberStr + '/'
        ReadFileName = readDIR + 'waterMaps-AGIPD' + moduleCntStr + '.h5'
        fileh5 = h5py.File(ReadFileName,'r')
        waterBasedMask[:, moduleCnt, :, :] = fileh5['waterBasedMask']
        maxBackMeanMap[:, moduleCnt, :, :] = fileh5['maxBackMeanMap']
        analogMax_HighGain[:, moduleCnt, :, :] = fileh5['analogMax_HighGain']
        #lineParam_Var_Mean[:, :, moduleCnt, :, :] = fileh5['lineParam_Var_Mean']
        fileh5.close()

    maxBackMeanMap[...] = maxBackMeanMap.max()
        
    print('Preparing mask...')
    maskFileh5 = h5py.File(maskFile,'a')
    badPixelMask_h5 =  maskFileh5['badPixelMask']
    badPixelMask_ByWater = badPixelMask_h5[...].copy()
    BadpixelMask_OnDa_h5 = maskFileh5['BadpixelMask_OnDa']
    BadpixelMaskByWater_OnDa = BadpixelMask_OnDa_h5[...].copy()
    
    waterBasedMask[waterBasedMask>0] += 18
    
    #badPixelMask_ByWater is just the dark one by now
    #only mask those that we found new by water and are not masked before water.
    for segCnt in range(3):
        badPixelMask_ByWater[segCnt, (badPixelMask_ByWater[segCnt]==0) & (waterBasedMask>0)] = \
                waterBasedMask[(badPixelMask_ByWater[segCnt]==0) & (waterBasedMask>0)]
    ####################### just for OnDa ####################
    print('Creating bad pixel mask for OnDa using worst of all cells...', flush=True)
    BadpixelcellsCombined = badPixelMask_ByWater.copy()
    BadpixelcellsCombined[BadpixelcellsCombined>0]=1
    BadpixelcellsCombined = 1 - BadpixelcellsCombined
    BadpixelcellsCombined = BadpixelcellsCombined.min(0).min(0)
    BadpixelMask_OnDa_New = np.zeros((numModules*moduleRowPixN, moduleClmPixN), dtype='uint8')
    for moduleCnt in range(numModules):
        BadpixelMask_OnDa_New[moduleCnt*512:(moduleCnt+1)*512,:] = \
                    np.squeeze(BadpixelcellsCombined[moduleCnt, :, :])
    BadpixelMask_OnDa_New = maskLib.badPixelMaskForASICRules(BadpixelMask_OnDa_New, (16, 16), rule='OnDa_DEADASIC')
    BadpixelMaskByWater_OnDa[BadpixelMask_OnDa_New==0] = 0
    print('In total, for OnDa: ' + str(int(10000*(1-BadpixelMaskByWater_OnDa.mean()))/100) + '% is masked')
    
    if ("badPixelMask_ByWater" not in maskFileh5):
        maskFileh5.create_dataset('badPixelMask_ByWater', data = badPixelMask_ByWater)
    else:
        _badPixelMask_ByWater = maskFileh5['badPixelMask_ByWater']
        _badPixelMask_ByWater[...] = badPixelMask_ByWater    
    if ("BadpixelMaskByWater_OnDa" not in maskFileh5):
        maskFileh5.create_dataset('BadpixelMaskByWater_OnDa', data = BadpixelMaskByWater_OnDa)
    else:
        _BadpixelMaskByWater_OnDa = maskFileh5['BadpixelMaskByWater_OnDa']
        _BadpixelMaskByWater_OnDa[...] = BadpixelMaskByWater_OnDa    
    if ("maxBackMeanMap" not in maskFileh5):
        maskFileh5.create_dataset('maxBackMeanMap', data = maxBackMeanMap)
    else:
        _maxBackMeanMap = maskFileh5['maxBackMeanMap']
        _maxBackMeanMap[...] = maxBackMeanMap    
    if ("analogMax_HighGain" not in maskFileh5):
        maskFileh5.create_dataset('analogMax_HighGain', data = analogMax_HighGain)
    else:
        _analogMax_HighGain = maskFileh5['analogMax_HighGain']
        _analogMax_HighGain[...] = analogMax_HighGain    

    maskFileh5.close()
        
    print("--- Finishing in %s seconds ---" % (time.time() - start_time))
    print("--- Finishing at %s ---" % time.time())
    exit()