import numpy as np
from multiprocessing import Process, Queue, cpu_count

import matplotlib.pyplot as plt
import cfelpyutils.crystfel_utils as cfelCryst
import cfelpyutils.geometry_utils as cfelGeom

import RGFLib.RobustGausFitLibPy as RGFLib
from textProgBar import textProgBar

def connCompLabl_multiprocFunc(aQ, _procID, mask, minimumIslandSize):
    mask = RGFLib.islandRemovalPy(mask, minimumIslandSize)
    aQ.put(list([_procID, mask]))
    
def connCompLabeling_multiproc(maskAll, minimumIslandSize):

    numMemCells_numModules, rows, col = maskAll.shape

    myCPUCount = cpu_count()-1
    aQ = Queue()
    numBusyCores = 0
    numProc = numMemCells_numModules
    numWiating = numMemCells_numModules
    numDone = 0
    
    procID = 0

    allLabels = np.zeros(maskAll.shape, maskAll.dtype)
    print('starting ' +str(numProc) + ' processes with ' + str(myCPUCount) + ' CPUs')
    
    while(numDone<numProc):
        if (not aQ.empty()):
            aQElement = aQ.get()
            Q_procID = aQElement[0]
            Q_result = aQElement[1]
            allLabels[Q_procID] = Q_result
            numDone += 1
            numBusyCores -= 1
            if(numDone==1):
                pBar = textProgBar(numProc-1, 75, title = 'Connected components labelling to remove islands')
            if(numDone>1):
                pBar.go()
            if(numBusyCores>0):
                continue

        if((numWiating>0) & (numBusyCores < myCPUCount)):
            inData = maskAll[procID, :, :]
            Process(target = connCompLabl_multiprocFunc,
                    args = (aQ, procID, inData, minimumIslandSize)).start()
            procID += 1
            numWiating -= 1
            numBusyCores += 1
            
    del pBar
    return (allLabels)
        
    
def maskByBoarders(numMemCells, numModules, moduleRowPixN, moduleClmPixN, ASICShape):    
    
    ASICMask = np.zeros(ASICShape, dtype='ubyte')
    ASICMask[0:, 0] = 1
    ASICMask[0:, -1] = 1
    ASICMask[0, 0:] = 1
    ASICMask[-1, 0:] = 1
    
    ASICMask[1, 0:] = 1
    ASICMask[-2, 0:] = 1
    
    ASICMask = np.tile(ASICMask, (numMemCells, numModules, int(moduleRowPixN/ASICShape[0]), int(moduleClmPixN/ASICShape[1])))
    
    return ASICMask

def badPixelMaskForASICRules(_mask, ruleParameter, rule):
    # _mask is 250x16x512x128
    _mask = _mask.copy()
    if(rule=='horizontalPatch'):
        _mask[_mask>0]=1
        moduleClmPixN = _mask.shape[3]
        for startIndex in range(0, moduleClmPixN, ruleParameter):
            inds = np.where(_mask[:,:,:,startIndex:startIndex+ruleParameter].sum(3)>=ruleParameter/2)
            _mask[inds[0], inds[1], inds[2], startIndex:startIndex+ruleParameter]=1
    
    if(rule=='verticalPatch'):
        _mask[_mask>0]=1
        moduleRowPixN = _mask.shape[2]
        for startIndex in range(0, moduleRowPixN, ruleParameter):
            inds = np.where(_mask[:,:,startIndex:startIndex+ruleParameter,:].sum(2)>=ruleParameter/2)
            _mask[inds[0], inds[1], startIndex:startIndex+ruleParameter, inds[2]]=1

    if(rule=='maskLonleyGoodPixels'):
        _mask[_mask>0]=1
        #tmp  = _mask[:,:, 0:-2, 0:-2]
        tmp = _mask[:,:, 1:-1, 0:-2].copy()
        #tmp += _mask[:,:, 2:  , 0:-2]
        tmp += _mask[:,:, 0:-2, 1:-1]
        tmp += _mask[:,:, 2:  , 1:-1]
        #tmp += _mask[:,:, 0:-2, 2:  ]
        tmp += _mask[:,:, 1:-1, 2:  ]
        #tmp += _mask[:,:, 2:  , 2:  ]
        #tmp[tmp < ruleParameter] = 0
        tmp[tmp >= ruleParameter] = 1
        _mask[:,:, 1:-1, 1:-1] += tmp
        _mask[_mask>0]=1

    if(rule=='unMaskLonleyBadPixels'):
        _mask[_mask>0]=1
        _mask = 1 - _mask
        tmp  = _mask[:,:, 0:-2, 0:-2].copy()
        tmp += _mask[:,:, 1:-1, 0:-2]
        tmp += _mask[:,:, 2:  , 0:-2]
        tmp += _mask[:,:, 0:-2, 1:-1]
        tmp += _mask[:,:, 2:  , 1:-1]
        tmp += _mask[:,:, 0:-2, 2:  ]
        tmp += _mask[:,:, 1:-1, 2:  ]
        tmp += _mask[:,:, 2:  , 2:  ]
        #tmp[tmp < ruleParameter] = 0
        tmp[tmp >= ruleParameter] = 1
        _mask[:,:, 1:-1, 1:-1] += tmp
        _mask[_mask>0]=1
        _mask = 1 - _mask
        
    if(rule=='removeIslands'):
        _mask[_mask>0]=1    #good is zero and 1 is bad
        _maskAll = _mask.reshape(_mask.shape[0]*_mask.shape[1], _mask.shape[2], _mask.shape[3])
        _maskAll = connCompLabeling_multiproc(_maskAll, minimumIslandSize = ruleParameter)
        _mask[_maskAll.reshape(_mask.shape)==1] = 1
                
    if(rule=='OnDa_DEADASIC'):
        patchX, patchY = ruleParameter
        nRows = _mask.shape[0]
        nClms = _mask.shape[1]
        for rCnt in range(0, nRows, patchX):
            for cCnt in range(0, nClms, patchY):
                if((_mask[rCnt:rCnt+patchX, cCnt:cCnt+patchY]).mean()<=0.5):
                    _mask[rCnt:rCnt+patchX, cCnt:cCnt+patchY] = 0
    return _mask

def showCurrentMap(inMap, geometryFile):
    """
    This functions visualizes the inMap according to the geometry file, using
    crystfel apply geometry.
    inputs: 
        inMap: is maps of size 16x512x128
        geometryFile
    """
    ToShow = np.zeros((16*512, 128))

    for moduleCnt in range(16):
        ToShow[moduleCnt*512:(moduleCnt+1)*512,:] = \
                np.squeeze(inMap[moduleCnt, :, :]).copy()

    myGeom = cfelCryst.load_crystfel_geometry(geometryFile)
    _mask_geom = cfelGeom.apply_geometry_to_data(ToShow, myGeom)
    plt.imshow(_mask_geom)
    plt.colorbar()
    ToShow[ToShow  > 0]=1
    ToShow[ToShow <= 0]=0
    plt.title('Total mask: ' + str(int(10000*ToShow.mean())/100) + '%')
    plt.show()


def showWhatGeometryDoes(geometryFile):

    ToShow = np.zeros((16*512, 128), dtype='uint32')

    oneModule  = np.zeros((512, 128), dtype='uint32')
    for rCnt in range(8):
        for cCnt in range(2):
            oneModule[rCnt*64:(rCnt+1)*64, cCnt*64:(cCnt+1)*64] = rCnt + 8*cCnt
    plt.imshow(oneModule), plt.show()
    
    for moduleCnt in range(16):
        ToShow[moduleCnt*512:(moduleCnt+1)*512,:] = moduleCnt*100 + oneModule*6

    plt.imshow(ToShow.T), plt.show()
        
    myGeom = cfelCryst.load_crystfel_geometry(geometryFile)
    _mask_geom = cfelGeom.apply_geometry_to_data(ToShow, myGeom)
    plt.imshow(_mask_geom)
    plt.title('Total mask: ' + str(int(10000*ToShow.mean())/100) + '%')
    plt.show()    

def showOnDaMap(BadpixelMask_OnDa, geometryFile):
    myGeom = cfelCryst.load_crystfel_geometry(geometryFile)
    _mask_geom = cfelGeom.apply_geometry_to_data(BadpixelMask_OnDa, myGeom)
    plt.imshow(_mask_geom)
    plt.title('Total mask: ' + str(int(10000*(1-BadpixelMask_OnDa.mean()))/100) + '%')
    plt.show()
    
def plotPercentagesOverCells(badPixelMask):
    numMemCells = badPixelMask.shape[0]
    brokenVec = np.zeros(numMemCells)
    for cellCnt in range(numMemCells):
        brokenVec[cellCnt] = badPixelMask[cellCnt, :,:,:].mean()
    plt.plot(brokenVec, '.-')
    plt.ylim(0, brokenVec.max())
    plt.show()

if __name__ == '__main__':
    tst = np.zeros((10,10,7,7))
    tst[:,:, 2:6, 2:6] = 1
    tst[:,:, 3:5, 3:5] = 0
    badPixelMaskForASICRules(tst, (10, 1), rule='removeIslands')