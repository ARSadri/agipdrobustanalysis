import h5py
import numpy as np
import os
import fnmatch
import time
import sys
import gc

from multiprocessing import Process, Queue, cpu_count
from RGFLib.RobustGausFitLibPy import *

from RGFLib.RobustGausFitLibMultiprocPy import RobustAlgebraicLineFittingTensorPy_MultiProc, RSGImage_by_Image_TensorPy_multiproc

from textProgBar import textProgBar
import AGIPDCorrectionRoutine
GainSTDLambda = 6.0

def maskForCell(aQ, _procID, inSNRs, _temporalMED, _Proc, waterSNR, waterSNR_prob):
    maskType = 20
    n_R = inSNRs.shape[1]
    n_C = inSNRs.shape[2]
    theMask = np.zeros((n_R, n_C), dtype='uint8')
    _SNRs = np.zeros(inSNRs.shape, dtype='int8')
    _SNRs[inSNRs >  waterSNR] = 1
    _SNRs = _SNRs.mean(0)
    theMask[_SNRs >  waterSNR_prob] = maskType

    _SNRs = np.zeros(inSNRs.shape, dtype='int8')
    _SNRs[inSNRs < -waterSNR] = 1
    _SNRs = _SNRs.mean(0)
    maskType += 1
    theMask[_SNRs >  waterSNR_prob] = maskType
    del inSNRs, _SNRs

    aQ.put(list([_procID, theMask]))
    gc.collect()
    

if __name__ == '__main__':
    time_time = time.time()
    print('Calibration of AGIPD by the water analysis for each module of AGIPD')
    print('PID ->' + str(os.getpid()))
    argCnt = 0
    ########################## get the inputs from slurm ########################    
    argCnt += 1
    rootFolder = sys.argv[argCnt]
    argCnt += 1
    rootEntry = sys.argv[argCnt]
    argCnt += 1
    WriteDIR = sys.argv[argCnt]
    argCnt += 1
    numMemCells = int(sys.argv[argCnt])
    argCnt += 1
    moduleRowPixN = int(sys.argv[argCnt])
    argCnt += 1
    moduleClmPixN = int(sys.argv[argCnt])
    argCnt += 1
    maxNumberFramesInParts = int(sys.argv[argCnt])
    argCnt += 1
    runCnt = int(sys.argv[argCnt])
    argCnt += 1
    moduleId = int(sys.argv[argCnt])
    argCnt += 1
    number_of_parts_to_process = int(sys.argv[argCnt])
    argCnt += 1
    maxNumCPUs = int(sys.argv[argCnt])
    argCnt += 1
    winX = int(sys.argv[argCnt])
    argCnt += 1
    winY = int(sys.argv[argCnt])
    argCnt += 1
    waterSNR = float(sys.argv[argCnt])
    argCnt += 1
    calibFile = sys.argv[argCnt]
    argCnt += 1
    maskFile = sys.argv[argCnt]
    argCnt += 1
    waterSNR_prob = float(sys.argv[argCnt])
    
    print('rootFolder->' + ' ' + rootFolder)
    print('rootEntry->' + ' ' + rootEntry)
    print('WriteDIR->' + ' ' + WriteDIR)
    print('numMemCells->' + ' ' + str(numMemCells))
    print('moduleRowPixN->' + ' ' + str(moduleRowPixN))
    print('moduleClmPixN->' + ' ' + str(moduleClmPixN))
    print('maxNumberFramesInParts->' + ' ' + str(maxNumberFramesInParts))
    print('runCnt->' + ' ' + str(runCnt))
    print('moduleId->' + ' ' + str(moduleId))
    print('number_of_parts_to_process->' + ' ' + str(number_of_parts_to_process))
    print('maxNumCPUs->' + ' ' + str(maxNumCPUs))
    print('winX->' + ' ' + str(winX))
    print('winY->' + ' ' + str(winY))
    
    ########################## Read a few parts for a module ########################

    runCntStr = "%04d" % runCnt
    moduleIdStr = "%02d" % moduleId
    fileNameTemplate = 'RAW-R*-AGIPD' + moduleIdStr + '-S*.h5'
    folderPrefix = rootFolder + 'r' + runCntStr + '/'
    flist = fnmatch.filter(os.listdir(folderPrefix), fileNameTemplate)
    flist.sort()
    number_of_parts_to_process = np.minimum(len(flist), number_of_parts_to_process)
    
    cellidSet_all = np.zeros((number_of_parts_to_process*maxNumberFramesInParts,1), dtype='uint32')
    dataSet_all = np.zeros((number_of_parts_to_process*maxNumberFramesInParts, 2, moduleRowPixN, moduleClmPixN), dtype='uint16')
    print('There are ' +str(number_of_parts_to_process) + ' parts to read')
    size_so_far = 0
    for cont in range(number_of_parts_to_process):
        cont = 1
        rFileName = flist[cont]
        ReadFileName = folderPrefix + rFileName
        fileh5 = h5py.File(ReadFileName,'r')
        lengthEntry = rootEntry + str(moduleId) + 'CH0:xtdf/image/length'
        CellIDEntry = rootEntry + str(moduleId) + 'CH0:xtdf/image/cellId'
        ReadEntry = rootEntry + str(moduleId) + 'CH0:xtdf/image/data'
        cellidSet = fileh5[lengthEntry]
        numFrames = np.count_nonzero(cellidSet[:])
        if(numFrames==0):
            print("file " + ReadFileName + " has no data points")
            continue
        print('Reading: ' + ReadFileName +' with ' +str(numFrames)+ ' frames')
        cellidSet = fileh5[CellIDEntry]
        dataSet = fileh5[ReadEntry]
        cellidSet_all[size_so_far:size_so_far + numFrames] = cellidSet[:numFrames]
        dataSet_all[size_so_far:size_so_far + numFrames,:,:,:] = dataSet[:numFrames,:,:,:]
        size_so_far += numFrames
        fileh5.close()
    
    cellidSet_all = np.squeeze(cellidSet_all[:size_so_far])
    dataSet_all = dataSet_all[:size_so_far,:,:,:]
    print("--- reading h5 dataset took %s seconds ---" % (time.time() - time_time))
    print('size of the dataset is ' + str(dataSet_all.nbytes/(1024*1024*1024)) + ' GB')
    
    print('Preparing pre-calculated statistics...')
    calibFileh5 = h5py.File(calibFile,'r')
    calib_AnalogOffset =  calibFileh5['AnalogOffset']
    AnalogOffset = np.squeeze(calib_AnalogOffset[:,:,moduleId,:,:])
    calib_GainLevel =  calibFileh5['GainLevel']
    GainLevel = np.squeeze(calib_GainLevel[:2,:,moduleId,:,:])     # --> 2x250x512x128
    calib_GainLevelStds =  calibFileh5['GainLevelStds']
    GainLevelStds = np.squeeze(calib_GainLevelStds[:2,:,moduleId,:,:])     # --> 2x250x512x128
    calib_RelativeGain =  calibFileh5['RelativeGain']
    RelativeGain = np.squeeze(calib_RelativeGain[:2,:,moduleId,:,:])     # --> 250x512x128
    calibFileh5.close()
    
    print('Preparing mask...')
    maskFileh5 = h5py.File(maskFile,'r')
    badPixelMask =  maskFileh5['badPixelMask']  #stages x cells x modules x rows x clms
    badPixelMask = np.squeeze(badPixelMask[:, :, moduleId, :, :])
    badPixelMask[badPixelMask>0]=1
    badPixelMask = 1 - badPixelMask
    maskFileh5.close()
    
    ################################################################################
    print('Finding where pixels have switched gains')
    backMask = np.squeeze(dataSet_all[:, 1, :, :]).copy()
    f_N = backMask.shape[0]
    r_N = backMask.shape[1]
    c_N = backMask.shape[2]
    
    r_nSeg = 8
    c_nSeg = 2
    
    r_winL = int(r_N/r_nSeg)
    c_winL = int(c_N/c_nSeg)
    
    DGL1 = np.squeeze(GainLevel[0, cellidSet_all, :, :] + GainSTDLambda*GainLevelStds[0, cellidSet_all, :, :])
    backMask[backMask <  DGL1] = 0
    backMask[backMask >= DGL1] = 1
    del DGL1
    # this gives the maximum value of a pixel as long as it has not switched gain
    analogMax_HighGain = np.zeros((numMemCells, moduleRowPixN, moduleClmPixN), dtype='float32')
    analogMax = np.squeeze(dataSet_all[:, 0, :, :] - AnalogOffset[0, cellidSet_all, :, :])
    analogMax[backMask==1]=0
    pBar = textProgBar(numMemCells, title='Calculating maximum of pixels for each memory cells')
    for cellCnt in range(numMemCells):
        analogMax_HighGain[cellCnt, :, :] = analogMax[cellidSet_all==cellCnt, :, :].max(0)   
        pBar.go()
    del pBar
    del analogMax
    
    ###############################################################################################333    
    print('\nCalculating Proc of water', flush=True)
    proc_time = time.time()
    Proc, inMask  = AGIPDCorrectionRoutine.AGIPDCorrectTensor_multiprocessing(dataSet_all, 
                                                                                cellidSet_all, 
                                                                                GainLevel, 
                                                                                GainLevelStds, 
                                                                                GainSTDLambda, 
                                                                                AnalogOffset, 
                                                                                RelativeGain, 
                                                                                badPixelMask,
                                                                                analogMax_HighGain)
    del AnalogOffset, GainLevelStds, GainLevel, RelativeGain, dataSet_all, badPixelMask

    outputFileName = WriteDIR + 'waterMaps-AGIPD' + moduleIdStr + '.h5'
    print('writing to ' + outputFileName)
    file_outData = h5py.File(outputFileName, "w")
    file_outData.create_dataset('analogMax_HighGain', data = analogMax_HighGain)
    file_outData.close()
    del analogMax_HighGain
    
    print('Making a mask for patches in which even a single pixel has switched gains')
    print('Stages: 1', end='', flush=True)
    gainStageTensor = backMask.copy()
    print(', 2', end='', flush=True)
    backMask = backMask.reshape(f_N, r_nSeg, r_winL, c_N)
    print(', 3', end='', flush=True)
    backMask = backMask.reshape(f_N, r_nSeg, r_winL, c_nSeg, c_winL)
    print(', 4', end='', flush=True)
    backMask = np.swapaxes(backMask, 2, 3)
    print(', 5', end='', flush=True)
    backMask = backMask.reshape(f_N * r_nSeg * c_nSeg, r_winL, c_winL)
    print(', 6', end='', flush=True)
    backMask[backMask.max(1).max(1)==1, :, :] = 1
    print(', 7', end='', flush=True)
    backMask = backMask.reshape(f_N, r_nSeg, c_nSeg, r_winL, c_winL)
    print(', 8', end='', flush=True)
    backMask = np.swapaxes(backMask, 2, 3)
    print(', 9', end='', flush=True)
    backMask = backMask.reshape(f_N, r_nSeg, r_winL, c_N)
    print(', 10', flush=True)
    backMask = 1-backMask.reshape(f_N, r_N, c_N)

    print('Calculating Proc took ' + str(time.time() - proc_time) + ' seconds for ' + str(f_N) + ' frames')
    print('That is ' + str(int(f_N/(time.time() - proc_time))) + ' frames per second')
    
    print('Calculating Proc background', flush=True)
    bckMP = RSGImage_by_Image_TensorPy_multiproc(Proc, 
                                                inMask = inMask,
                                                winX = 64,
                                                winY = 64)
    
    print('Background calculated at ' + str(time.time() - time_time) + 's')
    
    ################################################################################
    print('Calculating maximum background mean map', flush=True)
    maxBackMeanMap = (backMask*bckMP[0]).max(0)
    print('writing to ' + outputFileName)
    file_outData = h5py.File(outputFileName, "a")
    file_outData.create_dataset('maxBackMeanMap', data = maxBackMeanMap)
    file_outData.close()
    del maxBackMeanMap
    ################################################################################
    print('Calculating pixels separabilities in ASICs where no pixel has switched gains', flush=True)
    SNRs_ALL = inMask*backMask*(Proc - bckMP[0])/(bckMP[1]+(np.finfo(np.float32).eps))
    
    #calculate all backgrounds   Or   find median of them all #
    n_F = Proc.shape[0]
    n_R = Proc.shape[1]
    n_C = Proc.shape[2]
    
    temporalMED = np.median(np.squeeze(bckMP[0]).reshape(n_F, n_R*n_C), axis=1)
    del backMask, inMask, bckMP
    
    
    waterBasedMask = np.zeros((numMemCells, moduleRowPixN, moduleClmPixN), dtype='uint8')
    
    
    myCPUCount = cpu_count()-1
    aQ = Queue()
    numProc = numMemCells
    procID = 0
    numProcessed = 0
    numBusyCores = 0

    print('starting ' +str(numProc) + ' processes with ' + str(myCPUCount) + ' CPUs')
    os.system('free -m')
    while(numProcessed<numProc):
        if (not aQ.empty()):
            aQElement = aQ.get()
            Q_procID = aQElement[0]
            Q_result = aQElement[1]
            waterBasedMask[Q_procID] = Q_result
            numProcessed += 1
            numBusyCores -= 1
            if(numProcessed == 1):
                pBar = textProgBar(numProc-1, title = 'New mask for each memory cell')
            if(numProcessed > 1):
                pBar.go()

        if((procID<numProc) & (numBusyCores < myCPUCount)):
            Process(target = maskForCell, args = (aQ, procID, 
                                                SNRs_ALL[cellidSet_all==procID],
                                                temporalMED[cellidSet_all==procID],
                                                Proc[cellidSet_all==procID, :, :],
                                                waterSNR, waterSNR_prob)).start()
            procID += 1
            numBusyCores += 1
    del pBar
    
    outputFileName = WriteDIR + 'waterMaps-AGIPD' + moduleIdStr + '.h5'
    print('writing to ' + outputFileName)
    file_outData = h5py.File(outputFileName, "a")
    file_outData.create_dataset('waterBasedMask', data = waterBasedMask)
    file_outData.close()
    
    print("--- Finishing in %s seconds ---" % (time.time() - time_time))
    print('Finished at ' + str(time.time()), flush=True)
    exit()