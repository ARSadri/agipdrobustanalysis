import h5py
import numpy as np
import time
import sys

import maskLib
import RGFLib.RobustGausFitLibPy
import RGFLib.RobustGausFitLibMultiprocPy as RGFLib_multiproc

geometryFile = '/gpfs/exfel/exp/SPB/202001/p002450/scratch/alireza/agipd-2480-v5.geom'

if __name__ == '__main__':
    start_time = time.time()
    
    print('Bad pixel mask maker based on the Dark is called at ' + str(start_time))
    
    calibFolder = sys.argv[1]
    AGIPD_ASICRows = int(sys.argv[2])
    AGIPD_ASICClms = int(sys.argv[3])
    separabilityBetweenStages_Threshold = float(sys.argv[4])
    offsetSNR = float(sys.argv[5])
    darkSNR_prob = float(sys.argv[6])
    filterLength = int(sys.argv[7])
    minimumIslandSize = int(sys.argv[8])
    badpixelFilename = sys.argv[9]
    badDataFileName = sys.argv[10]
    
    print('calibFolder->' + calibFolder)
    print('AGIPD_ASICRows->' + str(AGIPD_ASICRows))
    print('AGIPD_ASICClms->' + str(AGIPD_ASICClms))
    print('separabilityBetweenStages_Threshold->' + str(separabilityBetweenStages_Threshold))
    print('darkSNR_prob->' + str(darkSNR_prob))
    print('offsetSNR->' + str(offsetSNR))
    print('filterLength->' + str(filterLength))
    print('minimumIslandSize->' + str(minimumIslandSize))
    print('badpixelFilename->' + str(badpixelFilename))

    ASICShape = (AGIPD_ASICRows, AGIPD_ASICClms)
    
    print('Reading the pixelwise models...')
    calibFileName = calibFolder + '/' + 'Cheetah-AGIPD-calib.h5'
    print('Calib file is: ' + calibFileName)
    calibFile = h5py.File(calibFileName,'r')
    AnalogOffset = calibFile['AnalogOffset']
    AnalogOffset = AnalogOffset[...]
    AnalogStds = calibFile['AnalogStds']
    AnalogStds = AnalogStds[...]
    
    GainLevel = calibFile['GainLevel']
    GainStds = calibFile['GainLevelStds']

    print('Reading the SNR maps for flickering pixels in the dark...')
    badDataFile = h5py.File(badDataFileName,'r')
    hotFlickMapDataSet = badDataFile['hotFlickMap']   #cell x modules x 512 x 128
    coldFlickMapDataSet = badDataFile['coldFlickMap']
    
    print('The pixelwise models are loaded.', flush=True)
    
    # they are all 3 x 250 x 16 x 512 x 128 and of 'float64'
    numRuns = AnalogOffset.shape[0]
    numMemCells = AnalogOffset.shape[1]
    numModules = AnalogOffset.shape[2]
    moduleRowPixN = AnalogOffset.shape[3]
    moduleClmPixN = AnalogOffset.shape[4]
    
    print('numRuns->' + str(numRuns))
    print('numMemCells->' + str(numMemCells))
    print('numModules->' + str(numModules))
    print('moduleRowPixN->' + str(moduleRowPixN))
    print('moduleClmPixN->' + str(moduleClmPixN))
    
    badPixelMask = np.zeros((numRuns, numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='uint8')
    maskType = 0
    ########### By borders ############
    maskType += 1
    _mask = maskLib.maskByBoarders(numMemCells, numModules, moduleRowPixN, moduleClmPixN, ASICShape)
    badPixelMask[:, _mask==1] = maskType
    print('ASICs border mask: ' + \
            str(int(10000*(badPixelMask==maskType).sum()/badPixelMask.size)/100) + \
            '% masked', flush=True)
        
    ###################################
      
    ########### By gap between gain stage indicators ############
    maskType += 1
    gainStagesGap_HM = (GainLevel[1,...] - GainLevel[0,...])/(GainStds[1,...] + GainStds[0,...]+1)
    _mask = np.zeros((numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='uint8')
    _mask[gainStagesGap_HM<separabilityBetweenStages_Threshold] = 1
    badPixelMask[:, _mask==1] = maskType
    print('Non-switching pixels mask between high and medium stages (ignoring Low-Medium): ' + \
            str(int(10000*(badPixelMask==maskType).sum()/badPixelMask.size)/100) + \
            '% masked', flush=True)
            
    #############################################################
    
    ########### flickering in the Dark By SNR modeled in the Dark ##########
    maskType += 1
    hotFlickMap = hotFlickMapDataSet[...]
    hotFlickMap[hotFlickMap<darkSNR_prob] = 0
    hotFlickMap[hotFlickMap>darkSNR_prob] = 1
    badPixelMask[hotFlickMap==1] = maskType
    print('Pixels flickering up in the dark ' + \
            str(int(10000*(badPixelMask==maskType).sum()/badPixelMask.size)/100) + \
            '% masked', flush=True)

    maskType += 1
    coldFlickMap = coldFlickMapDataSet[...]
    coldFlickMap[coldFlickMap<darkSNR_prob] = 0
    coldFlickMap[coldFlickMap>darkSNR_prob] = 1
    badPixelMask[coldFlickMap==1] = maskType
    print('Pixels flickering low in the dark ' + \
            str(int(10000*(badPixelMask==maskType).sum()/badPixelMask.size)/100) + \
            '% masked', flush=True)

    #######################################################################

    ################## By abnormal analog offsets ###################
    #runs x cells x modules x rows x clms
    print('Calculating analog offsets model')
    reshaped_AnalogOffset = AnalogOffset.reshape(AnalogOffset.shape[0]*AnalogOffset.shape[1]*AnalogOffset.shape[2], AnalogOffset.shape[3], AnalogOffset.shape[4])
    #notice: AnalogOffset[0, 6, 4] = reshaped_AnalogOffset[100]
    bckModel = RGFLib_multiproc.RSGImage_by_Image_TensorPy_multiproc(reshaped_AnalogOffset, winX=64, winY=64)
    bckModel = bckModel.reshape(2, AnalogOffset.shape[0],
                                   AnalogOffset.shape[1],
                                   AnalogOffset.shape[2],
                                   AnalogOffset.shape[3],
                                   AnalogOffset.shape[4])
    
    print('Calculating analog offsets separabilities.')
    SNRs = (AnalogOffset - bckModel[0])/bckModel[1]
    
    maskType += 1
    
    badPixelMask[SNRs > offsetSNR] = maskType
    print('Pixels with too high offsets in the dark ' + \
            str(int(10000*(badPixelMask==maskType).sum()/badPixelMask.size)/100) + \
            '% masked', flush=True)
    
    maskType += 1
    badPixelMask[SNRs < -offsetSNR] = maskType
    print('Pixels with too low offsets in the dark ' + \
            str(int(10000*(badPixelMask==maskType).sum()/badPixelMask.size)/100) + \
            '% masked', flush=True)
    #################################################################

    ################## By abnormal analog STDs ###################
    #runs x cells x modules x rows x clms
    print('Calculating analog STDs model')
    reshaped_AnalogSTDs = AnalogStds.reshape(AnalogStds.shape[0]*AnalogStds.shape[1]*AnalogStds.shape[2], AnalogStds.shape[3], AnalogStds.shape[4])
    #notice: AnalogStds[0, 6, 4] = reshaped_AnalogSTDs[100]
    bckModel = RGFLib_multiproc.RSGImage_by_Image_TensorPy_multiproc(reshaped_AnalogSTDs, winX=64, winY=64)
    bckModel = bckModel.reshape(2, AnalogStds.shape[0],
                                   AnalogStds.shape[1],
                                   AnalogStds.shape[2],
                                   AnalogStds.shape[3],
                                   AnalogStds.shape[4])
    
    print('Calculating analog STDs separabilities.')
    SNRs = (AnalogStds - bckModel[0])/bckModel[1]
    
    maskType += 1
    
    badPixelMask[SNRs > offsetSNR] = maskType
    print('Pixels with too high STDs in the dark ' + \
            str(int(10000*(badPixelMask==maskType).sum()/badPixelMask.size)/100) + \
            '% masked', flush=True)
    
    '''
    maskType += 1
    badPixelMask[SNRs < -offsetSNR] = maskType
    print('Pixels with too low STDs in the dark ' + \
            str(int(10000*(badPixelMask==maskType).sum()/badPixelMask.size)/100) + \
            '% masked', flush=True)
    '''
    #################################################################

    
    
    ################## By ASIC Rules ###################
    maskType += 1
    for stageCnt in range(3):
        _mask = maskLib.badPixelMaskForASICRules(np.squeeze(badPixelMask[stageCnt]), 
                                                 filterLength, rule='verticalPatch')
        _mask[badPixelMask[stageCnt]>0]=0
        badPixelMask[stageCnt, _mask==1] = maskType
    print('Mask for pixels on a vertical patch with half of pixels being bad is made')
    print('Rule: ' + \
            str(int(10000*(badPixelMask==maskType).sum()/badPixelMask.size)/100) + \
            '% masked', flush=True)

    maskType += 1
    for stageCnt in range(3):
        _mask = maskLib.badPixelMaskForASICRules(np.squeeze(badPixelMask[stageCnt]), 
                                                 filterLength, rule='horizontalPatch')
        _mask[badPixelMask[stageCnt]>0]=0
        badPixelMask[stageCnt, _mask==1] = maskType
    print('Mask for pixels on a horizontal patch with half of pixels being bad is made')
    print('Rule: ' + \
            str(int(10000*(badPixelMask==maskType).sum()/badPixelMask.size)/100) + \
            '% masked', flush=True)
    
    maskType += 1
    for stageCnt in range(2):
        inMask = np.squeeze(badPixelMask[stageCnt])
        inMask = maskLib.badPixelMaskForASICRules(inMask, 3, rule='maskLonleyGoodPixels')
        inMask = maskLib.badPixelMaskForASICRules(inMask, 4, rule='unMaskLonleyBadPixels')
        _mask = maskLib.badPixelMaskForASICRules(inMask, minimumIslandSize, rule='removeIslands')
        _mask[badPixelMask[stageCnt]>0]=0
        badPixelMask[stageCnt, _mask==1] = maskType
    print('Mask for lonleyPixels is made')
    print('Rule: ' + \
            str(int(10000*(badPixelMask==maskType).sum()/badPixelMask.size)/100) + \
            '% masked', flush=True)
    
    print('In total: ' + \
            str(int(10000*(badPixelMask!=0).sum()/(badPixelMask==0).sum())/100) + \
            '% masked', flush=True)

    ############################### appending bad pixel mask to the constant file #############################
    # these are kept:
    #   badPixelMask                3x250x16x512x128
    #   BadpixelMask_OnDa           8196x128

    ####################### just for OnDa ####################
    print('Creating bad pixel mask for OnDa using worst of all cells...', flush=True)
    BadpixelcellsCombined = badPixelMask.copy()
    BadpixelcellsCombined[BadpixelcellsCombined>0]=1
    BadpixelcellsCombined = 1 - BadpixelcellsCombined
    BadpixelcellsCombined = BadpixelcellsCombined.min(0).min(0)
    BadpixelMask_OnDa = np.zeros((numModules*moduleRowPixN, moduleClmPixN), dtype='uint8')
    for moduleCnt in range(numModules):
        BadpixelMask_OnDa[moduleCnt*512:(moduleCnt+1)*512,:] = \
                    1-RGFLib.RobustGausFitLibPy.islandRemovalPy(1-np.squeeze(BadpixelcellsCombined[moduleCnt, :, :]), islandSizeThreshold = 64)
                    
    BadpixelMask_OnDa = maskLib.badPixelMaskForASICRules(BadpixelMask_OnDa, (filterLength,filterLength), rule='OnDa_DEADASIC')

    print('In total, for OnDa: ' + str(int(10000*(1-BadpixelMask_OnDa.mean()))/100) + '% is masked')
    
    print('Creating calib file...', flush=True)
    
    badpixelFile = h5py.File(badpixelFilename,'w')
    
    if ("badPixelMask" not in badpixelFile):
        badpixelFile.create_dataset('badPixelMask', data = badPixelMask)
    else:
        _badPixelMask = badpixelFile['badPixelMask']
        _badPixelMask[...] = badPixelMask
    
    if ("BadpixelMask_OnDa" not in badpixelFile):
        badpixelFile.create_dataset('BadpixelMask_OnDa', data = BadpixelMask_OnDa)
    else:
        _Badpixel_maxOnDa = badpixelFile['BadpixelMask_OnDa']
        _Badpixel_maxOnDa[...] = BadpixelMask_OnDa

    badpixelFile.close()

    print("--- Finishing in %s seconds ---" % (time.time() - start_time))
    print('Finished at ' + str(time.time()), flush=True)
    exit()