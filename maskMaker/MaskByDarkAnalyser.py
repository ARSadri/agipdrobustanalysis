import h5py
import numpy as np
import os
import fnmatch
import time
import sys
from multiprocessing import Process, Queue, cpu_count

import RGFLib.RobustGausFitLibPy as RGFLib
from textProgBar import textProgBar


def calibConstants(dataSet, darkSNR, winX, winY):
    analogData  = np.squeeze(dataSet[:,0,:,:])

    goodFrames = np.median(analogData.reshape(analogData.shape[0], analogData.shape[1]*analogData.shape[2]), 1)
    goodFrames_mP = RGFLib.RobustSingleGaussianVecPy(goodFrames)
    goodFramesInds = np.where(np.fabs(goodFrames-goodFrames_mP[0]) < goodFrames_mP[1]*2)

    analogData = np.squeeze(analogData[goodFramesInds[0],:,:])
    analog_mean = analogData.mean(0)

    correctedTensor = analogData - np.tile(analog_mean, (analogData.shape[0], 1, 1))
    origShape = correctedTensor.shape

    inImage = correctedTensor.reshape(origShape[0]*origShape[1], origShape[2])
    bckModel = RGFLib.RMGImagePy(inImage, winX = winX, winY = winY, numModelParams = 1, optIters = 8)
    bckModel = bckModel.reshape(2, origShape[0], origShape[1], origShape[2])
    
    SNRs = (correctedTensor - bckModel[0])/bckModel[1]
    _SNRs = SNRs.copy()
    _SNRs[SNRs<darkSNR] = 0
    _SNRs[SNRs>darkSNR] = 1
    hotFlickMap = _SNRs.mean(0)

    _SNRs = SNRs.copy()
    _SNRs[SNRs>-darkSNR] = 0
    _SNRs[SNRs<-darkSNR] = 1
    coldFlickMap = _SNRs.mean(0)
    
    return (hotFlickMap, coldFlickMap)
        
def calibConstants_by_multiproc_func(aQ, _procID, inData, darkSNR, winX, winY):
    hotFlickMap, coldFlickMap = calibConstants(inData, darkSNR, winX, winY)
    aQ.put(list([_procID, hotFlickMap, coldFlickMap]))

def calibConstants_by_multiproc(dataSet_all, cellidSet_all, numMemCells, darkSNR, winX, winY):

    moduleRowPixN = dataSet_all.shape[2]
    moduleClmPixN = dataSet_all.shape[3]
    hotFlickMap = np.zeros((numMemCells, moduleRowPixN, moduleClmPixN), dtype='float32')
    coldFlickMap = np.zeros((numMemCells, moduleRowPixN, moduleClmPixN), dtype='float32')

    N = numMemCells
    
    myCPUCount = cpu_count()-1
    aQ = Queue()
    numBusyCores = 0
    numProc = N
    numWiating = N
    numDone = 0
    
    procID = 0
    
    print('starting ' +str(numProc) + ' processes with ' + str(myCPUCount) + ' CPUs')
    pBar = textProgBar(2*N, 100, title = 'Multiprocessing results progress bar')
    while(numDone<numProc):
        if (not aQ.empty()):
            aQElement = aQ.get()
            Q_procID                = aQElement[0]
            hotFlickMap[Q_procID]   = aQElement[1]
            coldFlickMap[Q_procID]  = aQElement[2]
            numDone += 1
            numBusyCores -= 1
            pBar.go()
            if(numBusyCores>0):
                continue;    # empty the queue

        if((numWiating>0) & (numBusyCores < myCPUCount)):
            inData  = np.squeeze(dataSet_all[cellidSet_all==procID,:,:,:])
            Process(target = calibConstants_by_multiproc_func,
                    args = (aQ, procID, inData, darkSNR, winX, winY)).start()
            procID += 1
            numWiating -= 1
            numBusyCores += 1
            pBar.go()
    del pBar
    return (hotFlickMap, coldFlickMap)
    
if __name__ == '__main__':
    time_time = time.time()
    print('Calibration of AGIPD by the Dark analysis for each module of AGIPD')
    argCnt = 0
    ########################## get the inputs from slurm ########################    
    argCnt += 1
    rootFolder = sys.argv[argCnt]
    argCnt += 1
    rootEntry = sys.argv[argCnt]
    argCnt += 1
    WriteDIR = sys.argv[argCnt]
    argCnt += 1
    numMemCells = int(sys.argv[argCnt])
    argCnt += 1
    moduleRowPixN = int(sys.argv[argCnt])
    argCnt += 1
    moduleClmPixN = int(sys.argv[argCnt])
    argCnt += 1
    maxNumberFramesInParts = int(sys.argv[argCnt])
    argCnt += 1
    runCnt = int(sys.argv[argCnt])
    argCnt += 1
    moduleId = int(sys.argv[argCnt])
    argCnt += 1
    number_of_parts_to_process = int(sys.argv[argCnt])
    argCnt += 1
    maxNumCPUs = int(sys.argv[argCnt])
    argCnt += 1
    winX = int(sys.argv[argCnt])
    argCnt += 1
    winY = int(sys.argv[argCnt])
    argCnt += 1
    darkSNR = float(sys.argv[argCnt])
    
    print('rootFolder->' + ' ' + rootFolder)
    print('rootEntry->' + ' ' + rootEntry)
    print('WriteDIR->' + ' ' + WriteDIR)
    print('numMemCells->' + ' ' + str(numMemCells))
    print('moduleRowPixN->' + ' ' + str(moduleRowPixN))
    print('moduleClmPixN->' + ' ' + str(moduleClmPixN))
    print('maxNumberFramesInParts->' + ' ' + str(maxNumberFramesInParts))
    print('runCnt->' + ' ' + str(runCnt))
    print('moduleId->' + ' ' + str(moduleId))
    print('number_of_parts_to_process->' + ' ' + str(number_of_parts_to_process))
    print('maxNumCPUs->' + ' ' + str(maxNumCPUs))
    print('winX->' + ' ' + str(winX))
    print('winY->' + ' ' + str(winY))
    
    ########################## Read a few parts for a module ########################

    runCntStr = "%04d" % runCnt
    moduleIdStr = "%02d" % moduleId
    fileNameTemplate = 'RAW-R*-AGIPD' + moduleIdStr + '-S*.h5'
    folderPrefix = rootFolder + 'r' + runCntStr + '/'
    flist = fnmatch.filter(os.listdir(folderPrefix), fileNameTemplate)
    flist.sort()
    number_of_parts_to_process = np.minimum(len(flist), number_of_parts_to_process)
    
    cellidSet_all = np.zeros((number_of_parts_to_process*maxNumberFramesInParts,1), dtype='uint32')
    dataSet_all = np.zeros((number_of_parts_to_process*maxNumberFramesInParts, 2, moduleRowPixN, moduleClmPixN), dtype='uint16')
    print('There are ' +str(number_of_parts_to_process) + ' parts to read', flush = True)
    size_so_far = 0
    for cont in range(number_of_parts_to_process):
        rFileName = flist[cont]
        ReadFileName = folderPrefix + rFileName
        moduleNumber = str(int(ReadFileName.split("AGIPD",2)[1][:2]))
        fileh5 = h5py.File(ReadFileName,'r')
        lengthEntry = rootEntry + moduleNumber + 'CH0:xtdf/image/length'
        CellIDEntry = rootEntry + moduleNumber + 'CH0:xtdf/image/cellId'
        ReadEntry = rootEntry + moduleNumber + 'CH0:xtdf/image/data'
        cellidSet = fileh5[lengthEntry]
        numFrames = np.count_nonzero(cellidSet[:])
        if(numFrames==0):
            print("file " + ReadFileName + " has no data points")
            continue
        print('Reading: ' + ReadFileName +' with ' +str(numFrames)+ ' frames')
        cellidSet = fileh5[CellIDEntry]
        dataSet = fileh5[ReadEntry]
        cellidSet_all[size_so_far:size_so_far + numFrames] = cellidSet[:numFrames]
        dataSet_all[size_so_far:size_so_far + numFrames,:,:,:] = dataSet[:numFrames,:,:,:]
        size_so_far += numFrames
    
    cellidSet_all = np.squeeze(cellidSet_all[:size_so_far])
    dataSet_all = dataSet_all[:size_so_far,:,:,:]
    print("--- reading h5 dataset took %s seconds ---" % (time.time() - time_time))
    print('size of the dataset is ' + str(dataSet_all.nbytes/(1024*1024*1024)) + ' GB')

    ################################### Processing the darks!... ########################
    
    hotFlickMap, coldFlickMap = \
                calibConstants_by_multiproc(dataSet_all, cellidSet_all, numMemCells, darkSNR, winX, winY)

    ########################## creating one file for a module with constants ########################
    outputFileNameFull = WriteDIR + 'pixelMask-AGIPD' + moduleIdStr + '.h5'
    file_outData = h5py.File(outputFileNameFull, "w")
    file_outData.create_dataset( 'hotFlickMap',data = hotFlickMap)
    file_outData.create_dataset( 'coldFlickMap',data = coldFlickMap)
    file_outData.close()        

    print("--- Processing the series into cells took %s seconds ---" % (time.time() - time_time))
    print('finished at ' + str(time.time())+ 's')

    exit()