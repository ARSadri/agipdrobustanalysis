import h5py
import time
import sys
import numpy

if __name__ == '__main__':
    start_time = time.time()
    print('Gatherer 48 files together for AGIPD-1M ', flush = True)
    ########################## get the inputs from slurm ########################    
    argcnt = 0
    argcnt += 1
    scratchFolder = sys.argv[argcnt]
    argcnt += 1
    numModules = int(sys.argv[argcnt])
    argcnt += 1
    numMemCells = int(sys.argv[argcnt])
    argcnt += 1
    moduleRowPixN = int(sys.argv[argcnt])
    argcnt += 1
    moduleClmPixN = int(sys.argv[argcnt])
    argcnt += 1
    runNumDarkHighGain = int(sys.argv[argcnt])
    argcnt += 1
    runNumDarkMediumGain = int(sys.argv[argcnt])
    argcnt += 1
    runNumDarkLowGain = int(sys.argv[argcnt])
    argcnt += 1
    badDataFileName = sys.argv[argcnt]

    runNumbers = [runNumDarkHighGain, runNumDarkMediumGain, runNumDarkLowGain]
    numRuns = len(runNumbers)
    
    ########################## gather 48 files for AGIPD three dark runs into one file ########################    
    #hotFlickMap, coldFlickMap  
    hotFlickMap = numpy.zeros((numRuns, numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='float32')
    coldFlickMap = numpy.zeros((numRuns, numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='float32')
    
    for moduleCnt in range(numModules):
        moduleCntStr = "%02d" % moduleCnt
        for runCnt, runNumber in enumerate(runNumbers):
            print('module ' +str(moduleCnt) + ' run ' +str(runCnt), flush=True)
            runNumberStr = "%04d" % runNumber
            readDIR = scratchFolder + '/r' + runNumberStr + '/'
            ReadFileName = readDIR + 'pixelMask-AGIPD' + moduleCntStr + '.h5'
            fileh5 = h5py.File(ReadFileName,'r')
            hotFlickMap[runCnt, :, moduleCnt, :, :] = fileh5['hotFlickMap']
            coldFlickMap[runCnt, :, moduleCnt, :, :] = fileh5['coldFlickMap']
            fileh5.close()
    
    file_outData = h5py.File(badDataFileName, "w")
    file_outData.create_dataset('hotFlickMap', data = hotFlickMap)
    file_outData.create_dataset('coldFlickMap', data = coldFlickMap)
    file_outData.close()
    
    print("--- Finishing in %s seconds ---" % (time.time() - start_time))
    print("--- Finishing at %s ---" % time.time())
    exit()