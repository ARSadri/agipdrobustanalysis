from multiprocessing import Process, Queue, cpu_count
import numpy as np
from textProgBar import textProgBar

def moduleCorrectionTensor(rawImg, GainLevel, GainLevelStds, GainSTDLambda, 
                                   AnalogOffset, RelativeGain, badPixelMask,
                                   rawCellId, analogMax_HighGain):

    inputAnalog = rawImg[:, 0, :, :]
    inputGains = rawImg[:, 1, :, :]
    
    procImg = np.zeros(inputAnalog.shape, dtype='float32')
    mask = np.ones(inputAnalog.shape, dtype='uint8')
    
    AnaOff = AnalogOffset[0,rawCellId,:,:]
    badMask = badPixelMask[0, rawCellId, :, :]
    relGain = RelativeGain[0, rawCellId, :, :]                                          
    _tmp = (inputAnalog - AnaOff)#*relGain
    del AnaOff

    DGL1 = GainLevel[0, rawCellId, :, :] + GainSTDLambda*GainLevelStds[0, rawCellId, :, :]
    indsi_1, indsj_1, indsk_1 = np.where(inputGains < DGL1)
    procImg[indsi_1, indsj_1, indsk_1] = _tmp[indsi_1, indsj_1, indsk_1].copy()
    mask[indsi_1, indsj_1, indsk_1] = badMask[indsi_1, indsj_1, indsk_1].copy()
    del indsi_1
    del indsj_1
    del indsk_1
    
    DGL2 = GainLevel[1, rawCellId, :, :] + GainSTDLambda*GainLevelStds[1, rawCellId, :, :]
    _analogMax_HighGain = analogMax_HighGain[rawCellId, :, :]
    indsi_12, indsj_12, indsk_12 = np.where((DGL1 <= inputGains) & 
                                            (inputGains <= DGL2) & 
                                            (_tmp > _analogMax_HighGain))
    procImg[indsi_12, indsj_12, indsk_12] = _tmp[indsi_12, indsj_12, indsk_12].copy()
    mask[indsi_12, indsj_12, indsk_12] = badMask[indsi_12, indsj_12, indsk_12].copy()
    
    indsi_12, indsj_12, indsk_12 = np.where((DGL1 <= inputGains) & 
                                            (inputGains <= DGL2) & 
                                            (_tmp <= _analogMax_HighGain))
    procImg[indsi_12, indsj_12, indsk_12] = _analogMax_HighGain[indsi_12, indsj_12, indsk_12].copy()
        
    del DGL1    
    indsi_2, indsj_2, indsk_2 = np.where(inputGains > DGL2)
    AnaOff = AnalogOffset[1,rawCellId, :, :]
    badMask = badPixelMask[1, rawCellId, :, :]
    relGain = RelativeGain[1, rawCellId, :, :]
    _tmp = (inputAnalog - AnaOff)*relGain
    
    procImg[indsi_2, indsj_2, indsk_2] = _tmp[indsi_2, indsj_2, indsk_2].copy()
    mask[indsi_2, indsj_2, indsk_2] = badMask[indsi_2, indsj_2, indsk_2].copy()

    indsi_12, indsj_12, indsk_12 = np.where((inputGains > DGL2) & (inputAnalog <= AnaOff))
    procImg[indsi_12, indsj_12, indsk_12] = _analogMax_HighGain[indsi_12, indsj_12, indsk_12].copy()

    return (procImg, mask)

def moduleCorrectionTensor_MultiProcFunc(queue, rawImg, GainLevel, GainLevelStds, 
                            GainSTDLambda, AnalogOffset, RelativeGain, badPixelMask,
                            imgCnt, rawCellId, analogMax_HighGain):
    procImg, mask = moduleCorrectionTensor(rawImg, GainLevel, GainLevelStds, 
                            GainSTDLambda, AnalogOffset, RelativeGain, badPixelMask,
                            rawCellId, analogMax_HighGain)
    queue.put(list([imgCnt, procImg, mask]))
    
def AGIPDCorrectTensor_multiprocessing(dataSet_all, 
                                        cellidSet_all, 
                                        GainLevel, 
                                        GainLevelStds, 
                                        GainSTDLambda, 
                                        AnalogOffset, 
                                        RelativeGain, 
                                        badPixelMask,
                                        analogMax_HighGain):
    f_N = dataSet_all.shape[0]
    r_N = dataSet_all.shape[2]
    c_N = dataSet_all.shape[3]
    Proc = np.zeros((f_N, r_N, c_N), dtype='float32')
    Mask = np.zeros((f_N, r_N, c_N), dtype='uint8')
    queue = Queue()
    mycpucount = cpu_count() - 1
    stride = int(np.floor(f_N/mycpucount)/2)
    print('Making proc: Multiprocessing ' + str(f_N) + ' frames...')
    numProc = f_N
    numWiating = numProc
    numDone = 0
    numBusyCores = 0
    firstProcessed = 0
    while(numDone<numProc):
        if (not queue.empty()):
            numBusyCores -= 1
            qElement = queue.get()
            baseImgNumber = qElement[0]
            f_N_inPart = qElement[1].shape[0]
            rawCellId = (np.squeeze(cellidSet_all[baseImgNumber:baseImgNumber+f_N_inPart])).astype('uint64')
            Proc[baseImgNumber:baseImgNumber+f_N_inPart, :, :] = qElement[1]
            Mask[baseImgNumber:baseImgNumber+f_N_inPart, :, :] = qElement[2]
            numDone += f_N_inPart
            if(firstProcessed==0):
                pBar = textProgBar(numProc-f_N_inPart, title = 'Calculationg Proc')
                firstProcessed = 1
            else:
                pBar.go(f_N_inPart)
            continue

        if((numWiating>0) & (numBusyCores < mycpucount)):
            imgCnt = numProc - numWiating
            stride = np.minimum(stride, numWiating)
            rawCellId = (np.squeeze(cellidSet_all[imgCnt:imgCnt+stride])).astype('uint64')
            rawImg = dataSet_all[imgCnt:imgCnt + stride, :, :, :]
            Process(target = moduleCorrectionTensor_MultiProcFunc, 
                    args=(queue, rawImg, GainLevel, GainLevelStds, 
                            GainSTDLambda, AnalogOffset, RelativeGain, badPixelMask,
                        imgCnt, rawCellId, analogMax_HighGain) ).start()
            numWiating -= stride
            numBusyCores += 1
    del pBar
    return(Proc, Mask)
    
def singleModuleCorrection(modImg, modGainLevel, modGainLevelStds, GainSTDLambda, 
                                   modAnalogOffset, modRelativeGain, 
                                   badPixelMask, modAnalogMax_HighGain):

    inputAnalog = modImg[0, :, :]
    inputGains = modImg[1, :, :]
    
    procImg = np.zeros(inputAnalog.shape, dtype='float32')
    mask = np.ones(inputAnalog.shape, dtype='uint8')
    
    AnaOff = modAnalogOffset[0, :, :]
    badMask = badPixelMask[0, :, :]
    relGain = modRelativeGain[0, :, :]
    _tmp = (inputAnalog - AnaOff)*relGain
    del AnaOff

    DGL1 = modGainLevel[0, :, :] + GainSTDLambda*modGainLevelStds[0, :, :]
    indsj_1, indsk_1 = np.where(inputGains < DGL1)
    procImg[indsj_1, indsk_1] = _tmp[indsj_1, indsk_1].copy()
    mask[indsj_1, indsk_1] = badMask[indsj_1, indsk_1].copy()
    del indsj_1
    del indsk_1
    
    DGL2 = modGainLevel[1, :, :] + GainSTDLambda*modGainLevelStds[1, :, :]
    _modAnalogMax_HighGain = modAnalogMax_HighGain
    indsj_12, indsk_12 = np.where((DGL1 <= inputGains) & 
                                            (inputGains <= DGL2) & 
                                            (_tmp > _modAnalogMax_HighGain))
    procImg[indsj_12, indsk_12] = _tmp[indsj_12, indsk_12].copy()
    mask[indsj_12, indsk_12] = badMask[indsj_12, indsk_12].copy()
    
    indsj_12, indsk_12 = np.where((DGL1 <= inputGains) & 
                                            (inputGains <= DGL2) & 
                                            (_tmp <= _modAnalogMax_HighGain))
    procImg[indsj_12, indsk_12] = _modAnalogMax_HighGain[indsj_12, indsk_12].copy()
        
    del DGL1    
    indsj_2, indsk_2 = np.where(inputGains > DGL2)
    AnaOff = modAnalogOffset[1, :, :]
    badMask = badPixelMask[1, :, :]
    relGain = modRelativeGain[1, :, :]
    _tmp = (inputAnalog - AnaOff)*relGain
    
    procImg[indsj_2, indsk_2] = _tmp[indsj_2, indsk_2].copy()
    mask[indsj_2, indsk_2] = badMask[indsj_2, indsk_2].copy()

    indsj_12, indsk_12 = np.where((inputGains > DGL2) & (inputAnalog <= AnaOff))
    procImg[indsj_12, indsk_12] = _modAnalogMax_HighGain[indsj_12, indsk_12].copy()

    return (procImg, mask)
