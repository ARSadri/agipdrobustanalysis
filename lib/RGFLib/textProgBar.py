"""
While I respect all the codes and methods on web that use \r for this task,
there are cases such as simple ssh terminals that do not support \r.
In such cases, if something is written at the end of the line it cannot be deleted.
The following code provides a good looking simple title for the progress bar
and shows the limits and is very simple to use.

define the object with length of the for loop, call its go funciton 
every time in the loop and delete the object afterwards.

"""
import time

class textProgBar:
    def __init__(self, length, numTicks = 70 , title = 'progress'):
        self.startTime = time.time()
        self.ck = 0
        self.prog = 0
        self.length = length
        if(numTicks < len(title) + 2 ):
            self.numTicks = len(title)+2
        else:
            self.numTicks = numTicks
        print(' ', end='')
        for _ in range(self.numTicks):
            print('_', end='')
        print(' ', flush = True)

        print('/', end='')
        for idx in range(self.numTicks - len(title)):
            if(idx==int((self.numTicks - len(title))/2)):
                print(title, end='')
            else:
                print(' ', end='')
        print(' \\')
        print(' ', end='')
    def go(self, ck=1):
        self.ck += ck
        cProg = int(self.numTicks*self.ck/self.length/3)
        while (self.prog < cProg):
            self.prog += 1
            remTimeS = self.startTime + (time.time() - self.startTime)/(self.ck/self.length) - time.time()
            if(remTimeS>=3600):
                progStr = "%02d" % int(remTimeS/3600)
                print(progStr, end='')
                print('h', end='', flush = True)
            elif(remTimeS>=60):
                progStr = "%02d" % int(remTimeS/60)
                print(progStr, end='')
                print('m', end='', flush = True)
            else:
                progStr = "%02d" % int(remTimeS)
                print(progStr, end='')
                print('s', end='', flush = True)
    
    def __del__(self):
        print('\n ', end='')
        for _ in range(self.numTicks):
            print('~', end='')
        print(' ', flush = True)

def test_for_textProgBar():
    pBar = textProgBar(180)
    for _ in range(60):
        for _ in range(10000000):
            pass
        pBar.go(3)
    del pBar

if __name__ == '__main__':
    test_for_textProgBar()
    print('simple...')