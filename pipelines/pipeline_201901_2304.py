import os
import subprocess
import fnmatch
import time
import numpy as np
import sbatchMonitoring as sMon
np.set_printoptions(suppress=True)
np.set_printoptions(precision=2)
#################################################
#################################################

#export PYTHONPATH="/home/sadriali/Projects/AGIPDAnalysisPipeline/lib"

FLAG_rm_SLURM            = True
#Constants by the dark
FLAG_CalibByDarkAnalyser = True
FLAG_CalibByDarkJoiner   = True
#Bad pixels by the dark    False
FLAG_MaskByDarkAnalyser  = True
FLAG_MaskByDarkJoiner    = True
FLAG_MaskByDarkMaker     = True
#Bad pixel mask by water   False
FLAG_rm_WATER            = True
FLAG_MapsByWaterAnalyser = True
FLAG_MapsByWaterJoiner   = True
#to create a NN dataset    True
FLAG_nonHitsAnalyser     = False
FLAG_nonHitsJoiner       = False
#Peak and hit finding      False
FLAG_rm_RPF              = True
FLAG_RPF_Analyser        = True
FLAG_rm_CXI              = True
remakeCXIsThatExist_FLAG = True
FLAG_RPF_Joiner          = True
#Indexing                  True
FLAG_IndexingFListMaker  = False
FLAG_rm_indexingFolder   = False
FLAG_USE_RPF             = False
FLAG_SPECIFICINDEXOR     = False
reIndexExisiting_FLAG    = False
FLAG_Indexing            = False
#Partialator               False
FLAG_StichingStreams     = False
FLAG_Partialator         = False
                           
if(FLAG_SPECIFICINDEXOR==True):
    specificIndexor      = 'xgandalf'
    
slurmPartition = 'upex'

#2450's
SPBorMID = 'SPB'
ExperimentDate = '201901'
proposalNumber = 'p002304'
numMemCells = 250
runNumDarkHighGain  = 128
runNumDarkMediumGain = 129
runNumDarkLowGain = 130
waterRun = 108
HIFF_rawDataFolder = '/gpfs/cfel/cxi/scratch/user/sadriali/data_from_xfel_p900145/'
#NNHitFinder
numAdverseriesPerASIC = 2

number_of_parts_to_process = 2 #used for calibration 2*256 data points each
number_of_modules_to_process = 16
maxNumberFramesInAnEpisode = 100000
moduleRowPixN= 512
moduleClmPixN = 128
AGIPD_ASICRows = 64
AGIPD_ASICClms = 128

darkSNR = 6.0
darkSNR_prob = 1.35e-3 #CDF(3.0) = 13e-3 # with his probability, a pixel has triped to beyond darkSNR
separabilityBetweenStages_Threshold = 8.0
offsetSNR = 6.0
filterLength = 32
minimumIslandSize = 64

number_of_parts_to_process_by_water = 3
WaterSNR = 6.0
waterSNR_prob = 0.25

RPF_list_of_runs = list([108])#list([108, 109, 110, 118])
number_of_parts_for_RPF = 1#1000
FLAG_Make_FullProc_Mask = 0
FLAG_JustCalib_nd_Mask = 0
FLAG_Use_XFEL_Proc = 0
withOrNoMaskFlag = 'withMask' #'withMask' or 'noMask'
RPF_bckSNR = 6.0            # DON'T TOUCH THIS
peakAcceptableSNR = 6.0     # TOUCH THIS
dataReductionSNR = 6.0      # ITS NOT RECOMMENDED TO TOUCH THIS
HIT_THRESHOLD = 0
dropMaskedHitsThreshold = 0  #if masked portion is more than this, the hit will be dropped
numHitsInSubpart = 1000
minimumResolution = 0
maximumResolution = 0
maxNumberOfImagesInEuXFEL = 100000
includeData = 0
###############################
########## Folders ############
###############################

projectName = '_robustpeakfinder8_'
#projectName = '_RPF_AlirezaTightMask_withRingMask_2pix_SNR6'
# /gpfs/exfel/exp/SPB/202030/p900119/scratch/alireza
#AGIPD facts
rootFolder = '/gpfs/exfel/exp/'+SPBorMID+'/'+ExperimentDate+'/'+proposalNumber+'/'
rawDataFolder = rootFolder + 'raw/'
procDataFolder = rootFolder + 'proc/'
scratchFolder = rootFolder + 'scratch/marjan/'
NNdatasetFolder = scratchFolder + 'dataset/'
calibFolder = scratchFolder + 'calibData/'
calibFile = calibFolder + 'Cheetah-AGIPD-calib.h5'
maskFile = calibFolder + 'bad-pixel-mask.h5'
badDataFileName = calibFolder + 'AGIPB-BadMaskData.h5'
geomFilePath = scratchFolder + 'dep/agipd_2450_v9.geom'
cellFilePath = scratchFolder + 'dep/1VDS.cell'
streamsFolder = scratchFolder + '/results/streams'+projectName+'/'
RPFProcFolder = scratchFolder + 'RPFProc'+projectName+'/'
RPFCXIDIR = scratchFolder + 'RPFCXI'+projectName+'/'
indexingDIR = scratchFolder + 'indexing'+projectName+'/'
rootEntry = '/INSTRUMENT/'+SPBorMID+'_DET_AGIPD1M-1/DET/'
partialatorFolder = streamsFolder
#######################################################################################
################################### The pipeline starts here ##########################
#######################################################################################

print('The script started at ' + str(time.time()))
if(FLAG_RPF_Analyser==False):
    FLAG_rm_RPF = False
if(FLAG_RPF_Joiner==False):
    FLAG_rm_CXI = False
if(FLAG_Indexing==False):
    FLAG_rm_indexingFolder = False
if(FLAG_rm_SLURM==True):
    os.system('rm slurm*')
    os.system('rm -rf indexamajig.*')

jobIDs = []

if(FLAG_CalibByDarkAnalyser==True):
    print('Calling AGIPD calibration constants analyser for each module')
    minNumCPUs = 71
    maxNumCPUs = 71
    commandOptionsList = []
    shellCommand = 'sbatch --partition='+slurmPartition+' --nodes=1 --time=00:30:00 --mincpus=' + str(minNumCPUs)
    for runCnt in [runNumDarkHighGain, runNumDarkMediumGain, runNumDarkLowGain]:
        runCntStr = "%04d" % runCnt
        WriteDIR = calibFolder + 'r' + runCntStr + '/'
        os.system('rm -rf ' + WriteDIR)
        os.system('mkdir -p ' + WriteDIR)
        for moduleCnt in range(number_of_modules_to_process):
            commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                             'python3' + ' ' + \
                             './../CalibConstantsMaker/CalibByDarkAnalyser.py' + ' ' + \
                             rawDataFolder + ' ' + \
                             rootEntry + ' ' + \
                             WriteDIR + ' ' + \
                             str(numMemCells) + ' ' + \
                             str(moduleRowPixN) + ' ' +  \
                             str(moduleClmPixN) + ' ' + \
                             str(maxNumberFramesInAnEpisode) + ' ' + \
                             str(runCnt) + ' ' + \
                             str(moduleCnt) + ' ' + \
                             str(number_of_parts_to_process) + ' ' + \
                             str(maxNumCPUs) + ' ' + \
                             str(AGIPD_ASICRows) + ' ' + \
                             str(AGIPD_ASICClms) + ' ' + \
                             str(darkSNR)
            commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)
    
if(FLAG_CalibByDarkJoiner==True):
    print('Calling AGIPD calibration constants Joiner to creat Cheetah compatible constants')
    minNumCPUs = 71
    commandOptionsList = []
    shellCommand = 'sbatch --partition='+slurmPartition+' --nodes=1 --time=00:30:00 --mincpus=' + str(minNumCPUs)
    commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                     'python3' + ' ' + \
                     './../CalibConstantsMaker/CalibByDarkJoiner.py' + ' ' + \
                     calibFolder + ' ' + \
                     str(number_of_modules_to_process) + ' ' + \
                     str(numMemCells) + ' ' + \
                     str(moduleRowPixN) + ' ' + \
                     str(moduleClmPixN) + ' ' + \
                     str(runNumDarkHighGain) + ' ' + \
                     str(runNumDarkMediumGain) + ' ' + \
                     str(runNumDarkLowGain) + ' ' + \
                     badDataFileName
    commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)

if(FLAG_MaskByDarkAnalyser==True):
    print('Calling AGIPD mask by dark analyser for each module')
    minNumCPUs = 71
    maxNumCPUs = 71
    commandOptionsList = []
    shellCommand = 'sbatch --partition='+slurmPartition+' --nodes=1 --time=00:30:00 --mincpus=' + str(minNumCPUs)
    for runCnt in [runNumDarkHighGain, runNumDarkMediumGain, runNumDarkLowGain]:
        runCntStr = "%04d" % runCnt
        WriteDIR = calibFolder + 'r' + runCntStr + '/'
        os.system('rm -rf ' + WriteDIR)
        os.system('mkdir -p ' + WriteDIR)
        for moduleCnt in range(number_of_modules_to_process):
            commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                             'python3' + ' ' + \
                             './../maskMaker/MaskByDarkAnalyser.py' + ' ' + \
                             rawDataFolder + ' ' + \
                             rootEntry + ' ' + \
                             WriteDIR + ' ' + \
                             str(numMemCells) + ' ' + \
                             str(moduleRowPixN) + ' ' +  \
                             str(moduleClmPixN) + ' ' + \
                             str(maxNumberFramesInAnEpisode) + ' ' + \
                             str(runCnt) + ' ' + \
                             str(moduleCnt) + ' ' + \
                             str(number_of_parts_to_process) + ' ' + \
                             str(maxNumCPUs) + ' ' + \
                             str(AGIPD_ASICRows) + ' ' + \
                             str(AGIPD_ASICClms) + ' ' + \
                             str(darkSNR)
            commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)
    
if(FLAG_MaskByDarkJoiner==True):
    print('Calling AGIPD mask by dark Joiner')
    minNumCPUs = 71
    commandOptionsList = []
    shellCommand = 'sbatch --partition='+slurmPartition+' --nodes=1 --time=00:30:00 --mincpus=' + str(minNumCPUs)
    commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                     'python3' + ' ' + \
                     './../maskMaker/MaskByDarkJoiner.py' + ' ' + \
                     calibFolder + ' ' + \
                     str(number_of_modules_to_process) + ' ' + \
                     str(numMemCells) + ' ' + \
                     str(moduleRowPixN) + ' ' + \
                     str(moduleClmPixN) + ' ' + \
                     str(runNumDarkHighGain) + ' ' + \
                     str(runNumDarkMediumGain) + ' ' + \
                     str(runNumDarkLowGain) + ' ' + \
                     badDataFileName
    commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)

if(FLAG_MaskByDarkMaker==True):
    print('Calling AGIPD mask maker based on the Dark data')
    minNumCPUs = 71
    commandOptionsList = []
    shellCommand = 'sbatch --partition='+slurmPartition+' --nodes=1 --time=02:00:00 --mincpus=' + str(minNumCPUs)
    commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                     'python3' + ' ' + \
                     './../maskMaker/MaskByDarkMaker.py' + ' ' + \
                     calibFolder + ' ' + \
                     str(AGIPD_ASICRows) + ' ' + \
                     str(AGIPD_ASICClms) + ' ' + \
                     str(separabilityBetweenStages_Threshold) + ' ' + \
                     str(offsetSNR) + ' ' + \
                     str(darkSNR_prob) + ' ' + \
                     str(filterLength) + ' ' + \
                     str(minimumIslandSize) + ' ' + \
                     maskFile + ' ' + \
                     badDataFileName
    commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)

if(FLAG_MapsByWaterAnalyser==True):
    print('Calling maps by Water runs analyser for each module')
    minNumCPUs = 71
    maxNumCPUs = 79
    commandOptionsList = []
    shellCommand = 'sbatch --partition='+slurmPartition+' --nodes=1 --time=12:00:00 --mincpus=' + str(minNumCPUs)
    runCntStr = "%04d" % waterRun
    WriteDIR = calibFolder + 'r' + runCntStr + '/'
    if(FLAG_rm_WATER==True):
        os.system('rm -rf ' + WriteDIR)
        os.system('mkdir -p ' + WriteDIR)
    for moduleCnt in range(number_of_modules_to_process):
        commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                         'python3' + ' ' + \
                         './../maskMaker/MapsByWaterAnalyser.py' + ' ' + \
                         rawDataFolder + ' ' + \
                         rootEntry + ' ' + \
                         WriteDIR + ' ' + \
                         str(numMemCells) + ' ' + \
                         str(moduleRowPixN) + ' ' +  \
                         str(moduleClmPixN) + ' ' + \
                         str(maxNumberFramesInAnEpisode) + ' ' + \
                         str(waterRun) + ' ' + \
                         str(moduleCnt) + ' ' + \
                         str(number_of_parts_to_process_by_water) + ' ' + \
                         str(maxNumCPUs) + ' ' + \
                         str(AGIPD_ASICRows) + ' ' + \
                         str(AGIPD_ASICClms) + ' ' + \
                         str(WaterSNR) + ' ' + \
                         calibFile + ' ' + \
                         maskFile + ' ' + \
                         str(waterSNR_prob)
        commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)

if(FLAG_MapsByWaterJoiner==True):
    print('Calling AGIPD water based joiner')
    minNumCPUs = 71
    commandOptionsList = []
    shellCommand = 'sbatch --partition='+slurmPartition+' --nodes=1 --time=00:10:00 --mincpus=' + str(minNumCPUs)
    runCntStr = "%04d" % waterRun
    WriteDIR = calibFolder + 'r' + runCntStr + '/'
    commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                     'python3' + ' ' + \
                     './../maskMaker/MapsByWaterJoiner.py' + ' ' + \
                     calibFolder + ' ' + \
                     str(number_of_modules_to_process) + ' ' + \
                     str(numMemCells) + ' ' + \
                     str(moduleRowPixN) + ' ' + \
                     str(moduleClmPixN) + ' ' + \
                     str(waterRun) + ' ' + \
                     maskFile
    commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)

if(FLAG_nonHitsAnalyser==True):
    print('Calling non hit background pattern analyser')
    minNumCPUs = 71
    maxNumCPUs = 71
    commandOptionsList = []
    shellCommand = 'sbatch --partition='+slurmPartition+' --nodes=1 --time=00:30:00 --mincpus=' + str(minNumCPUs)
    runCntStr = "%04d" % waterRun
    WriteDIR = NNdatasetFolder + 'r' + runCntStr + '/'
    os.system('rm -rf ' + WriteDIR)
    os.system('mkdir -p ' + WriteDIR)
    for moduleCnt in range(number_of_modules_to_process):
        commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                         'python3' + ' ' + \
                         './../NNHitFinder/nonHitsByWaterAnalyser.py' + ' ' + \
                         rawDataFolder + ' ' + \
                         rootEntry + ' ' + \
                         WriteDIR + ' ' + \
                         str(numMemCells) + ' ' + \
                         str(moduleRowPixN) + ' ' +  \
                         str(moduleClmPixN) + ' ' + \
                         str(maxNumberFramesInAnEpisode) + ' ' + \
                         str(waterRun) + ' ' + \
                         str(moduleCnt) + ' ' + \
                         str(number_of_parts_to_process_by_water) + ' ' + \
                         str(maxNumCPUs) + ' ' + \
                         str(AGIPD_ASICRows) + ' ' + \
                         str(AGIPD_ASICClms) + ' ' + \
                         str(WaterSNR) + ' ' + \
                         calibFile + ' ' + \
                         maskFile + ' ' + \
                         str(waterSNR_prob) + ' ' + \
                         str(numAdverseriesPerASIC)
        commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)        

if(FLAG_nonHitsJoiner==True):
    print('Calling AGIPD water based joiner')
    minNumCPUs = 39
    commandOptionsList = []
    shellCommand = 'sbatch --partition='+slurmPartition+' --nodes=1 --time=00:10:00 --mincpus=' + str(minNumCPUs)
    runCntStr = "%04d" % waterRun
    WriteDIR = NNdatasetFolder + 'r' + runCntStr + '/'
    commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                     'python3' + ' ' + \
                     './../NNHitFinder/nonHitsByWaterJoiner.py' + ' ' + \
                     WriteDIR + ' ' + \
                     str(number_of_modules_to_process) + ' ' + \
                     str(numMemCells) + ' ' + \
                     str(moduleRowPixN) + ' ' + \
                     str(moduleClmPixN) + ' ' + \
                     str(waterRun) + ' ' + \
                     maskFile
    commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)

if(FLAG_RPF_Analyser==True):
    os.system('python3 sbatchMonitoring.py')
    print('Calling Robust Peak-finder for each module')
    minNumCPUs = 71
    commandOptionsList = []
    shellCommand = 'sbatch --partition='+slurmPartition #+ ' --nodes=1 --time=00:30:00 --mincpus=' + str(minNumCPUs)
    for runNumber in RPF_list_of_runs:
        runNumberStr = "%04d" % runNumber
        if(FLAG_Use_XFEL_Proc==0):
            runDataFolder = rawDataFolder + 'r' + runNumberStr + '/'
        else:
            runDataFolder = procDataFolder + 'r' + runNumberStr + '/'        
        RPFProc = RPFProcFolder + 'r' + runNumberStr + '/'
        if(FLAG_rm_RPF==True):
            os.system('rm -rf ' + RPFProc)
        if(not os.path.isdir(RPFProc)):
            os.system('mkdir -p ' + RPFProc)
        fileNameTemplate = '*AGIPD00*.h5'
        flist = fnmatch.filter(os.listdir(runDataFolder), fileNameTemplate)
        numParts = np.minimum(len(flist), number_of_parts_for_RPF)
        print('number of parts to be analysed: ' + str(numParts))
        for partCnt in range(numParts):  #P3 M3 f30500
            partCntStr = "%05d" % partCnt
            numMods = number_of_modules_to_process
            for modCnt in range(numMods):
                modCntStr = "%02d" % modCnt
                if(FLAG_Use_XFEL_Proc==1):
                    dataFileName = runDataFolder+'CORR-R'+runNumberStr+'-AGIPD'+modCntStr+'-S'+partCntStr+'.h5'
                else:
                    dataFileName = runDataFolder+'RAW-R'+runNumberStr+'-AGIPD'+modCntStr+'-S'+partCntStr+'.h5'
                outputFileName = RPFProc+'CORR-R'+runNumberStr+'-AGIPD'+modCntStr+'-S'+partCntStr+'.h5'
                if(os.path.isfile(outputFileName)):
                    continue
                commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                                 'python3' + ' ' + \
                                 './../RobustHitFinder/RPF_Analyser.py' + ' ' + \
                                 dataFileName + ' ' + \
                                 rootEntry + ' ' + \
                                 calibFile + ' ' + \
                                 maskFile + ' ' + \
                                 outputFileName + ' ' + \
                                 str(RPF_bckSNR) + ' ' + \
                                 str(FLAG_Make_FullProc_Mask) + ' ' + \
                                 withOrNoMaskFlag + ' ' + \
                                 str(FLAG_Use_XFEL_Proc) + ' ' + \
                                 str(includeData)
                commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)

if(FLAG_RPF_Joiner==True):
    #os.system('python3 sbatchMonitoring.py')
    print('Calling Joiners....')
    minNumCPUs = 39
    commandOptionsList = []
    shellCommand = 'sbatch --partition upex --nodes 1 --time 0-02:00:00 --mincpus '+ \
                        str(minNumCPUs)
    for runNumber in RPF_list_of_runs:
        runNumberStr = "%04d" % runNumber
        if(FLAG_Use_XFEL_Proc==0):
            runDataFolder = rawDataFolder + 'r' + runNumberStr + '/'
        else:
            runDataFolder = procDataFolder + 'r' + runNumberStr + '/'
        RPFProc = RPFProcFolder + 'r' + runNumberStr + '/'
        fileNameTemplate = '*AGIPD00*.h5'
        flist = fnmatch.filter(os.listdir(runDataFolder), fileNameTemplate)
        numParts = np.minimum(len(flist), number_of_parts_for_RPF)
        print('number of parts to be joined: ' + str(numParts))
        RPFCXIFolder = RPFCXIDIR + 'r' + runNumberStr + '/'
        if(FLAG_rm_CXI==True):
            os.system('rm -rf ' + RPFCXIFolder)
        if(not os.path.isdir(RPFCXIFolder)):
            os.system('mkdir -p ' + RPFCXIFolder)
        for partCnt in range(numParts):
            partCntStr = "%05d" % partCnt
            outputFileName = RPFCXIFolder + 'part_' + partCntStr + '.cxi'
            outputList = fnmatch.filter(os.listdir(RPFCXIFolder), 'part_' + partCntStr + '*.cxi')
            if(remakeCXIsThatExist_FLAG==False):
                if(len(outputList)>0):
                    continue
            commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                             'python3' + ' ' + \
                             './../RobustHitFinder/RPF_VCXIMaker.py' + ' ' + \
                             RPFProc + ' ' + \
                             outputFileName + ' ' + \
                             partCntStr + ' ' + \
                             str(HIT_THRESHOLD) + ' ' + \
                             str(peakAcceptableSNR) + ' ' + \
                             str(dataReductionSNR) + ' ' + \
                             rootEntry + ' ' + \
                             str(maxNumberOfImagesInEuXFEL) + ' ' + \
                             str(numHitsInSubpart) + ' ' + \
                             str(minimumResolution) + ' ' + \
                             str(maximumResolution) + ' ' + \
                             geomFilePath + ' ' + \
                             str(FLAG_JustCalib_nd_Mask) +' ' + \
                             str(includeData)
            commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)

if(FLAG_IndexingFListMaker==True):
    os.system('python3 sbatchMonitoring.py')
    print('Calling file list maker....')
    oneFListForEachCXIFlag = True
    minNumCPUs = 39
    commandOptionsList = []
    shellCommand = 'sbatch --partition=upex'
    for runNumber in RPF_list_of_runs:
        runNumberStr = "%04d" % runNumber
        runDataFolder = rawDataFolder + 'r' + runNumberStr + '/'
        RPFCXIFolder = RPFCXIDIR + 'r' + runNumberStr + '/'
        commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                         'python3' + ' ' + \
                         './../indexing/flistMakerPerRun.py ' + \
                         RPFCXIFolder + ' ' + \
                         str(oneFListForEachCXIFlag)
        commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)
            
if(FLAG_Indexing==True):
    print('Calling Crystfel (optionally peakfinder8 and) indexamajig....')
    os.system('python3 sbatchMonitoring.py')
    minNumCPUs = 71
    commandOptionsList = []
    shellCommand = 'sbatch --partition upex --nodes 1 --time 0-24:00:00 --mincpus ' + str(minNumCPUs)
    for runNumber in RPF_list_of_runs:
        runNumberStr = "%04d" % runNumber
        runDataFolder = rawDataFolder + 'r' + runNumberStr + '/'
        RPFCXIFolder = RPFCXIDIR + 'r' + runNumberStr + '/'
        indexingFolder = indexingDIR + 'r' + runNumberStr + '/'
        if(FLAG_rm_indexingFolder==True):
            os.system('rm -rf ' + indexingFolder)
        if(not os.path.isdir(indexingFolder)):
            os.system('mkdir -p ' + indexingFolder)
        fileNameTemplate = '*.lst'
        flist = fnmatch.filter(os.listdir(RPFCXIFolder), fileNameTemplate)
        flist.sort()
        numFiles = len(flist)
        for fileCnt in range(numFiles):
            infileName = flist[fileCnt]
            outfileName = 'indexing_' + infileName[:-4] + '.stream'
            if(reIndexExisiting_FLAG==False):
                if(os.path.isfile(indexingFolder+outfileName)):
                    if(os.stat(indexingFolder+outfileName).st_size > 500000):
                        continue
            commandOptions = './anyCommandByBash.sh' + ' ' + \
                             'indexamajig' + ' ' + \
                             '-i '+RPFCXIFolder+infileName + ' ' + \
                             '-g '+geomFilePath + ' ' + \
                             '-o ' + indexingFolder + outfileName + ' ' + \
                             '-j 80' + ' ' + \
                             '--no-revalidate --no-check-peaks' + ' ' + \
                             '--int-radius=2,4,7' + ' ' + \
                             '-p ' + cellFilePath + ' '  
            if(FLAG_USE_RPF):
                commandOptions += '--peaks=cxi'
            else:
                commandOptions += '--serial-start=1' + ' ' + \
                                  '--peaks=peakfinder8' + ' ' + \
                                  '--min-snr=6' + ' ' + \
                                  '--threshold=100' + ' ' + \
                                  '--min-pix-count=1' + ' ' + \
                                  '--max-pix-count=30' + ' ' + \
                                  '--min-peaks=15' + ' ' + \
                                  '--local-bg-radius=8'
                                          
            if(FLAG_SPECIFICINDEXOR == True):
                commandOptions += ' --indexing=' + specificIndexor
            commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)
    
if(FLAG_StichingStreams==True):
    os.system('python3 sbatchMonitoring.py')
    print('Calling stitching streams....')
    minNumCPUs = 39
    commandOptionsList = []
    shellCommand = 'sbatch --partition='+slurmPartition + \
                   ' --nodes=1 --mincpus=' + str(minNumCPUs)
    
    if(not os.path.isdir(streamsFolder)):
        os.system('mkdir -p ' + streamsFolder)
    commandOptions = ' ./anyCommandByBash.sh' + ' ' + \
                     'python3' + ' ' + \
                     './../indexing/streamStitcher.py' + ' ' + \
                      indexingDIR + ' ' + \
                      streamsFolder + 'experiment.stream'
    
    commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)

if(FLAG_Partialator==True):
    os.system('python3 sbatchMonitoring.py')
    print('Calling Crystfel partialator....')
    minNumCPUs = 79
    commandOptionsList = []
    shellCommand = 'sbatch --partition upex --nodes 1 --time 0-48:00:00 --mincpus ' + str(minNumCPUs)
    fileNameTemplate = '*.stream'
    print('partialatorFolder -> ' + partialatorFolder)
    flist = fnmatch.filter(os.listdir(partialatorFolder), fileNameTemplate)
    flist.sort()
    numFiles = len(flist)
    for fileCnt in range(numFiles):
        fileName = flist[fileCnt]
        commandOptions = './anyCommandByBash.sh' + ' ' + \
                         'partialator' + ' ' + \
                         '-i ' + partialatorFolder + fileName + ' ' + \
                         '-o ' + partialatorFolder + fileName[:-6] + 'hkl' + ' ' + \
                         '-y 4/mmm' + ' ' + \
                         '--min-res=3' + ' ' + \
                         '--push-res=1.0' + ' ' + \
                         '--no-logs' + ' ' + \
                         '--iterations=3' + ' ' + \
                         '--model=unity' + ' ' + \
                         '-j 80'
        commandOptionsList.append(commandOptions)
    jobDependencies = sMon.createjobDependenciesByList(jobIDs.copy())
    jobIDs = sMon.sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies)