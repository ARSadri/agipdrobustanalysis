#!/bin/zsh
source /etc/profile.d/modules.sh
module load anaconda3
module load hdf5/1.10.5
module load crystfel
module load ccp4
export PYTHONPATH="./../lib"
$*