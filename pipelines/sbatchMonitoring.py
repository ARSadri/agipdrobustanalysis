#!/usr/bin/python3
import os
import subprocess
import time
import numpy as np
import pyslurm
import pwd
import sys
import getpass

np.set_printoptions(suppress=True)
np.set_printoptions(precision=2)
    
def jobSubmit(shellCommand, jobDependencies, commandOptions):
    _tmpCommand = shellCommand
    if(len(jobDependencies)>0):
        _tmpCommand += ' --dependency=afterok' + jobDependencies
    _tmpCommand += ' ' + commandOptions
    _jID = subprocess.check_output(_tmpCommand, shell=True)
    _jID = [int(s) for s in _jID.split() if s.isdigit()]
    _jID = _jID[0]
    print(str(_jID) + '->' + _tmpCommand)
    return _jID
    
def sbatchMonitoring(shellCommand, commandOptionsList, jobDependencies):
    jobIDs = np.zeros(len(commandOptionsList), dtype='int')
    for idx, commandOptions in enumerate(commandOptionsList):
        jobIDs[idx] = jobSubmit(shellCommand, jobDependencies, commandOptions)
    time_time = time.time()
    print('waiting for 5 seconds....')
    while(time.time() - time_time<5):
        pass
    return(jobIDs)

def createjobDependenciesByList(jobIDs):
    jobDependencies = ''
    for aJobKey in jobIDs:
        jobDependencies += ':' + str(int(aJobKey))
    return jobDependencies    
    
##############################################################################
##########             function for monitoring                   #############
##############################################################################

def waitFunc(Wtime): 
    time_time = time.time()
    while((time.time() - time_time)<=Wtime):
        pass

def getUID(MYUSER):
    uid = pwd.getpwnam(MYUSER)
    return(uid[2])
        
def createjobDependencies(uid):
    aJob = pyslurm.job()
    try:
        jobDict = aJob.find_user(uid)
    except:
        print('no jobs are in the queue')
        return

    aJobKeys = list((jobDict).keys())
    jobDependencies = ''
    jobIDs = np.zeros(len(aJobKeys), dtype='int')
    for idx, aJobKey in enumerate(aJobKeys):
        jobInfo = aJob.find_id(aJobKey)
        _jobInfo = jobInfo[0]
        jobState = _jobInfo['job_state']
        if((jobState != 'FAILED') & (jobState != 'COMPLETED')):
            jobDependencies = jobDependencies + ':' + str(int(aJobKey))
            jobIDs[idx] = aJobKey
     
    return(jobDependencies, jobIDs[jobIDs>0])  
    
def reportjobs():
    MYUSER = getpass.getuser()
    uid = getUID(MYUSER)
    print('Looking for status of jobs of ' + MYUSER)
    _, jobIDs = createjobDependencies(uid)
    jobIDs = jobIDs.copy()
    aJob = pyslurm.job()
    flags = np.zeros(jobIDs.shape[0])
    print('PD \t R \t F \t C \t T(s)')
    time_time = time.time()
    while((flags<2).any()):
        for idx in range(jobIDs.shape[0]):
            if(flags[idx]<2):
                aJobKey = jobIDs[idx]
                try:
                    jobInfo = aJob.find_id(int(aJobKey))
                except:
                    print('job ' + str(int(aJobKey)) + ' is removed!')
                    flags[idx] = 3
                    continue
                _jobInfo = jobInfo[0]
                jobState = _jobInfo['job_state']
                if(jobState == 'PENDING'):
                    flags[idx] = 0
                if(jobState == 'RUNNING'):
                    flags[idx] = 1
                if(jobState == 'FAILED'):
                    flags[idx] = 2
                    print('...................FAILED ................')
                    print(_jobInfo['std_out'])
                    print(_jobInfo['command'])
                    print('==========================================')
                if(jobState == 'COMPLETED'):
                    flags[idx] = 3
                if(jobState == 'CANCELLED'):
                    flags[idx] = 4
        print(str(int((flags==0).sum()))+' \t ' \
             +str(int((flags==1).sum()))+' \t ' \
             +str(int((flags==2).sum()))+' \t ' \
             +str(int((flags==3).sum()))+' \t ' \
             +str(int(time.time() - time_time)))
        waitFunc(10)
    print('No more jobs to report about...')
    
if __name__ == '__main__':
    reportjobs()