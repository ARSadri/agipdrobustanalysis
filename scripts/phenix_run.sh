source /etc/profile.d/modules.sh
module load phenix/1.13
module load xray

cp ./../../dep/create-mtz-422 .
chmod +x create-mtz-422
./create-mtz-422 experiment.hkl

cp ./../../dep/6ftr.pdb .
cp ./../../dep/6FTR.mtz .
cp ./../../dep/parameter_lys ./parameter_lys_experiment.eff

sed -i "s/output_file = N/output_file = rfree_lys_experiment.mtz/g" parameter_lys_experiment.eff
sed -i "s/file_name = M/file_name = experiment.mtz/g" parameter_lys_experiment.eff
iotbx.reflection_file_editor parameter_lys_experiment.eff
cp ./../../dep/refine_param_lys ./refine_param_lys_experiment.eff
sed -i "s/refine_NXC/refine_param_lys_experiment/g" refine_param_lys_experiment.eff

phenix.refine rfree_lys_experiment.mtz 6ftr.pdb refine_param_lys_experiment.eff  xray_data.high_resolution=1.63 output.n_resolution_bins=20 --unused_ok


