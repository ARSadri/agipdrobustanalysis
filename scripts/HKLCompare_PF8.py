import numpy as np
import matplotlib.pyplot as plt

def read_rcompareHKL_DAT_File(fileName):
    with open(fileName) as f:
        f.readline()
        array = [[float(x) for x in line.split()] for line in f]
        return array

def plotTheCurves(_ax, legendLabel, File, resClmInd, valClmInd, statLabel, _color, resL, resH, marker):
    data = np.array(read_rcompareHKL_DAT_File(File))
    inds = np.where((data[:, resClmInd]>=resL) & (data[:, resClmInd]<=resH))[0]
    _ax.plot(data[inds, resClmInd], data[inds, valClmInd], '.-', color=_color, linewidth=LWidth, label=statLabel+legendLabel, marker=marker)
    
LWidth = 3

font = {
        'weight' : 'bold',
        'size'   : 8}

plt.rc('font', **font)

params = {'legend.fontsize': 'x-large',
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}
plt.rcParams.update(params)

fig, ax1 = plt.subplots()
ax1.set_xlabel('resolution', weight='bold')
ax1.set_ylabel('Rsplit', color='black', weight='bold')
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
ax2.set_ylabel('CC*', color='black', weight='bold')
ax1.tick_params(axis='y', labelcolor='black')
ax2.tick_params(axis='y', labelcolor='black')
#best: streams_60s_maskSNR6_bckSNR6_hitSNR6_hitTh10_resLimit800

resultsFolder = '/gpfs/cfel/cxi/scratch/user/sadriali/p0119/'
rsplit_shells = 'rsplit_shells.dat'
ccstar_shells = 'ccstar_shells.dat'
cchalf_shells = 'cchalf_shells.dat'
SNR_shells = 'SNR_shells.dat'

procLabel = 'PF8 no mask'
procFolder = resultsFolder + 'streams_pf8_NoMask_2pix_SNR6/'
resL = 1.65
resH = 2.0
_color = 'red'
plotTheCurves(ax1, procLabel, procFolder + rsplit_shells, 
                3, 1, 'Rsplit: ', _color, resL, resH, marker='s')
plotTheCurves(ax2, procLabel, procFolder + ccstar_shells, 
                3, 1, 'CC*: ', _color, resL, resH, marker='x')

procLabel = 'PF8 old'
procFolder = resultsFolder + 'streams_pf8_AlirezaMask_noRingMask_2pix_SNR8/'
resL = 1.59
resH = 2.0
_color = 'green'
plotTheCurves(ax1, procLabel, procFolder + rsplit_shells, 
                3, 1, 'Rsplit: ', _color, resL, resH, marker='s')
plotTheCurves(ax2, procLabel, procFolder + ccstar_shells, 
                3, 1, 'CC*: ', _color, resL, resH, marker='x')

procLabel = 'RPF new'
procFolder = resultsFolder + 'streams_robustpeakfinder8_3/'
resL = 1.63
resH = 2.0
_color = 'blue'
plotTheCurves(ax1, procLabel, procFolder + rsplit_shells, 
                3, 1, 'Rsplit: ', _color, resL, resH, marker='s')
plotTheCurves(ax2, procLabel, procFolder + ccstar_shells, 
                3, 1, 'CC*: ', _color, resL, resH, marker='x')

procLabel = 'RPF recalib'
procFolder = resultsFolder + 'streams_robustpeakfinder8_recalib0/'
resL = 1.53
resH = 2.0
_color = 'gold'
plotTheCurves(ax1, procLabel, procFolder + rsplit_shells, 
                3, 1, 'Rsplit: ', _color, resL, resH, marker='s')
plotTheCurves(ax2, procLabel, procFolder + ccstar_shells, 
                3, 1, 'CC*: ', _color, resL, resH, marker='x')

fig.tight_layout()  # otherwise the right y-label is slightly clipped
fig.legend(bbox_to_anchor=(0.88, 0.8))
plt.grid()
plt.xlim(resL, resH)
plt.show()

############################################### CCHalf and SNR #########################

plt.rc('font', **font)
plt.rcParams.update(params)
fig, ax1 = plt.subplots()
ax1.set_xlabel('resolution', weight='bold')
ax1.set_ylabel('SNR', color='black', weight='bold')
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
ax2.set_ylabel('CChalf', color='black', weight='bold')
ax1.tick_params(axis='y', labelcolor='black')
ax2.tick_params(axis='y', labelcolor='black')

procLabel = 'PF8 no mask'
procFolder = resultsFolder + 'streams_pf8_NoMask_2pix_SNR6/'
resL = 1.65
resH = 2.0
_color = 'red'
plotTheCurves(ax1, procLabel, procFolder + SNR_shells, 
                9, 6, 'SNR_', _color, resL, resH, marker='s')

plotTheCurves(ax2, procLabel, procFolder + cchalf_shells, 
                3, 1, 'CChalf_', _color, resL, resH, marker='x')

procLabel = 'RPF'
procFolder = resultsFolder + 'streams_pf8_AlirezaMask_noRingMask_2pix_SNR8/'
resL = 1.59
resH = 2.0
_color = 'green'
plotTheCurves(ax1, procLabel, procFolder + SNR_shells, 
                9, 6, 'SNR_', _color, resL, resH, marker='s')
plotTheCurves(ax2, procLabel, procFolder + cchalf_shells, 
                3, 1, 'CChalf', _color, resL, resH, marker='x')

procLabel = 'RPF_new'
procFolder = resultsFolder + 'streams_robustpeakfinder8_3/'
resL = 1.63
resH = 2.0
_color = 'blue'
plotTheCurves(ax1, procLabel, procFolder + SNR_shells, 
                9, 6, 'SNR_', _color, resL, resH, marker='s')
plotTheCurves(ax2, procLabel, procFolder + cchalf_shells, 
                3, 1, 'CChalf', _color, resL, resH, marker='x')

procLabel = 'RPF recalib'
procFolder = resultsFolder + 'streams_robustpeakfinder8_recalib0/'
resL = 1.53
resH = 2.0
_color = 'gold'
plotTheCurves(ax1, procLabel, procFolder + SNR_shells, 
                9, 6, 'SNR_', _color, resL, resH, marker='s')
plotTheCurves(ax2, procLabel, procFolder + cchalf_shells, 
                3, 1, 'CChalf', _color, resL, resH, marker='x')


fig.tight_layout()  # otherwise the right y-label is slightly clipped
fig.legend(bbox_to_anchor=(0.9, 0.65))
plt.grid()
#plt.xlim(resL, resH)
plt.show()
