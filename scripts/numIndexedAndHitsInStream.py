# python3 numIndexedAndHitsInStream.py /gpfs/exfel/exp/SPB/202030/

import sys
import pickle

def procInfoFunc(fileName):

    File_object = open(fileName, "r")
    numImages = 0
    numHits = 0
    numIndexed = 0
    
    for line in File_object:
        if( "indexed_by = " in line):
            numImages += 1
            if( "indexed_by = none" not in line):
                numIndexed += 1
        if( "hit = 1" in line):
            numHits += 1
    return(numImages, numHits, numIndexed)

if __name__ == '__main__':
    file1 = sys.argv[1]
    print('file1 ->' + file1)

    numImages, numHits, numIndexed = procInfoFunc(file1)
    
    print('numImages-> ' + str(numImages))
    print('numHits-> ' + str(numHits))
    print('numIndexed-> ' + str(numIndexed))
