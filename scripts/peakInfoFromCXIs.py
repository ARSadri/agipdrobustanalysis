import os
import h5py
import numpy as np
import matplotlib.pyplot as plt

import cfelpyutils.crystfel_utils as cfelCryst
import cfelpyutils.geometry_utils as cfelGeom

geomFilePath = '/gpfs/exfel/exp/SPB/201901/p002304/scratch/marjan/dep/agipd_2450_v9.geom'
myGeom = cfelCryst.load_crystfel_geometry(geomFilePath)
geomMap = cfelGeom.compute_pix_maps(myGeom)
geomMap_X = geomMap[0]
geomMap_Y = geomMap[1]

runCnt = 118
runCntStr = "%04d" % runCnt
CXIFolder = '/gpfs/exfel/data/scratch/hadianm/p2304/RPFCXI_2304_RPFresults/r'+runCntStr+'/'

############## CXI files entries ##############
dataEntry = '/entry_1/data_1/data'
maskEntry = '/entry_1/data_1/mask'
pksXEntry = '/entry_1/result_1/peakYPosRaw'
pksYEntry = '/entry_1/result_1/peakXPosRaw'
nPeaksEntry = '/entry_1/result_1/nPeaks'
frameNumbersEntry = '/entry_1/result_1/frameNumber'
peakTotalIntensityEntry = 'entry_1/result_1/peakTotalIntensity'
peakSNREntry = 'entry_1/result_1/peakSNR'
peakNPixelsEntry = 'entry_1/result_1/peakNPixels'
TIDEntry = '/instrument/trainID'
PIDEntry = '/instrument/pulseID'
CIDEntry = '/instrument/cellID'

if __name__ == '__main__':
    flist = []
    for subdir, dirs, files in os.walk(CXIFolder):
        for file in files:
            filepath = subdir + os.sep + file
            if filepath.endswith('.cxi'):
                flist.append(filepath)
    flist.sort()
    numFiles = len(flist)
    numHits = 0
    for fileCnt in range(numFiles):
        dataFile = flist[fileCnt]
        print('file is: ' + dataFile)
        dataFile = h5py.File(dataFile,'r')
        h5CID = dataFile[CIDEntry]
        CID = h5CID[...]
        numHits += CID.shape[0]    
        dataFile.close()
    
    print('numFiles -> ' + str(numFiles))
    print('numHits -> ' + str(numHits))
    
    TID = np.zeros((numHits), dtype='uint64')
    PID = np.zeros((numHits), dtype='uint16')
    nPeaksSet = np.zeros((numHits), dtype='uint16')
    PeaksXSet = np.zeros((numHits, 1024), dtype='float32')
    PeaksYSet = np.zeros((numHits, 1024), dtype='float32')
    peakTotalIntensity = np.zeros((numHits, 1024), dtype='float32')
    peakNPixels = np.zeros((numHits, 1024), dtype='uint8')
    peakSNR = np.zeros((numHits, 1024), dtype='float32')
    
    numHitsSoFar = 0
    for fileCnt in range(numFiles):
        dataFilePath = flist[fileCnt]
        dataFile = h5py.File(dataFilePath,'r')
        h5TID = dataFile[TIDEntry]
        h5PID = dataFile[PIDEntry]
        h5CID = dataFile[CIDEntry]
        h5PeaksXSet = dataFile[pksXEntry]
        h5PeaksYSet = dataFile[pksYEntry]
        h5nPeaksSet = dataFile[nPeaksEntry]
        h5peakTotalIntensity = dataFile[peakTotalIntensityEntry]
        h5peakNPixels = dataFile[peakNPixelsEntry]
        h5peakSNR = dataFile[peakSNREntry]
        
        CID = h5CID[...]
        numFramesInPart = CID.shape[0]
        
        TID[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5TID[...]
        PID[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5PID[...]
        PeaksXSet[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5PeaksXSet[...]
        PeaksYSet[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5PeaksYSet[...]
        nPeaksSet[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5nPeaksSet[...]
        peakTotalIntensity[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5peakTotalIntensity[...]
        peakNPixels[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5peakNPixels[...]
        peakSNR[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5peakSNR[...]
        
        numHitsSoFar += numFramesInPart
        dataFile.close()

    TID = TID - TID.min()
    maxTIDIn_mSec = 1000    # for andrew's experiment, 250 memCells are used and the distance is 4ms.
    #There are 10 trains per second, that I don't think changes...
    evTime = (TID*maxTIDIn_mSec+PID)/10*maxTIDIn_mSec
    plt.plot(evTime, nPeaksSet,'.')
    plt.show()

    peaksResolution = np.zeros((numHits, 1024), dtype='float32')
    for frmCnt in range(numHits):
        nPeaks = nPeaksSet[frmCnt]
        _peaksX = PeaksXSet[frmCnt, :nPeaks].astype('int32')
        _peaksY = PeaksYSet[frmCnt, :nPeaks].astype('int32')
        peaksX = geomMap_X[_peaksX, _peaksY]
        peaksY = geomMap_Y[_peaksX, _peaksY]
        peaksResolution[frmCnt, :nPeaks] = (peaksX**2 + peaksY**2)**0.5
    peaksResolution_mask = peaksResolution.copy()
    peaksResolution_mask[peaksResolution_mask!=0]=1
    peaksResolution_min = peaksResolution.copy()
    peaksResolution_min[peaksResolution_mask==0]=10000
    plt.plot(evTime, peaksResolution.max(1),'.')
    plt.plot(evTime, peaksResolution.sum(1)/peaksResolution_mask.sum(1),'.')
    plt.plot(evTime, peaksResolution_min.min(1),'.')
    plt.show()