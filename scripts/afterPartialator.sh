module load crystfel
module load ccp4

check_hkl experiment.hkl -y 4/mmm -p ./../../dep/1VDS.cell --nshells=20 --highres=1.5
mv shells.dat SNR_shells.dat

compare_hkl experiment.hkl1 experiment.hkl2  -y 4/mmm -p ./../../dep/1VDS.cell --fom=rsplit --nshells=20 --highres=1.5
mv shells.dat rsplit_shells.dat

compare_hkl experiment.hkl1 experiment.hkl2  -y 4/mmm -p ./../../dep/1VDS.cell --fom=ccstar --nshells=20 --highres=1.5
mv shells.dat ccstar_shells.dat

compare_hkl experiment.hkl1 experiment.hkl2  -y 4/mmm -p ./../../dep/1VDS.cell --fom=cc --nshells=20 --highres=1.5
mv shells.dat cchalf_shells.dat

cat SNR_shells.dat
cat rsplit_shells.dat
cat ccstar_shells.dat
cat cchalf_shells.dat
