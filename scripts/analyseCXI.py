import os
import h5py
import numpy as np
import matplotlib.pyplot as plt
import cfelpyutils.crystfel_utils as cfelCryst
import cfelpyutils.geometry_utils as cfelGeom

np.set_printoptions(suppress=True)
np.set_printoptions(precision=2)

geomFilePath = '/gpfs/exfel/exp/SPB/202001/p002450/scratch/alireza/agipd_2450_v4.geom'
myGeom = cfelCryst.load_crystfel_geometry(geomFilePath)

runCnt = 90
runCntStr = "%04d" % runCnt
CXIFolder = '/gpfs/exfel/exp/SPB/201901/p002304/scratch/alireza/RPFCXI/r'+runCntStr+'/'
peakInfoFile = CXIFolder + 'peakInfo.h5'
############## CXI files entries ##############
dataEntry = '/entry_1/data_1/data'
maskEntry = '/entry_1/data_1/mask'
pksXEntry = '/entry_1/result_1/peakYPosRaw'
pksYEntry = '/entry_1/result_1/peakXPosRaw'
nPeaksEntry = '/entry_1/result_1/nPeaks'
frameNumbersEntry = '/entry_1/result_1/frameNumber'
peakTotalIntensityEntry = 'entry_1/result_1/peakTotalIntensity'
peakSNREntry = 'entry_1/result_1/peakSNR'
peakNPixelsEntry = 'entry_1/result_1/peakNPixels'
TIDEntry = '/instrument/trainID'
PIDEntry = '/instrument/pulseID'
CIDEntry = '/instrument/cellID'

if __name__ == '__main__':
    flist = []
    for subdir, dirs, files in os.walk(CXIFolder):
        for file in files:
            filepath = subdir + os.sep + file
            if filepath.endswith('.cxi'):
                flist.append(filepath)
    flist.sort()
    numFiles = len(flist)
    numHits = 0
    for fileCnt in range(numFiles):
        dataFilePath = flist[cnt]
        dataFile = h5py.File(dataFilePath,'r')
        h5CID = dataFile[CIDEntry]
        CID = h5CID[...]
        numHits += CID.shape[0]    
    
    TID = np.zeros((numHits), dtype='uint64')
    PID = np.zeros((numHits), dtype='uint16')
    nPeaksSet = np.zeros((numHits), dtype='uint16')
    PeaksXSet = np.zeros((numHits, 1024), dtype='float32')
    PeaksYSet = np.zeros((numHits, 1024), dtype='float32')
    peakTotalIntensity = np.zeros((numHits, 1024), dtype='float32')
    peakNPixels = np.zeros((numHits, 1024), dtype='uint8')
    peakSNR = np.zeros((numHits, 1024), dtype='float32')
    
    numHitsSoFar = 0
    pBar = textProgBar(numFiles):
    for fileCnt in range(numFiles):
        pBar.go()
        dataFilePath = flist[cnt]
        dataFile = h5py.File(dataFilePath,'r')
        h5TID = dataFile[TIDEntry]
        h5PID = dataFile[PIDEntry]
        h5CID = dataFile[CIDEntry]
        h5PeaksXSet = dataFile[pksXEntry]
        h5PeaksYSet = dataFile[pksYEntry]
        h5nPeaksSet = dataFile[nPeaksEntry]
        h5peakTotalIntensity = dataFile[peakTotalIntensityEntry]
        h5peakNPixels = dataFile[peakNPixelsEntry]
        h5peakSNR = dataFile[peakSNREntry]
        
        CID = h5CID[...]
        numFramesInPart = CID.shape[0]
        
        TID[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5TID[...]
        PID[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5PID[...]
        PeaksXSet[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5PeaksXSet[...]
        PeaksYSet[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5PeaksYSet[...]
        nPeaksSet[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5nPeaksSet[...]
        peakTotalIntensity[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5peakTotalIntensity[...]
        peakNPixels[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5peakNPixels[...]
        peakSNR[numHitsSoFar:numHitsSoFar+numFramesInPart] = h5peakSNR[...]
        
        numHitsSoFar += numFramesInPart
    del pBar
    
    print('creating one file:\n '+peakInfoFile)
    file_outData = h5py.File(peakInfoFile, "w")
    file_outData.create_dataset( 'TID', data = TID)
    file_outData.create_dataset( 'PID', data = PID)
    file_outData.create_dataset( 'PeaksX', data = PeaksXSet)
    file_outData.create_dataset( 'PeaksY', data = PeaksYSet)
    file_outData.create_dataset( 'nPeaks', data = nPeaksSet)
    file_outData.create_dataset( 'peakTotalIntensity', data = peakTotalIntensity)
    file_outData.create_dataset( 'peakNPixels', data = peakNPixels)
    file_outData.create_dataset( 'peakSNR', data = peakSNR)

    file_outData.close()