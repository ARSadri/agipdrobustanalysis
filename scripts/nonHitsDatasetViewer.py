import numpy as np
import matplotlib.pyplot as plt

def visSubset(inImages, n_r = None, n_c = None):
    n_F = inImages.shape[0]
    if(n_r is None):
        n_r = int(n_F**0.5)
    if(n_c is None):
        n_c = int(n_F/n_r)
    for i in range(n_F):
        plt.subplot(n_r,n_c,i+1)
        plt.imshow(np.squeeze(inImages[i]))
    plt.show()

if __name__ == '__main__':
    readDIR = '/gpfs/exfel/exp/SPB/201901/p002304/scratch/alireza/nonHitDataset/'
    
    nonHits_Back = np.load(readDIR + 'nonHits_Back.npy')
    nonHits_label = np.load(readDIR + 'nonHits_label.npy')
    nonHits_peak = np.load(readDIR + 'nonHits_peak.npy')
    nonHits_adverse = np.load(readDIR + 'nonHits_adverse.npy')
    
    images = np.zeros((4, nonHits_Back.shape[1], nonHits_Back.shape[2]), dtype='float32')
    
    frmNumber = int(nonHits_Back.shape[0]*np.random.rand(1))
    
    images[0,:,:] = nonHits_Back[frmNumber]
    images[1,:,:] = nonHits_label[frmNumber]
    images[2,:,:] = nonHits_peak[frmNumber]
    images[3,:,:] = nonHits_adverse[frmNumber]
    visSubset(images, 1, 4)
    
    exit()