from RobustPeakFinder import robustPeakFinderPyFunc as RPF
import h5py
import numpy as np
import os
import time
import AGIPDCorrectionRoutine
import matplotlib.pyplot as plt
import cfelpyutils.crystfel_utils as cfelCryst
import cfelpyutils.geometry_utils as cfelGeom
from StreamFileParser import *

from RobustGaussianFittingLibrary import fitBackground, fitValue

#import maskLib
#maskLib.showWhatGeometryDoes(geometryFile)
np.set_printoptions(suppress=True)
np.set_printoptions(precision=2)
os.system('source /etc/profile.d/modules.sh')
os.system('module load anaconda/3')
os.system('module load crystfel')
os.system('module load ccp4')

######################################################################
SPBorMID = 'SPB'
ExperimentDate = '202030'
proposalNumber = 'p900119'
numMemCells = 352
runNumDarkHighGain  = 109
runNumDarkMediumGain = 110
runNumDarkLowGain = 111
lightRun = 97

rootFolder = '/gpfs/exfel/exp/'+SPBorMID+'/'+ExperimentDate+'/'+proposalNumber+'/'
dataFolder = rootFolder + 'raw/'
scratchFolder = rootFolder + 'scratch/alireza/'
calibFolder = scratchFolder + 'calibData/'
calibFile = calibFolder + 'Cheetah-AGIPD-calib.h5'
maskFile = calibFolder + 'bad-pixel-mask.h5'
#calibFile = calibFolder + 'Cheetah-AGIPD-recalib.h5'
#maskFile = calibFolder + 'bad-pixel-mask_ByLight.h5'
geometryFile = scratchFolder + 'dep/agipd_2450_v9.geom'
cellFile = scratchFolder + 'dep/1VDS.cell'
outputFolder = scratchFolder + 'samples/'

rootEntry = '/INSTRUMENT/'+SPBorMID+'_DET_AGIPD1M-1/DET/'
'''
Run_0095_Part_00000_TID_689505085_PID_660_CID_165_frameNumbers_[10725 10725 10725 10725 10725 10373 10725 10725 10725 10725 10725 10725
 10725 10725 10725 10725]
'''
'''
runNumber = 95
partNumber = 3
TID = 689505826
PID = 512
CID = 128
'''
''' 
Run_0096_Part_00019_TID_689516905_PID_468_CID_117_frameNumbers_
[20885 20533 20885 20533 20885 20885 20885 20533 20885 20533 20885 20533 20533 20533 20885 20885]
'''
'''
runNumber = 96
partNumber = 19
TID = 689516905
PID = 468
CID = 117
'''
'''
Run_0097_Part_00000_TID_689519307_PID_348_CID_87_frameNumbers_[8535 8535 8535 8535 8535 8535 8535 8183 8535 8535 8535 8535 8535 8535
 8183 8183]

#Run_0095_Part_00020_TID_689510374_PID_708_CID_177

# too bright: Run_0097_Part_00000_TID_689519283_PID_56_CID_14

#just noise : Run_0097_Part_00000_TID_689519283_PID_72_CID_18

Run_0095_Part_00003_TID_689505826_PID_512_CID_128_frameNumbers_[1184 1184 1184 1184 1184 1184 1184 1184 1184 1184 1184 1184 1184 1184
 1184 1184]

''
runNumber = 95
partNumber = 3
TID = 689505826
PID = 512
CID = 128
'''

#'''
runNumber = 96
partNumber = 1
TID = 689512346
PID = 776
CID = 194
#'''



GainSTDLambda = 6.0
bckSNR = 6.0
numModules = 16
FLAG_WITH_MASK = True

if __name__ == '__main__':
    
    print('Robust PeakFinder Analyser started....', flush = True)
    print('PID ->' + str(os.getpid()))
    time_time = time.time()
    print('Peakfinding started at ' + str(time_time) + 's')
    
    print('rootEntry-> ' + rootEntry)
    print('calibFile-> ' + calibFile)
    print('maskFile-> ' + maskFile)
    print('bckSNR-> ' + str(bckSNR))

    if (not os.path.isdir(outputFolder)):
        os.system('mkdir -p ' + outputFolder)

    Proc = np.zeros((numModules, 512, 128), dtype='float32')
    procMask = np.zeros((numModules, 512, 128), dtype='uint8')
    theDarkThreshold = np.zeros((numModules, 512, 128), dtype='float32')
    maxBackMeanMap = np.zeros((numModules, 512, 128), dtype='float32')
    modStatistics = np.zeros((numModules, 512, 128), dtype='float32')
    modStatistics2 = np.zeros((numModules, 512, 128), dtype='float32')
    cellidSet = CID + np.zeros(numModules, dtype='uint32')
    runCntStr = "%04d" % runNumber
    partCntStr = "%05d" % partNumber
    outputFileName = 'sample_run_' + runCntStr + '_part_' + \
                        partCntStr + '_TID_' + str(TID) + '_PID_' + str(PID) + '.cxi'
    outputFile = outputFolder + outputFileName
    runRawFolder = dataFolder + 'r' + runCntStr + '/'
    for moduleId in range(numModules):
        moduleIdStr = "%02d" % moduleId

        rawFile = runRawFolder + 'RAW-R'+runCntStr+'-AGIPD'+moduleIdStr+'-S'+partCntStr+'.h5'
        print('processing file ' + rawFile, flush=True)
        try:
            rawFileh5 = h5py.File(rawFile,'r')
        except:
            continue
        moduleIdStr = str(moduleId)
        h5Entry = rootEntry + moduleIdStr + 'CH0:xtdf/image/'
        trainIDEntry = h5Entry+'trainId'
        trainidSet = rawFileh5[trainIDEntry]
        trainidSet = trainidSet[...]
        pulseIDEntry = h5Entry+'pulseId'
        pulseidSet = rawFileh5[pulseIDEntry]
        pulseidSet = pulseidSet[...]
        cellIDEntry = h5Entry+'cellId'
        rawcellidSet = rawFileh5[cellIDEntry]
        _cellidSet = rawcellidSet[...]
        
        frmNumber = np.where((trainidSet==TID) & (pulseidSet==PID))
        frmNumber = frmNumber[0]
        _CID = _cellidSet[frmNumber]
        if(CID != _CID):
            print('This is a disaster!, CID does not maaaaaaaaaaaaaaaaaaaatch')
            exit()

        rawDataReadEntry = h5Entry+'data'
        dataSeth5= rawFileh5[rawDataReadEntry]
        cellidSet[moduleId] = _cellidSet[frmNumber]
        
        ######################################################## Proc
        print('Preparing pre-calculated statistics...')
        calibFileh5 = h5py.File(calibFile,'r')
        calib_AnalogOffset =  calibFileh5['AnalogOffset']
        AnalogOffset = np.squeeze(calib_AnalogOffset[:,CID,moduleId,:,:])
        calib_AnalogStds =  calibFileh5['AnalogStds']
        theDarkThreshold[moduleId, :, :] = np.squeeze(calib_AnalogStds[0,CID,moduleId,:,:]) # -> cells x Rows x Clms
        calib_GainLevel =  calibFileh5['GainLevel']
        GainLevel = np.squeeze(calib_GainLevel[:2,CID,moduleId,:,:])     # --> 2x250x512x128
        calib_GainLevelStds =  calibFileh5['GainLevelStds']
        GainLevelStds = np.squeeze(calib_GainLevelStds[:2,CID,moduleId,:,:])     # --> 2x250x512x128
        calib_RelativeGain =  calibFileh5['RelativeGain']
        RelativeGain = np.squeeze(calib_RelativeGain[:,CID,moduleId,:,:])     # --> 250x512x128
        
        RelativeGain[0] = 1 + 0*RelativeGain[0]
        
        calibFileh5.close()
        
        print('Preparing mask...')
        maskFileh5 = h5py.File(maskFile,'r')
        calib_maxBackMeanMap =  maskFileh5['maxBackMeanMap']
        maxBackMeanMap[moduleId, :, :] = np.squeeze(calib_maxBackMeanMap[CID, moduleId, :, :])
        calib_analogMax_HighGain =  maskFileh5['analogMax_HighGain']
        analogMax_HighGain = np.squeeze(calib_analogMax_HighGain[CID, moduleId, :, :])
        calib_Badpixel =  maskFileh5['badPixelMask_ByWater']
        BadpixelAll = np.squeeze(calib_Badpixel[:, CID, moduleId, :, :])
        
        '''
        if (FLAG_WITH_MASK):
            BadpixelAll[BadpixelAll>0]=1
        else:
            BadpixelAll[BadpixelAll!=1]=0
        '''
        #BadpixelAll[BadpixelAll<2]=0
        BadpixelAll[BadpixelAll>0]=1
        BadpixelAll = 1 - BadpixelAll
        maskFileh5.close()        
        
        _Proc, _mask = AGIPDCorrectionRoutine.singleModuleCorrection( \
                                    np.squeeze(dataSeth5[frmNumber,...]), 
                                    GainLevel, GainLevelStds, GainSTDLambda, 
                                    AnalogOffset, RelativeGain, 
                                    BadpixelAll, analogMax_HighGain)
        
        Proc[moduleId, :, :] = _Proc
        procMask[moduleId, :, :] = _mask
        modStatistics[moduleId, :, :] = np.squeeze(dataSeth5[frmNumber,0,...]) - AnalogOffset[0]
        
        modStatistics2[moduleId, :, :] = RelativeGain[0]
        rawFileh5.close()
    print('Raw data ready...', flush=True)
                                       
    print('Proc is ready after ' + str(time.time() - time_time) + ' seconds', flush=True)
    
    nPeaks = np.zeros(numModules, dtype='uint32')
    peakListTensor = np.zeros((numModules, 1024, 6), dtype='float32')
    for modCnt in range(numModules):
        if (FLAG_WITH_MASK):
            _pkList = RPF(inData=Proc[modCnt],
                          inMask = procMask[modCnt],
                          minPeakValMap = 6*theDarkThreshold[modCnt], 
                          maxBackMeanMap = maxBackMeanMap[modCnt], 
                          bckSNR = bckSNR)
        else:
            _pkList = RPF(inData=Proc[modCnt], inMask = procMask[modCnt])
        nPeaks[modCnt] = _pkList.shape[0]
        peakListTensor[modCnt, :nPeaks[modCnt], :] = _pkList
    '''
    -----------------------------------------------------------------------------------------
    Mass_Center_X, Mass_Center_Y, Mass_Total, Number of pixels in a peak, Maximum value, SNR
    -----------------------------------------------------------------------------------------
    '''
    
    print('Number of Peaks is ' + str(nPeaks.sum())) 
    peakRowsPosRaw = np.zeros(nPeaks.sum())
    peakClmsPosRaw = np.zeros(nPeaks.sum())
    dataset_peakTotalIntensity = np.zeros(nPeaks.sum())
    dataset_peakNPixels = np.zeros(nPeaks.sum())
    dataset_peakMaximumValue = np.zeros(nPeaks.sum())
    dataset_peakSNR = np.zeros(nPeaks.sum())
    stackedProc = np.zeros((16*512, 128))
    stackedMask = np.zeros((16*512, 128))
    stackedStatistics = np.zeros((16*512, 128))
    stackedStatistics2 = np.zeros((16*512, 128))
    NPeaksSoFar = int(0)
    for modCnt in range(16):
        stackedProc[modCnt*512:(modCnt+1)*512,:] = np.squeeze(Proc[modCnt, :, :]).copy()
        stackedMask[modCnt*512:(modCnt+1)*512,:] = np.squeeze(procMask[modCnt, :, :]).copy()
        stackedStatistics[modCnt*512:(modCnt+1)*512,:] = np.squeeze(modStatistics[modCnt, :, :]).copy()
        stackedStatistics2[modCnt*512:(modCnt+1)*512,:] = np.squeeze(modStatistics2[modCnt, :, :]).copy()
        numPeaks = nPeaks[modCnt]
        if (numPeaks):
            tmp = np.squeeze(peakListTensor[modCnt, :numPeaks,0])
            peakRowsPosRaw[NPeaksSoFar:NPeaksSoFar+numPeaks] = tmp + modCnt*512
            tmp = np.squeeze(peakListTensor[modCnt, :numPeaks,1])
            peakClmsPosRaw[NPeaksSoFar:NPeaksSoFar+numPeaks] = tmp
            tmp = np.squeeze(peakListTensor[modCnt, :numPeaks,2])
            dataset_peakTotalIntensity[NPeaksSoFar:NPeaksSoFar+numPeaks] = tmp
            tmp = np.squeeze(peakListTensor[modCnt, :numPeaks,3])
            dataset_peakNPixels[NPeaksSoFar:NPeaksSoFar+numPeaks] = tmp
            tmp = np.squeeze(peakListTensor[modCnt, :numPeaks,4])
            dataset_peakMaximumValue[NPeaksSoFar:NPeaksSoFar+numPeaks] = tmp
            tmp = np.squeeze(peakListTensor[modCnt, :numPeaks,5])
            dataset_peakSNR[NPeaksSoFar:NPeaksSoFar+numPeaks] = tmp

            NPeaksSoFar += numPeaks

    myGeom = cfelCryst.load_crystfel_geometry(geometryFile)
    geomMap = cfelGeom.compute_pix_maps(myGeom)
    geomMap_X = geomMap[0]
    geomMap_Y = geomMap[1]
    stackedProc_geom = cfelGeom.apply_geometry_to_data(stackedProc.copy(), myGeom)
    stackedMask_geom = cfelGeom.apply_geometry_to_data(stackedMask.copy(), myGeom)
    toShow_geom = stackedProc_geom * stackedMask_geom
    centX = toShow_geom.shape[1]/2-1/2
    centY = toShow_geom.shape[0]/2-1/2

    _peaksX = peakRowsPosRaw.astype('uint32')
    _peaksY = peakClmsPosRaw.astype('uint32')
    peaksX = geomMap_X[_peaksX, _peaksY] + centX
    peaksY = geomMap_Y[_peaksX, _peaksY] + centY
    showdata_RPF = np.vstack((peaksX, peaksY)).T
        
    if(False):
        plt.imshow(toShow_geom), plt.show()        
        toShow_geom = cfelGeom.apply_geometry_to_data(stackedStatistics, myGeom)
        plt.imshow(toShow_geom), plt.show()        
        toShow_geom = cfelGeom.apply_geometry_to_data(stackedStatistics2, myGeom)
        plt.imshow(toShow_geom), plt.show()        

        pixRes = (geomMap_X**2 + geomMap_Y**2)**0.5
        pixRes_geom = cfelGeom.apply_geometry_to_data(pixRes, myGeom)
        indsi, indsj = np.where((pixRes_geom>92) & (pixRes_geom<93))
        ringMask = np.zeros(stackedMask_geom.shape, dtype=stackedMask_geom.dtype)
        ringMask[indsi, indsj] = 1
        ringMask = ringMask*stackedMask_geom
        
        maxInt = 2000
       
        hitWidth = 5
       
        vec = stackedProc_geom[ringMask==1].flatten()
        mP = fitValue(vec, MSSE_LAMBDA=4)
        mP = fitValue(vec, topKthPerc=0.5, bottomKthPerc=0, MSSE_LAMBDA=4, modelValueInit=mP[0])
        
        bins1, edges1 = np.histogram(vec, 100, range=(-100,maxInt))
        #bins1 = bins1/bins1.max()
        plt.plot(edges1[:-1], bins1, linewidth = 3)
        plt.bar(edges1[:-1], bins1, width = hitWidth, alpha=0.5)
        maxLine = bins1.max()
        LWidth = 3
        mean_vec = np.mean(vec[(22<vec)&(vec<1000)])
        median_vec = np.median(vec)
        _color = 'green'
        plt.plot(np.array([median_vec, median_vec]), np.array([0, maxLine]), color=_color, linewidth=LWidth, label='median')
        _color = 'red'
        plt.plot(np.array([mP[0], mP[0]]), np.array([0, maxLine]), color=_color, linewidth=LWidth, label='mode')
        densities = maxLine*np.exp(-(edges1-mP[0])**2/(2*mP[1]**2))
        plt.plot(edges1, densities, color=_color, linewidth=LWidth)

        _color = 'gold'
        plt.plot(np.array([mean_vec, mean_vec]), np.array([0, maxLine]), color=_color, linewidth=LWidth, label='mu')
        
        plt.grid()
        plt.legend(fontsize=14)
        plt.xlabel('vector elements values',fontsize=15)
        plt.ylabel('frequency',fontsize=15)
        plt.xticks(fontsize=15)
        plt.yticks(fontsize=15)
        plt.xlim([-100, maxInt])
        plt.show()
        
        
        plt.plot(edges1[:-1], bins1, linewidth = 3)
        plt.bar(edges1[:-1], bins1, width = hitWidth, alpha=0.5)

        vec = stackedProc_geom[ringMask==1].flatten()
        vec = vec[vec<=maxInt]
        randVec = np.random.rand(vec.shape[0])
        darkSTD = 10
        NORM_IS_NORM = 4
        probValue = np.exp(-(NORM_IS_NORM*darkSTD - vec)**2/(2*darkSTD*darkSTD))
        probValue[vec>NORM_IS_NORM*darkSTD]=2
        vec = vec[randVec<=probValue]

        edges3 = edges1[:int(edges1.shape[0]/2)]
        bins3 = np.exp(-(NORM_IS_NORM*darkSTD - edges3)**2/(2*darkSTD*darkSTD))
        bins3[edges3>NORM_IS_NORM*darkSTD]=1.00001
        bins3 = 100*bins3/bins3.max()

        bins2, edges2 = np.histogram(vec, 100, range=(-100,maxInt))
        #bins2 = bins2/bins2.max()
        plt.plot(edges2[:-1], bins2, linewidth = 3)
        plt.bar(edges2[:-1], bins2, width = hitWidth, alpha=0.5)

        maxLine = bins2.max()

        LWidth = 3
        plt.plot(np.array([mean_vec, mean_vec]), np.array([0, maxLine]), linewidth=LWidth, label='mean')

        plt.plot(np.array([median_vec, median_vec]), np.array([0, maxLine]), linewidth=LWidth, label='median')
        plt.plot(np.array([mP[0], mP[0]]), np.array([0, maxLine]), linewidth=LWidth, label='FLKOS: Dark')
        mP = fitValue(vec)
        mP = fitValue(vec, topKthPerc=0.3, bottomKthPerc=0.15, MSSE_LAMBDA=4, modelValueInit=mP[0])
        plt.plot(np.array([mP[0], mP[0]]), np.array([0, maxLine]), color='pink', linewidth=LWidth, label='FLKOS: Dark removed')
        plt.grid()
        plt.legend(fontsize=14)
        plt.xlabel('vector elements values',fontsize=15)
        plt.ylabel('frequency',fontsize=15)
        plt.xticks(fontsize=15)
        plt.yticks(fontsize=15)
        plt.plot(edges3, bins3, color='gold', linewidth = 3)

        densities = maxLine*np.exp(-(edges2-mP[0])**2/(2*mP[1]**2))
        plt.plot(edges2, densities, color='pink', linewidth=LWidth)
        
        plt.show()
        
        plt.show()
        plt.imshow(stackedProc_geom*ringMask, vmin=0, vmax=400)
        plt.colorbar()
        plt.scatter(showdata[:,0],showdata[:,1], s=80, facecolors='none', edgecolors='r')
        plt.show()
        #exit()
        plt.imshow(stackedProc_geom, vmin=0, vmax=maxInt)
        plt.colorbar()
        plt.scatter(showdata[:,0],showdata[:,1], s=80, facecolors='none', edgecolors='r')
        plt.show()
    if(False):
        myGeom = cfelCryst.load_crystfel_geometry(geometryFile)
        geomMap = cfelGeom.compute_pix_maps(myGeom)
        stackedProc_geom = cfelGeom.apply_geometry_to_data(stackedProc.copy(), myGeom)
        stackedMask_geom = cfelGeom.apply_geometry_to_data(stackedMask.copy(), myGeom)
        plt.imshow(stackedProc_geom, vmin=0, vmax=400)
        plt.show()
        

    print('creating one file:\n '+outputFile)
    file_outData = h5py.File(outputFile, "w")
    file_outData.create_dataset( 'entry_1/data_1/data', data = np.array([stackedProc]))
    file_outData.create_dataset( 'entry_1/data_1/mask', data = np.array([stackedMask]))
    file_outData.create_dataset( 'entry_1/result_1/peakXPosRaw', data = np.array([peakClmsPosRaw]))
    file_outData.create_dataset( 'entry_1/result_1/peakYPosRaw', data = np.array([peakRowsPosRaw]))
    file_outData.create_dataset( 'entry_1/result_1/peakMaximumValue', data = np.array([dataset_peakMaximumValue]))
    file_outData.create_dataset( 'entry_1/result_1/peakNPixels', data = np.array([dataset_peakNPixels]))
    file_outData.create_dataset( 'entry_1/result_1/peakSNR', data = np.array([dataset_peakSNR]))
    file_outData.create_dataset( 'entry_1/result_1/peakTotalIntensity', data = np.array([dataset_peakTotalIntensity]))
    file_outData.create_dataset( 'entry_1/result_1/nPeaks', data = np.array([nPeaks.sum()]))
    file_outData.create_dataset( 'instrument/cellID', data = np.array([CID]))
    file_outData.create_dataset( 'instrument/pulseID', data = np.array([PID]))
    file_outData.create_dataset( 'instrument/trainID', data = np.array([TID]))
    file_outData.close()
    
    flistFile = outputFolder+'flist.lst'
    File_object = open(flistFile,"w")
    File_object.writelines(outputFile)
    File_object.close()
   
    #################################### RPF ###########################
    '''
    indexamagic_options = '-i '+ flistFile + ' ' + \
                         '-g '+ geometryFile + ' ' + \
                         '-o ' + outputFile[:-4] + '_RPF.stream' + ' ' + \
                         '--no-cell-combinations --profile --peaks=cxi' + ' ' + \
                         '--no-revalidate --no-check-peaks' + ' ' + \
                         '--int-radius=2,4,7' + ' ' + \
                         '-p ' + cellFile
    print('indexamagic_options -> ' + indexamagic_options)
    os.system('indexamajig ' + indexamagic_options)
    '''
    #################################### PF8 ###########################
    #'''
    indexamagic_options = '-i '+ flistFile + ' ' + \
                         '-g '+ geometryFile + ' ' + \
                         '-o ' + outputFile[:-4] + '_PF8.stream' + ' ' + \
                         '--no-cell-combinations --profile' + ' ' + \
                         '--no-revalidate --no-check-peaks' + ' ' + \
                         '--int-radius=2,4,7' + ' ' + \
                         '-p ' + cellFile + ' ' + \
                         '--serial-start=1' + ' ' + \
                         '--peaks=peakfinder8' + ' ' + \
                         '--min-snr=' + str(bckSNR) + ' ' + \
                         '--threshold=122' + ' ' + \
                         '--min-pix-count=1' + ' ' + \
                         '--max-pix-count=30' + ' ' + \
                         '--indexing=' + 'xgandalf' + ' ' + \
                         '--local-bg-radius=8' + ' ' + \
                         '--min-peaks=20'# + ' ' + \
                         #'--min-res=30' + ' ' + \
                         #'--max-res=150' + ' ' + \
                         
    print('indexamagic_options -> ' + indexamagic_options)
    os.system('indexamajig ' + indexamagic_options)
    #'''
    #'''
    indexamagic_options = '-i '+ flistFile + ' ' + \
                         '-g '+ geometryFile + ' ' + \
                         '-o ' + outputFile[:-4] + '_RPF8.stream' + ' ' + \
                         '--no-cell-combinations --profile' + ' ' + \
                         '--no-revalidate --no-check-peaks' + ' ' + \
                         '--int-radius=2,4,7' + ' ' + \
                         '-p ' + cellFile + ' ' + \
                         '--serial-start=1' + ' ' + \
                         '--peaks=robustpeakfinder8' + ' ' + \
                         '--min-snr=' + str(bckSNR) + ' ' + \
                         '--threshold=122' + ' ' + \
                         '--min-pix-count=1' + ' ' + \
                         '--max-pix-count=30' + ' ' + \
                         '--indexing=' + 'xgandalf' + ' ' + \
                         '--local-bg-radius=8' + ' ' + \
                         '--min-peaks=20'# + ' ' + \
                         #'--min-res=30' + ' ' + \
                         #'--max-res=150' + ' ' + \
    print('indexamagic_options -> ' + indexamagic_options)
    os.system('/home/sadriali/cryst/bin/indexamajig ' + indexamagic_options)
    print('outputFile: ' + outputFile[:-4] + '_RPF8.stream')
    #os.system('nano ' + outputFile[:-4] + '_RPF8.stream')
    #'''
    
    inFile = outputFile[:-4] + '_RPF8.stream'
    streamFileParser = StreamFileParser(inFile)
    peakRowsPosRaw = streamFileParser.streamFileChunks[0][5][1]
    peakClmsPosRaw = streamFileParser.streamFileChunks[0][5][0]
    peakBckSigma = streamFileParser.streamFileChunks[0][5][3]
    
    print('Number of found peaks in RPF8: ' + str(peakRowsPosRaw.shape[0]))
    
    _peaksX = peakRowsPosRaw.astype('uint32')
    _peaksY = peakClmsPosRaw.astype('uint32')
    peaksX = geomMap_X[_peaksX, _peaksY] + centX
    peaksY = geomMap_Y[_peaksX, _peaksY] + centY
    showdata = np.vstack((peaksX, peaksY)).T
    
    plt.imshow(toShow_geom, vmin=0, vmax=2*np.percentile(toShow_geom.flatten(), 99))
    plt.colorbar()
    plt.scatter(showdata_RPF[:,0],showdata_RPF[:,1], marker='d', s=60, facecolors='none', edgecolors='c')
    plt.scatter(showdata[:,0],showdata[:,1], s=80, facecolors='none', edgecolors='r')
    
    #for ptCnt in range(showdata[:,0].shape[0]):
    #    plt.text(showdata[ptCnt,0],showdata[ptCnt,1], peakBckSigma[ptCnt])

    inFile = outputFile[:-4] + '_PF8.stream'
    os.system('cat ' + inFile)
    streamFileParser = StreamFileParser(inFile)
    peakRowsPosRaw = streamFileParser.streamFileChunks[0][5][1]
    peakClmsPosRaw = streamFileParser.streamFileChunks[0][5][0]

    _peaksX = peakRowsPosRaw.astype('uint32')
    _peaksY = peakClmsPosRaw.astype('uint32')
    peaksX = geomMap_X[_peaksX, _peaksY] + centX
    peaksY = geomMap_Y[_peaksX, _peaksY] + centY

    predictedRowsPosRaw = streamFileParser.streamFileChunks[0][6][0][1]
    predictedClmsPosRaw = streamFileParser.streamFileChunks[0][6][0][0]

    _predictedsX = predictedRowsPosRaw.astype('uint32')
    _predictedsY = predictedClmsPosRaw.astype('uint32')
    predictedsX = geomMap_X[_predictedsX, _predictedsY] + centX
    predictedsY = geomMap_Y[_predictedsX, _predictedsY] + centY
    
    print('Number of found peaks in PF8: ' + str(peakRowsPosRaw.shape[0]))
    print('Number of found predicted is: ' + str(predictedRowsPosRaw.shape[0]))
    
    showdata = np.vstack((peaksX, peaksY)).T
    showpredicted = np.vstack((predictedsX, predictedsY)).T
    plt.imshow(toShow_geom, vmin=0, vmax=2*np.percentile(toShow_geom.flatten(), 99))
    plt.scatter(showdata[:,0],showdata[:,1], marker='s', s=40, facecolors='none', edgecolors='y')
    plt.scatter(showpredicted[:,0],showpredicted[:,1], marker='o', s=80, facecolors='none', edgecolors='r')

    plt.show()
    pass