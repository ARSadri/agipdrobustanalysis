from RobustPeakFinder import robustPeakFinderPyFunc as RPF
import h5py
import numpy as np
import os
import time
import AGIPDCorrectionRoutine
import matplotlib.pyplot as plt
import cfelpyutils.crystfel_utils as cfelCryst
import cfelpyutils.geometry_utils as cfelGeom
from StreamFileParser import *

from RobustGaussianFittingLibrary import fitBackground, fitValue, MSSE

#import maskLib
#maskLib.showWhatGeometryDoes(geometryFile)
np.set_printoptions(suppress=True)
np.set_printoptions(precision=2)
os.system('source /etc/profile.d/modules.sh')
os.system('module load anaconda/3')
os.system('module load crystfel')
os.system('module load ccp4')

######################################################################
SPBorMID = 'SPB'
ExperimentDate = '202030'
proposalNumber = 'p900119'
numMemCells = 352
runNumDarkHighGain  = 109
runNumDarkMediumGain = 110
runNumDarkLowGain = 111
lightRun = 97

rootFolder = '/gpfs/exfel/exp/'+SPBorMID+'/'+ExperimentDate+'/'+proposalNumber+'/'
dataFolder = rootFolder + 'raw/'
scratchFolder = rootFolder + 'scratch/alireza/'
calibFolder = scratchFolder + 'calibData/'
calibFile = calibFolder + 'Cheetah-AGIPD-calib.h5'
maskFile = calibFolder + 'bad-pixel-mask.h5'
#calibFile = calibFolder + 'Cheetah-AGIPD-recalib.h5'
#maskFile = calibFolder + 'bad-pixel-mask_ByLight.h5'
geometryFile = scratchFolder + 'dep/agipd_2450_v9.geom'
cellFile = scratchFolder + 'dep/1VDS.cell'
outputFolder = scratchFolder + 'samples/'

rootEntry = '/INSTRUMENT/'+SPBorMID+'_DET_AGIPD1M-1/DET/'
'''
Run_0095_Part_00000_TID_689505085_PID_660_CID_165_frameNumbers_[10725 10725 10725 10725 10725 10373 10725 10725 10725 10725 10725 10725
 10725 10725 10725 10725]
'''
'''
runNumber = 95
partNumber = 3
TID = 689505826
PID = 512
CID = 128
'''
''' 
Run_0096_Part_00019_TID_689516905_PID_468_CID_117_frameNumbers_
[20885 20533 20885 20533 20885 20885 20885 20533 20885 20533 20885 20533 20533 20533 20885 20885]
'''
'''
runNumber = 96
partNumber = 19
TID = 689516905
PID = 468
CID = 117
'''
'''
Run_0097_Part_00000_TID_689519307_PID_348_CID_87_frameNumbers_[8535 8535 8535 8535 8535 8535 8535 8183 8535 8535 8535 8535 8535 8535
 8183 8183]

#Run_0095_Part_00020_TID_689510374_PID_708_CID_177

# too bright: Run_0097_Part_00000_TID_689519283_PID_56_CID_14

#just noise : Run_0097_Part_00000_TID_689519283_PID_72_CID_18


Run_0095_Part_00003_TID_689505826_PID_512_CID_128_frameNumbers_[1184 1184 1184 1184 1184 1184 1184 1184 1184 1184 1184 1184 1184 1184
 1184 1184]
'''

#'''
runNumber = 96
partNumber = 1
TID = 689512346
PID = 776
CID = 194
#'''

GainSTDLambda = 6.0
bckSNR = 6.0
numModules = 16
FLAG_WITH_MASK = True

if __name__ == '__main__':
    
    print('Robust PeakFinder Analyser started....', flush = True)
    print('PID ->' + str(os.getpid()))
    time_time = time.time()
    print('Peakfinding started at ' + str(time_time) + 's')
    
    print('rootEntry-> ' + rootEntry)
    print('calibFile-> ' + calibFile)
    print('maskFile-> ' + maskFile)
    print('bckSNR-> ' + str(bckSNR))

    if (not os.path.isdir(outputFolder)):
        os.system('mkdir -p ' + outputFolder)

    runCntStr = "%04d" % runNumber
    partCntStr = "%05d" % partNumber
    outputFileName = 'sample_run_' + runCntStr + '_part_' + \
                        partCntStr + '_TID_' + str(TID) + '_PID_' + str(PID) + '.cxi'
    outputFile = outputFolder + outputFileName
    runRawFolder = dataFolder + 'r' + runCntStr + '/'

    print('Reading file:\n '+outputFile)
    file_outData = h5py.File(outputFile, "r")
    stackedProc = file_outData[ 'entry_1/data_1/data' ][...]
    stackedMask = file_outData[ 'entry_1/data_1/mask' ][...]
    file_outData.close()

    myGeom = cfelCryst.load_crystfel_geometry(geometryFile)
    geomMap = cfelGeom.compute_pix_maps(myGeom)
    geomMap_X = geomMap[0]
    geomMap_Y = geomMap[1]
    stackedProc_geom = cfelGeom.apply_geometry_to_data(stackedProc.copy(), myGeom)
    stackedMask_geom = cfelGeom.apply_geometry_to_data(stackedMask.copy(), myGeom)
    toShow_geom = stackedProc_geom * stackedMask_geom
    centX = toShow_geom.shape[1]/2-1/2
    centY = toShow_geom.shape[0]/2-1/2

    inFile = outputFile[:-4] + '_PF8.stream'
    os.system('cat ' + inFile)
    streamFileParser = StreamFileParser(inFile)
    peakRowsPosRaw = streamFileParser.streamFileChunks[0][5][1]
    peakClmsPosRaw = streamFileParser.streamFileChunks[0][5][0]

    _peaksX = peakRowsPosRaw.astype('uint32')
    _peaksY = peakClmsPosRaw.astype('uint32')
    peaksX = geomMap_X[_peaksX, _peaksY] + centX
    peaksY = geomMap_Y[_peaksX, _peaksY] + centY

    predictedRowsPosRaw = streamFileParser.streamFileChunks[0][6][0][1]
    predictedClmsPosRaw = streamFileParser.streamFileChunks[0][6][0][0]

    _predictedsX = predictedRowsPosRaw.astype('uint32')
    _predictedsY = predictedClmsPosRaw.astype('uint32')
    predictedsX = geomMap_X[_predictedsX, _predictedsY] + centX
    predictedsY = geomMap_Y[_predictedsX, _predictedsY] + centY
    
    print('Number of found peaks in PF8: ' + str(peakRowsPosRaw.shape[0]))
    print('Number of found predicted is: ' + str(predictedRowsPosRaw.shape[0]))
    
    showdata = np.vstack((peaksX, peaksY)).T
    showpredicted = np.vstack((predictedsX, predictedsY)).T
    plt.imshow(toShow_geom, vmin=0, vmax=2*np.percentile(toShow_geom.flatten(), 99))
    plt.scatter(showdata[:,0],showdata[:,1], marker='s', s=40, facecolors='none', edgecolors='y')
    plt.scatter(showpredicted[:,0],showpredicted[:,1], marker='o', s=80, facecolors='none', edgecolors='r')

    
    nPeaks = peakRowsPosRaw.shape[0]
    for pk_i in range(nPeaks):
        dX = peaksX - peaksX[pk_i]
        dY = peaksY - peaksY[pk_i]
        dist = (dX**2 + dY**2)**0.5
        scale = 50
        inds = np.argsort(dist)
        for indsCnt in range(1, 8):
            plt.plot(np.array([ peaksX[pk_i] , peaksX[inds[indsCnt]] ]),
                     np.array([ peaksY[pk_i] , peaksY[inds[indsCnt]] ]),
                     alpha = np.exp(-dist[inds[indsCnt]]**2/(2*scale**2)))
       
    
    plt.show()
    pass