import h5py
import matplotlib.pyplot as plt
import cfelpyutils.crystfel_utils as cfelCryst
import cfelpyutils.geometry_utils as cfelGeom

geometryFile = '/gpfs/exfel/exp/SPB/202001/p002450/scratch/alireza/agipd_2450_v4.geom'
myGeom = cfelCryst.load_crystfel_geometry(geometryFile)

h5File = h5py.File('mask_lyso_new.h5','r')
mask = h5File['data/data']
mask = mask[...]
_mask_geom = cfelGeom.apply_geometry_to_data(mask, myGeom)
plt.imshow(_mask_geom), plt.show()
