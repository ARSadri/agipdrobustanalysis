'''
we are looking at:
Run_0118_Part_00000_TID_550057389_PID_548_CID_137_frameNumbers_[58387 58387 58387 58387 58387 58137 58137 58137 57637 58137 58387 58137
 58387 57887 58137 58387]
'''

import os
import h5py
import numpy as np
import matplotlib.pyplot as plt
import cfelpyutils.crystfel_utils as cfelCryst
import cfelpyutils.geometry_utils as cfelGeom

np.set_printoptions(suppress=True)
np.set_printoptions(precision=2)

#/gpfs/exfel/exp/SPB/202030/p900119/scratch/alireza/RPFCXI_robustpeakfinder8_/r0096/part_00001_011.cxi

runCnt = 96
partNumber = 1
subpartNumber = 11

frmNumber = 47

runCntStr = "%04d" % runCnt
geomFilePath = '/gpfs/exfel/exp/SPB/202030/p900119/scratch/alireza/dep/agipd_2450_v9.geom'
peakFinderOutFolder = '/gpfs/exfel/exp/SPB/202030/p900119/scratch/alireza/RPFCXI_robustpeakfinder8_/r'+runCntStr+'/'
dataEntry = '/entry_1/data_1/data'
maskEntry = '/entry_1/data_1/mask'
pksClmEntry = '/entry_1/result_1/peakXPosRaw'
pksRowEntry = '/entry_1/result_1/peakYPosRaw'
nPeaksEntry = '/entry_1/result_1/nPeaks'
frameNumbersEntry = '/entry_1/result_1/frameNumber'
TIDEntry = '/instrument/trainID'
PIDEntry = '/instrument/pulseID'
CIDEntry = '/instrument/cellID'

if __name__ == '__main__':
    myGeom = cfelCryst.load_crystfel_geometry(geomFilePath)

    partNumberStr = "%05d" % partNumber
    subpartNumberStr = "%03d" % subpartNumber
    dataFilePath = peakFinderOutFolder + 'part_'+partNumberStr+'_'+subpartNumberStr+'.cxi'
    #dataFilePath = peakFinderOutFolder + 'part_'+partNumberStr+'.cxi'
    dataFile = h5py.File(dataFilePath,'r')
    allTID = dataFile[TIDEntry]
    allTID = allTID[...]
    allPID = dataFile[PIDEntry]
    allPID = allPID[...]
    allCID = dataFile[CIDEntry]
    allCID = allCID[...]
    
    h5ProcSet = dataFile[dataEntry]
    h5MaskSet = dataFile[maskEntry]
    h5PeaksClmSet = dataFile[pksClmEntry]
    h5PeaksRowSet = dataFile[pksRowEntry]
    nPeaksSet = dataFile[nPeaksEntry]
    frameNumbers = dataFile[frameNumbersEntry]
    
    print('There are ' + str(h5ProcSet.shape[0]) + ' frames in ' + dataFilePath)
    print('we are looking at:')
    print('Run_'+runCntStr+'_Part_'+partNumberStr+\
          '_TID_'+str(allTID[frmNumber])+\
          '_PID_'+str(allPID[frmNumber])+\
          '_CID_'+str(allCID[frmNumber])+\
          '_frameNumbers_'+str(frameNumbers[frmNumber, :]))
    
    _proc = (h5ProcSet[frmNumber, :, :]).copy()
    _mask = h5MaskSet[frmNumber, :, :]
    
    _proc[_mask==0]=0
    print('the bad pixels are %' + str(np.floor(10000*(1-_mask.flatten().mean()))/100))

    _proc_geom = cfelGeom.apply_geometry_to_data(_proc, myGeom)
    _mask_geom = cfelGeom.apply_geometry_to_data(_mask, myGeom)
    
    nPeaks = nPeaksSet[frmNumber]
    print('There are ' + str(nPeaks) +' peaks.')
    
    _peaksRow = (h5PeaksRowSet[frmNumber, :nPeaks]).astype('uint32')
    _peaksClm = (h5PeaksClmSet[frmNumber, :nPeaks]).astype('uint32')
    geomMap = cfelGeom.compute_pix_maps(myGeom)
    geomMap_X = geomMap[0]
    geomMap_Y = geomMap[1]
    peaksX = geomMap_X[_peaksRow, _peaksClm]
    peaksY = geomMap_Y[_peaksRow, _peaksClm]

    centX = _proc_geom.shape[1]/2-1/2
    centY = _proc_geom.shape[0]/2-1/2
    peaksX += centX
    peaksY += centY
    
    
    showdata = np.vstack((peaksX, peaksY)).T
    #plt.imshow(_proc_geom*_mask_geom, vmin=0, vmax=2000)
    plt.imshow(_proc_geom, vmax=3000)
    plt.scatter(showdata[:,0],showdata[:,1], s=80, facecolors='none', edgecolors='r')
    plt.show()

    #fig, axes = plt.subplots(2, 1)
    #axes[0].imshow(procImgFrame_geom*BadpixelFrame_geom, vmin = -100, vmax = 2000)
    #axes[0].scatter(showdata[:,0],showdata[:,1] , c="r")
    #axes[1].imshow(BadpixelFrame_geom, vmin = 0, vmax = 1)
    #axes[1].scatter(showdata[:,0],showdata[:,1] , c="r")
    #plt.show()
    exit()