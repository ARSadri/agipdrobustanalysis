# python3 compareTwoStreams.py /gpfs/exfel/exp/SPB/202001/p002450/scratch/alireza/streams_pF8_SNR8_masked/experiment.stream /gpfs/exfel/exp/SPB/202001/p002450/scratch/alireza/streams_RPF_SNR8_masked/Robust_withMask_peakSNR_8.0_hitSNT8.0.stream

import sys
import pickle

def procInfoFunc(fileName):
    indx = 0
    File_object = open(fileName, "r")
    hitInfo = []
    indexedInfo = []
    for line in File_object:
        if( "Image filename: " in line):
            img_filename = (line[:-1].split('/r0'))[1]
        if( "Event: //" in line):
            event_str = (line[:-1].split(': //'))[1]
        if( "indexed_by = " in line):
            if(indx % 10000 == 0):
                print(str(indx) + ',', end='', flush=True)
            indx += 1
            hitInfo.append(img_filename+event_str)
            if( "indexed_by = none" not in line):
                indexedInfo.append(img_filename+'.'+event_str)
    return(hitInfo, indexedInfo)

if __name__ == '__main__':
    '''
    file1 = sys.argv[1]
    file2 = sys.argv[2]
    print('file1 ->' + file1)
    print('file2 ->' + file2)

    hitInfo1, indexedInfo1 = procInfoFunc(file1)
    hitInfo2, indexedInfo2 = procInfoFunc(file2)
    
    with open("indexedInfo1.txt", "wb") as fp:
        pickle.dump(indexedInfo1, fp)    
    with open("hitInfo1.txt", "wb") as fp:
        pickle.dump(hitInfo1, fp)
    with open("hitInfo2.txt", "wb") as fp:
        pickle.dump(hitInfo2, fp)    
    with open("indexedInfo2.txt", "wb") as fp:
        pickle.dump(indexedInfo2, fp)
    '''
    with open("indexedInfo1.txt", "rb") as fp:
        indexedInfo1 = pickle.load(fp)
    with open("indexedInfo2.txt", "rb") as fp:
        indexedInfo2 = pickle.load(fp)
    with open("hitInfo1.txt", "rb") as fp:
        hitInfo1 = pickle.load(fp)
    with open("hitInfo2.txt", "rb") as fp:
        hitInfo2 = pickle.load(fp)
    
    print('number of hits images in file1 -> ' + str(len(hitInfo1)))
    print('number of hits images in file2 -> ' + str(len(hitInfo2)))
    print('number of hits images in 1 but not in 2 -> ' + \
            str(len(set(hitInfo1).difference(set(hitInfo2)))))
    print('number of hits images in 2 but not in 1 -> ' + \
            str(len(set(hitInfo2).difference(set(hitInfo1)))))
    print('number of hits images in 1  and in 2 -> ' + \
            str(len(set(hitInfo1).intersection(set(hitInfo2)))))
    print('number of hits images in 1  or in 2 -> ' + \
            str(len(set(hitInfo1).union(set(hitInfo2)))))

    print('number of indexed images in file1 -> ' + str(len(indexedInfo1)))
    print('number of indexed images in file2 -> ' + str(len(indexedInfo2)))
    print('number of indexed images in 1 but not in 2 -> ' + \
            str(len(set(indexedInfo1).difference(set(indexedInfo2)))))
    print('number of indexed images in 2 but not in 1 -> ' + \
            str(len(set(indexedInfo2).difference(set(indexedInfo1)))))
    print('number of indexed images in 1 and in 2 -> ' + \
            str(len(set(indexedInfo1).intersection(set(indexedInfo2)))))
    print('number of indexed images in 1 or in 2 -> ' + \
            str(len(set(indexedInfo1).union(set(indexedInfo2)))))

    print('--------------------------------')
    print(set(hitInfo1).difference(set(hitInfo2)))
    