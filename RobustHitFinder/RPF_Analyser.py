import h5py
import numpy as np
from os import getpid
import gc
from multiprocessing import Process, Queue, cpu_count
import sys
import time
import matplotlib.pyplot as plt

import AGIPDCorrectionRoutine
import RGFLib.RobustGausFitLibMultiprocPy as RGFLib_multiproc
from textProgBar import textProgBar
import RPF.RobustPeakFinder_Python_Wrapper as RPFPy

np.set_printoptions(suppress=True)
np.set_printoptions(precision=2)

GainSTDLambda = 6.0

if __name__ == '__main__':
    print('Robust PeakFinder Analyser started....', flush = True)
    print('PID ->' + str(getpid()))
    time_time = time.time()
    argCnt = 0
    
    argCnt += 1
    ReadFileName = sys.argv[argCnt]
    argCnt += 1
    rootEntry = sys.argv[argCnt]
    argCnt += 1
    calibFile = sys.argv[argCnt]
    argCnt += 1
    maskFile = sys.argv[argCnt]
    argCnt += 1
    outputFileName = sys.argv[argCnt]
    argCnt += 1
    bckSNR = np.float32(sys.argv[argCnt])
    argCnt += 1
    FLAG_Make_FullProc_Mask = int(sys.argv[argCnt])
    argCnt += 1
    withOrNoMaskFlag = sys.argv[argCnt]
    argCnt += 1
    FLAG_Use_XFEL_Proc = int(sys.argv[argCnt])
    argCnt += 1
    includeData = int(sys.argv[argCnt])
    
    print('ReadFileName-> ' + ReadFileName)
    print('rootEntry-> ' + rootEntry)
    print('calibFile-> ' + calibFile)
    print('outputFileName-> ' + outputFileName)
    print('bckSNR-> ' + str(bckSNR))
    print('FLAG_Make_FullProc_Mask-> ' + str(FLAG_Make_FullProc_Mask))
    print('withOrNoMaskFlag-> ' + withOrNoMaskFlag)
    print('FLAG_Use_XFEL_Proc-> ' + str(FLAG_Use_XFEL_Proc))
    print('includeData-> ' + str(includeData))

    print('Peakfinding started at ' + str(time_time) + 's')
    print('processing file ' + ReadFileName, flush=True)
    inputFileh5 = h5py.File(ReadFileName,'r')
    moduleId = int(ReadFileName.split("AGIPD",2)[1][:2])
    moduleIdStr = str(moduleId)
    h5Entry = rootEntry + moduleIdStr + 'CH0:xtdf/image/'
    lengthEntry = h5Entry+'length'
    lengthSet = inputFileh5[lengthEntry]
    lengthSet = np.squeeze(lengthSet[...])
    numFrames = np.count_nonzero(lengthSet)
    
    if(numFrames==0):
        print('Part has no frames')
        exit()
    
    print('Reading input data...')
    lengthSet = np.squeeze(lengthSet[:numFrames])
    cellIDEntry = h5Entry+'cellId'
    inputcellidSet = np.squeeze(inputFileh5[cellIDEntry])
    cellidSet = inputcellidSet[:numFrames]
    trainIDEntry = h5Entry+'trainId'
    trainidSet = np.squeeze(inputFileh5[trainIDEntry])
    trainidSet = trainidSet[:numFrames]
    pulseIDEntry = h5Entry+'pulseId'
    pulseidSet = np.squeeze(inputFileh5[pulseIDEntry])
    pulseidSet = pulseidSet[:numFrames]
    
    procMaskReadEntry = h5Entry+'mask'
    inputDataReadEntry = h5Entry+'data'
    try:
        dataSet = inputFileh5[inputDataReadEntry]
    except:
        print('dataset not found')
        exit()

    if(FLAG_Use_XFEL_Proc == 1):
        procMask = inputFileh5[procMaskReadEntry]
        procMask = procMask[:numFrames,...]
        procMask[procMask>0]=1
        procMask = 1 - procMask
        Proc = dataSet[:numFrames,...]
    else:
        dataSet = dataSet[:numFrames,...]
    inputFileh5.close()
    print('input data ready...', flush=True)
    
    if(FLAG_Use_XFEL_Proc==0):    
        print('Preparing pre-calculated statistics...')
        calibFileh5 = h5py.File(calibFile,'r')
        calib_AnalogOffset =  calibFileh5['AnalogOffset']
        AnalogOffset = np.squeeze(calib_AnalogOffset[:,:,moduleId,:,:])
        calib_AnalogStds =  calibFileh5['AnalogStds']
        theDarkThreshold = np.squeeze(4.0*calib_AnalogStds[0,:,moduleId,:,:]) # -> cells x Rows x Clms
        calib_GainLevel =  calibFileh5['GainLevel']
        GainLevel = np.squeeze(calib_GainLevel[:2,:,moduleId,:,:])     # --> 2x250x512x128
        calib_GainLevelStds =  calibFileh5['GainLevelStds']
        GainLevelStds = np.squeeze(calib_GainLevelStds[:2,:,moduleId,:,:])     # --> 2x250x512x128
        calib_RelativeGain =  calibFileh5['RelativeGain']
        RelativeGain = np.squeeze(calib_RelativeGain[:2,:,moduleId,:,:])     # --> 250x512x128
        calibFileh5.close()
    
        print('Preparing mask...')
        maskFileh5 = h5py.File(maskFile,'r')
        '''
        calib_lineParam_Var_Mean =  maskFileh5['lineParam_Var_Mean']
        lineParam_Var_Mean = np.squeeze(calib_lineParam_Var_Mean[:, :, moduleId, :, :])
        '''
        calib_maxBackMeanMap =  maskFileh5['maxBackMeanMap']
        maxBackMeanMap = np.squeeze(calib_maxBackMeanMap[:, moduleId, :, :])
        calib_analogMax_HighGain =  maskFileh5['analogMax_HighGain']
        analogMax_HighGain = np.squeeze(calib_analogMax_HighGain[:, moduleId, :, :])
        calib_Badpixel =  maskFileh5['badPixelMask_ByWater']
        BadpixelAll = np.squeeze(calib_Badpixel[:, :, moduleId, :, :])
        if(withOrNoMaskFlag=='withMask'):
            BadpixelAll[BadpixelAll > 0] = 1
        elif(withOrNoMaskFlag=='noMask'):
            BadpixelAll[BadpixelAll != 1] = 0   #just keep the boarders
            theDarkThreshold = 0*theDarkThreshold
            maxBackMeanMap = 0*maxBackMeanMap + 3000000
        
        BadpixelAll = 1 - BadpixelAll
        maskFileh5.close()
    
        print('data read in ' + str((time.time() - time_time)), flush=True)
        
        Proc, procMask  = AGIPDCorrectionRoutine.AGIPDCorrectTensor_multiprocessing(dataSet, 
                                                                                cellidSet, 
                                                                                GainLevel, 
                                                                                GainLevelStds, 
                                                                                GainSTDLambda, 
                                                                                AnalogOffset, 
                                                                                RelativeGain, 
                                                                                BadpixelAll,
                                                                                analogMax_HighGain)
                                       
        del AnalogOffset
        del GainLevelStds
        del GainLevel
        del RelativeGain
        del dataSet
        print('Proc is ready after ' + str(time.time() - time_time) + ' seconds', flush=True)
        
        if(withOrNoMaskFlag=='withMask'):
            if(FLAG_Make_FullProc_Mask == 1):
                print('Calculating Proc background, it takes a few minutes', flush=True)
                bckMP = RGFLib_multiproc.RSGImage_by_Image_TensorPy_multiproc(Proc,
                                                                            inMask = procMask,
                                                                            winX = 32,
                                                                            winY = 32)
                bckAVG = bckMP[0]
                bckSTD = bckMP[1]
                procMask[ (bckAVG > maxBackMeanMap[cellidSet]) &
                          ( (Proc > bckAVG+3*bckSTD) | (Proc < bckAVG-3*bckSTD) ) ] = 0
                procMask[ (bckAVG < theDarkThreshold[cellidSet]) &
                          (Proc > theDarkThreshold[cellidSet]) ] = 0 
                del bckAVG
                del bckSTD
                del bckMP
    print('SNRs map and mask is ready after ' + str(time.time() - time_time) + ' seconds', flush=True)
    print('Calling RPF...', flush=True)
    if(FLAG_Use_XFEL_Proc==0):
        nPeaks, peakListTensor = RPFPy.robustPeakFinderPyFunc_multiproc( \
                                    inData=Proc, 
                                    inMask=procMask,
                                    minPeakValMap=theDarkThreshold[cellidSet],
                                    maxBackMeanMap=maxBackMeanMap[cellidSet],
                                    bckSNR=bckSNR)
    else:
        nPeaks, peakListTensor = RPFPy.robustPeakFinderPyFunc_multiproc( \
                                    inData=Proc, 
                                    inMask=procMask,
                                    bckSNR=bckSNR)
    

    print('Peaklist is ready after ' + str(time.time() - time_time) + ' seconds', flush=True)
    print('Making the total intensity estimate for each image')
    
    moduleIntensities = np.median(np.median(Proc*procMask, axis=1), axis=1)
    print('total intensity estimate is ready after ' + str(time.time() - time_time) + ' seconds', flush=True)
    
    print("--- making proc file ---")
    file_outData = h5py.File(outputFileName, "w")
    print('writing to ' + outputFileName, flush=True)
    if(includeData):
        file_outData.create_dataset(h5Entry + 'data', data = Proc)
        file_outData.create_dataset(h5Entry + 'mask', data = procMask)
    file_outData.create_dataset(h5Entry + 'RPF/moduleIntensities', data = moduleIntensities)
    file_outData.create_dataset(h5Entry + 'length', data = lengthSet)
    file_outData.create_dataset(h5Entry + 'cellId', data = cellidSet)
    file_outData.create_dataset(h5Entry + 'trainId', data = trainidSet)
    file_outData.create_dataset(h5Entry + 'pulseId', data = pulseidSet)
    file_outData.create_dataset(h5Entry + 'moduleId', data = np.array([moduleId]))
    file_outData.create_dataset(h5Entry + 'RPF/PeakList', data = peakListTensor)
    file_outData.create_dataset(h5Entry + 'RPF/nPeaks', data = nPeaks)
    file_outData.close()
    
    """PeakList
    -----------------------------------------------------------------------------------------
    Mass_Center_X, Mass_Center_Y, Mass_Total, Number of pixels in a peak, Maximum value, SNR
    -----------------------------------------------------------------------------------------
    """
    
    print("--- Processing this part took %s seconds ---" % (time.time() - time_time))
    print('finished at ' + str(time.time()), flush=True) 
    gc.collect()
    exit()