import h5py
import numpy as np
import matplotlib.pyplot as plt
import os
import gc
import fnmatch
from multiprocessing import Process, Queue, cpu_count
import sys
import time
from RobustGaussianFittingLibrary import textProgBar
import cfelpyutils.crystfel_utils as cfelCryst
import cfelpyutils.geometry_utils as cfelGeom
    
def subPartVCXIMaker(aQ, sub_outputFileNameFull, 
                        _numHits, modsList, flist, RPFProc,
                        rootEntry, includeData,
                        sub_dataset_imageIntensities  , 
                        sub_dataset_nPeaks            , 
                        sub_dataset_cellID            , 
                        sub_dataset_pulseID           , 
                        sub_dataset_trainID           , 
                        sub_dataset_frameNumber       , 
                        sub_dataset_moduleAvailable   , 
                        sub_dataset_peakRowsPosRaw    , 
                        sub_dataset_peakClmsPosRaw    , 
                        sub_dataset_peakTotalIntensity, 
                        sub_dataset_peakNPixels       , 
                        sub_dataset_peakMaximumValue  , 
                        sub_dataset_peakSNR ):
    if(includeData):
        sub_dataset_data = h5py.VirtualLayout(shape=(_numHits, 16*512, 128), dtype='float32')
        sub_dataset_mask = h5py.VirtualLayout(shape=(_numHits, 16*512, 128), dtype='ubyte')
        for modCnt in modsList:
            #print('Mapping data for module: ' + str(modCnt), flush=True)
            image_numbers = sub_dataset_frameNumber[:, modCnt]
            flagIfModuleAvailable = sub_dataset_moduleAvailable[:, modCnt]
            if (not flagIfModuleAvailable.any()):
                aQ.put(list([0]))
                return()
            modFileInd = np.where(modsList==modCnt)
            filename = flist[modFileInd[0][0]]
            RPFFile = h5py.File(RPFProc + filename,'r')
            moduleIdStr = str(modCnt)
            h5Entry = rootEntry + moduleIdStr + 'CH0:xtdf/image/'
            dset_data = RPFFile[h5Entry + 'data']
            dset_mask = RPFFile[h5Entry + 'mask']
            sub_dataset_data[flagIfModuleAvailable==1, modCnt*512:(modCnt+1)*512, :] = \
                h5py.VirtualSource(dset_data)[image_numbers[flagIfModuleAvailable==1], :, :]
            sub_dataset_mask[flagIfModuleAvailable==1, modCnt*512:(modCnt+1)*512, :] = \
                h5py.VirtualSource(dset_mask)[image_numbers[flagIfModuleAvailable==1], :, :]
            RPFFile.close()  
    '''
    ################### the goal is the following structure #################3
    # f_N number of frames
    entry_1
        data_1
            data    f_N x 8192 x 128
            mask    f_N x 8192 x 128
        instrument_1
            detector_1
                data    link to entry_1/data_1/data
        result_1 
            nPeaks              f_N
            peakMaximumValue    f_N x 1024
            peakNPixels         f_N x 1024
            peakSNR             f_N x 1024
            peakTotalIntensity  f_N x 1024
            peakXPosRaw         f_N x 1024
            peakYPosRaw         f_N x 1024
    instrument 
        cellID                  f_N
        pulseID                 f_N
        trainID                 f_N
    ###########################################################################
    '''
    print('creating one file:\n '+sub_outputFileNameFull)
    file_outData = h5py.File(sub_outputFileNameFull, 'w')
    if(includeData):
        file_outData.create_virtual_dataset( 'entry_1/data_1/data', sub_dataset_data, fillvalue=0)
        file_outData.create_virtual_dataset( 'entry_1/data_1/mask', sub_dataset_mask, fillvalue=0)
    file_outData.create_dataset( 'entry_1/data_1/length', data = np.array([_numHits]))
    file_outData.create_dataset( 'entry_1/result_1/peakMaximumValue', data = sub_dataset_peakMaximumValue)
    file_outData.create_dataset( 'entry_1/result_1/peakNPixels', data = sub_dataset_peakNPixels)
    file_outData.create_dataset( 'entry_1/result_1/peakSNR', data = sub_dataset_peakSNR)
    file_outData.create_dataset( 'entry_1/result_1/peakTotalIntensity', data = sub_dataset_peakTotalIntensity)
    file_outData.create_dataset( 'entry_1/result_1/peakXPosRaw', data = sub_dataset_peakClmsPosRaw)
    file_outData.create_dataset( 'entry_1/result_1/peakYPosRaw', data = sub_dataset_peakRowsPosRaw)
    file_outData.create_dataset( 'entry_1/result_1/imageIntensities', data = sub_dataset_imageIntensities)
    file_outData.create_dataset( 'entry_1/result_1/nPeaks', data = sub_dataset_nPeaks)
    file_outData.create_dataset( 'entry_1/result_1/frameNumber', data = sub_dataset_frameNumber)
    file_outData.create_dataset( 'instrument/cellID', data = sub_dataset_cellID)
    file_outData.create_dataset( 'instrument/pulseID', data = sub_dataset_pulseID)
    file_outData.create_dataset( 'instrument/trainID', data = sub_dataset_trainID)
    file_outData.close()
    gc.collect()
    aQ.put(list([1]))

if __name__ == '__main__':
    np.set_printoptions(suppress=True)
    np.set_printoptions(precision=2)
    start_time = time.time()
    np.set_printoptions(threshold=sys.maxsize)
    print('Joiner started...', flush=True)

    RPFProc = sys.argv[1]
    outputFileNameFull = sys.argv[2]
    partCntStr = sys.argv[3]
    HIT_THRESHOLD = int(sys.argv[4])
    peakAcceptableSNR = float(sys.argv[5])
    dataReductionSNR = float(sys.argv[6])
    rootEntry = sys.argv[7]
    maxNumberOfImagesInEuXFEL = int(sys.argv[8])
    numHitsInSubpart = int(sys.argv[9])
    minimumResolution = float(sys.argv[10])
    maximumResolution = float(sys.argv[11])
    geomFile = sys.argv[12]
    minNumPixelsInPeaks = int(sys.argv[13])
    includeData = int(sys.argv[14])

    myGeom = cfelCryst.load_crystfel_geometry(geomFile)
    geomMap = cfelGeom.compute_pix_maps(myGeom)
    geomMap_X = geomMap[0]
    geomMap_Y = geomMap[1]
    
    fileNameTemplate = '*S'+partCntStr+'.h5'
    flist = fnmatch.filter(os.listdir(RPFProc), fileNameTemplate)
    flist.sort()
    
    numOfModules = len(flist)

    modsList = np.zeros(numOfModules, dtype='uint8')
    for modCnt in range(numOfModules):
        ReadFileName = flist[modCnt]
        modsList[modCnt] = int(ReadFileName.split('AGIPD',2)[1][:2])
    
    print('modules '+str(modsList)+' are available.')

    print('Reading images information and creating a stack out of all modules...', flush=True)
    maxNumberOfImagesInEuXFEL = 100000
    imgInfo = np.zeros((maxNumberOfImagesInEuXFEL*numOfModules,8), dtype='uint64')
    peakList = np.zeros((maxNumberOfImagesInEuXFEL*numOfModules,1024, 6), dtype='float32')
    numOfImgs= 0

    for modCnt in modsList:
        print('reading peaklists for module: ' + str(modCnt) + '...', end='', flush=True)
        modFileInd = np.where(modsList==modCnt)
        rFileName = flist[modFileInd[0][0]]
        RPFFile = h5py.File(RPFProc + rFileName,'r')
        moduleIdStr = str(modCnt)

        h5Entry = rootEntry + moduleIdStr + 'CH0:xtdf/image/'
        trainId = RPFFile[h5Entry + 'trainId'][...]
        pulseId = RPFFile[h5Entry + 'pulseId'][...]
        cellId = RPFFile[h5Entry + 'cellId'][...]
        moduleId = RPFFile[h5Entry + 'moduleId'][...]
        _nPeaks = RPFFile[h5Entry + 'RPF/nPeaks'][...]
        _moduleIntensities = RPFFile[h5Entry + 'RPF/moduleIntensities'][...]
        _peakList = RPFFile[h5Entry + 'RPF/PeakList'][...]
        
        RPFFile.close()
        numOfImgs_mod = _peakList.shape[0]        
        print(' finished, ', end='', flush=True)
        
        if(maximumResolution>0):
            tmp = _peakList[:, :, 0].astype('int')
            _peaksRow = tmp + modCnt*512
            print(' Xs, ', end='', flush=True)
            tmp = _peakList[:, :, 1].astype('int')
            _peaksClm = tmp
            print(' Ys, ', end='', flush=True)
            peaksX = geomMap_X[_peaksRow, _peaksClm]
            peaksY = geomMap_Y[_peaksRow, _peaksClm]
            
            peaksResolution = (peaksX**2 +peaksY**2)**0.5
            print(' --> Ress, ', end='', flush=True)

            for imgCnt in range(numOfImgs_mod):
                _peakListImg = _peakList[imgCnt, :_nPeaks[imgCnt], :].copy()
                _peaksResolution = peaksResolution[imgCnt, :_nPeaks[imgCnt]]
                indspeaks_peak = np.where((_peaksResolution < maximumResolution) & 
                                          (_peaksResolution > minimumResolution) &
                                          (_peakListImg[:, 3] >= minNumPixelsInPeaks))[0]
                _peakList[imgCnt, :, :] = 0
                _peakList[imgCnt, :indspeaks_peak.shape[0], :] = _peakListImg[indspeaks_peak, :]
            print(' fixed!', flush=True)
            _nPeaks = (_peakList[:, :, 5]>0).sum(1)
        else:
            print(' no resolution limit applied')
                        
        print('Only keep Bragg peaks with SNR above ' + str(peakAcceptableSNR) + '...', end='', flush=True)
        for imgCnt in range(numOfImgs_mod):
            _peakListImg = _peakList[imgCnt, :_nPeaks[imgCnt], :].copy()
            indspeaks_peak = np.where(_peakListImg[:, 5] > peakAcceptableSNR)[0]
            _peakList[imgCnt, :, :] = 0
            _peakList[imgCnt, :indspeaks_peak.shape[0], :] = _peakListImg[indspeaks_peak, :]
        _nPeaks = (_peakList[:, :, 5]>peakAcceptableSNR).sum(1)

        imgInfo[numOfImgs: numOfImgs + numOfImgs_mod,0] = trainId
        imgInfo[numOfImgs: numOfImgs + numOfImgs_mod,1] = pulseId
        imgInfo[numOfImgs: numOfImgs + numOfImgs_mod,2] = cellId
        imgInfo[numOfImgs: numOfImgs + numOfImgs_mod,3] = moduleId
        imgInfo[numOfImgs: numOfImgs + numOfImgs_mod,4] = np.arange(numOfImgs_mod)
        imgInfo[numOfImgs: numOfImgs + numOfImgs_mod,5] = _nPeaks
        imgInfo[numOfImgs: numOfImgs + numOfImgs_mod,6] = (_peakList[:, :, 5]>dataReductionSNR).sum(1)
        imgInfo[numOfImgs: numOfImgs + numOfImgs_mod,7] = _moduleIntensities
        peakList[numOfImgs: numOfImgs + numOfImgs_mod, :, :] = _peakList
        numOfImgs += numOfImgs_mod
        print('Stack for module: ' + str(moduleId[0]) + ' is made!', flush=True)
        
    imgInfo = imgInfo[:numOfImgs, :]
    peakList = peakList[:numOfImgs, :, :]
    print('Making the stack of ' +str(numOfImgs) +' imageInfos took ' + str(time.time() - start_time))
    inds = np.lexsort((imgInfo[:,3], imgInfo[:,1], imgInfo[:,0]))
    imgInfo = imgInfo[inds,:]
    peakList = peakList[inds, :, :]
    print('Stack sorted according to TID, PID and MID')
    
    (_, counts) = np.unique(imgInfo[:, 3], return_counts=True)
    numFrames = counts.max()
    print('maximum number of frames is: ' + str(numFrames))
    dataset_peakMaximumValue = np.zeros((numFrames, 1024), dtype='float32')
    dataset_peakNPixels = np.zeros((numFrames, 1024), dtype='ubyte')
    dataset_peakSNR = np.zeros((numFrames, 1024), dtype='float32')
    dataset_peakTotalIntensity = np.zeros((numFrames, 1024), dtype='float32')
    dataset_peakRowsPosRaw = np.zeros((numFrames, 1024), dtype='float32')
    dataset_peakClmsPosRaw = np.zeros((numFrames, 1024), dtype='float32')
    dataset_imageIntensities = np.zeros((numFrames), dtype='float32')
    dataset_nPeaks = np.zeros((numFrames), dtype='uint16')
    dataset_cellID = np.zeros(numFrames, dtype='uint64')
    dataset_pulseID = np.zeros(numFrames, dtype='uint64')
    dataset_trainID = np.zeros(numFrames, dtype='uint64')
    dataset_frameNumber = np.zeros((numFrames, 16), dtype='uint64')
    dataset_moduleAvailable = np.zeros((numFrames, 16), dtype='uint64')
    numHits = 0
    imgCnt = 0
    pBar = textProgBar(numOfImgs, title='Counting number of peaks in each full image')
    while(imgCnt < numOfImgs):
        TID = imgInfo[imgCnt,0]
        PID = imgInfo[imgCnt,1]
        CID = imgInfo[imgCnt,2]
        
        numTotalPeaks = int(0)
        numHighSNRPeaks = int(0)
        imgIntensity = float(0)
        framesInHits = []
        modulesInHit = []
        indsInStack = []
        
        peakRowsPosRaw = np.zeros(16*1024, dtype='float32')
        peakClmsPosRaw = np.zeros(16*1024, dtype='float32')
        peakTotalIntensity = np.zeros(16*1024, dtype='float32')
        peakNPixels = np.zeros(16*1024, dtype='ubyte')
        peakMaximumValue = np.zeros(16*1024, dtype='float32')
        peakSNR = np.zeros(16*1024, dtype='float32')
        
        while( (imgInfo[imgCnt,0] == TID) & (imgInfo[imgCnt,1] == PID) ):
            NPeaks = int(imgInfo[imgCnt,5])
            numHighSNRPeaks += imgInfo[imgCnt,6]
            imgIntensity += imgInfo[imgCnt,7]
            indsInStack.append(imgCnt)
            MID = imgInfo[imgCnt,3]
            modulesInHit.append(MID)
            framesInHits.append(imgInfo[imgCnt,4])

            if(NPeaks>0):
                peakRowsPosRaw[numTotalPeaks:numTotalPeaks+NPeaks] = peakList[imgCnt, :NPeaks, 0].copy() + MID*512
                peakClmsPosRaw[numTotalPeaks:numTotalPeaks+NPeaks] = peakList[imgCnt, :NPeaks, 1].copy() 
                peakTotalIntensity[numTotalPeaks:numTotalPeaks+NPeaks] = peakList[imgCnt, :NPeaks, 2].copy() 
                peakNPixels[numTotalPeaks:numTotalPeaks+NPeaks] = peakList[imgCnt, :NPeaks, 3].copy() 
                peakMaximumValue[numTotalPeaks:numTotalPeaks+NPeaks] = peakList[imgCnt, :NPeaks, 4].copy() 
                peakSNR[numTotalPeaks:numTotalPeaks+NPeaks] = peakList[imgCnt, :NPeaks, 5].copy() 

            numTotalPeaks += NPeaks
            imgCnt += 1
            pBar.go()
            if (imgCnt>=numOfImgs):
                break
        if((numHighSNRPeaks >= HIT_THRESHOLD) & (numTotalPeaks < 1024)):
            dataset_imageIntensities[numHits] = imgIntensity/len(modulesInHit)
            dataset_nPeaks[numHits] = numTotalPeaks
            dataset_cellID[numHits] = CID
            dataset_pulseID[numHits] = PID
            dataset_trainID[numHits] = TID
            dataset_frameNumber[numHits, modulesInHit] = framesInHits
            dataset_moduleAvailable[numHits, modulesInHit] = 1
            dataset_peakRowsPosRaw[numHits,...] = peakRowsPosRaw[:1024]
            dataset_peakClmsPosRaw[numHits,...] = peakClmsPosRaw[:1024]
            dataset_peakTotalIntensity[numHits,...] = peakTotalIntensity[:1024]
            dataset_peakNPixels[numHits,...] = peakNPixels[:1024]
            dataset_peakMaximumValue[numHits,...] = peakMaximumValue[:1024]
            dataset_peakSNR[numHits,...] = peakSNR[:1024]
            numHits += 1
    del pBar

    dataset_imageIntensities    = dataset_imageIntensities[:numHits]
    dataset_nPeaks              = dataset_nPeaks[:numHits]
    dataset_cellID              = dataset_cellID[:numHits]
    dataset_pulseID             = dataset_pulseID[:numHits]
    dataset_trainID             = dataset_trainID[:numHits]
    dataset_frameNumber         = dataset_frameNumber[:numHits]
    dataset_moduleAvailable     = dataset_moduleAvailable[:numHits]
    dataset_peakRowsPosRaw      = dataset_peakRowsPosRaw[:numHits]
    dataset_peakClmsPosRaw      = dataset_peakClmsPosRaw[:numHits]
    dataset_peakTotalIntensity  = dataset_peakTotalIntensity[:numHits]
    dataset_peakNPixels         = dataset_peakNPixels[:numHits]
    dataset_peakMaximumValue    = dataset_peakMaximumValue[:numHits]
    dataset_peakSNR             = dataset_peakSNR[:numHits]

    if (numHits<2):
        print('no hits were found...exit', flush=True)
        exit()
    if (numOfImgs>0):
        print('number of hits/frames is: ' + str(numHits) + '/' + str(numFrames))
        print('percentage of hits: ', str(np.floor(10000*numHits/numFrames)/100))
        print('Hits were counted in ' + str(time.time() - start_time) + ' seconds', flush=True)
    
    aQ = Queue()
    numSubparts = int(np.ceil(numHits/numHitsInSubpart))
    hitIdEnd = -1
    
    for subpartCnt in range(numSubparts):
        hitIdStart = subpartCnt*numHitsInSubpart
        hitIdStart = np.maximum(hitIdStart, hitIdEnd)
        hitIdEnd = np.minimum((subpartCnt+1)*numHitsInSubpart, numHits)
        if (numHits - hitIdEnd < 10):
            hitIdEnd = numHits
        
        sub_dataset_imageIntensities    = dataset_imageIntensities[hitIdStart:hitIdEnd] 
        sub_dataset_nPeaks              = dataset_nPeaks[hitIdStart:hitIdEnd]                
        sub_dataset_cellID              = dataset_cellID[hitIdStart:hitIdEnd]                
        sub_dataset_pulseID             = dataset_pulseID[hitIdStart:hitIdEnd]               
        sub_dataset_trainID             = dataset_trainID[hitIdStart:hitIdEnd]               
        sub_dataset_frameNumber         = dataset_frameNumber[hitIdStart:hitIdEnd]           
        sub_dataset_moduleAvailable     = dataset_moduleAvailable[hitIdStart:hitIdEnd]       
        sub_dataset_peakRowsPosRaw      = dataset_peakRowsPosRaw[hitIdStart:hitIdEnd]        
        sub_dataset_peakClmsPosRaw      = dataset_peakClmsPosRaw[hitIdStart:hitIdEnd]        
        sub_dataset_peakTotalIntensity  = dataset_peakTotalIntensity[hitIdStart:hitIdEnd]    
        sub_dataset_peakNPixels         = dataset_peakNPixels[hitIdStart:hitIdEnd]           
        sub_dataset_peakMaximumValue    = dataset_peakMaximumValue[hitIdStart:hitIdEnd]      
        sub_dataset_peakSNR             = dataset_peakSNR[hitIdStart:hitIdEnd]               

        sub_numHits = hitIdEnd-hitIdStart
        subpartCntStr = '%03d' % subpartCnt
        sub_outputFileNameFull = outputFileNameFull[:-4]+'_'+subpartCntStr+'.cxi'
        Process(target = subPartVCXIMaker, args = (aQ, sub_outputFileNameFull, 
                                                   sub_numHits, modsList, 
                                                   flist, RPFProc, 
                                                   rootEntry, includeData,
                                                   sub_dataset_imageIntensities  , 
                                                   sub_dataset_nPeaks            , 
                                                   sub_dataset_cellID            , 
                                                   sub_dataset_pulseID           , 
                                                   sub_dataset_trainID           , 
                                                   sub_dataset_frameNumber       , 
                                                   sub_dataset_moduleAvailable   , 
                                                   sub_dataset_peakRowsPosRaw    , 
                                                   sub_dataset_peakClmsPosRaw    , 
                                                   sub_dataset_peakTotalIntensity, 
                                                   sub_dataset_peakNPixels       , 
                                                   sub_dataset_peakMaximumValue  , 
                                                   sub_dataset_peakSNR )).start()
    subpartCnt = 0
    while(subpartCnt<numSubparts):
        if (not aQ.empty()):
            if(aQ.get()[0]==0):
                print('one subpart failed because a module was missing in the entire sub-part.')
                print('I dont know how to fill in empty virtual datsets')
            subpartCnt += 1
            
    

    print('it took ' + str(int(time.time() - start_time)) + ' seconds to make the cxi file')
    gc.collect()
    exit()