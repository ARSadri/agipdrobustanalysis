import h5py
import time
import sys
import numpy

if __name__ == '__main__':
    start_time = time.time()
    print('Gatherer 48 files together for AGIPD-1M calibration constants from the dark ')
    ########################## get the inputs from slurm ########################    
    argcnt = 0
    argcnt += 1
    scratchFolder = sys.argv[argcnt]
    argcnt += 1
    numModules = int(sys.argv[argcnt])
    argcnt += 1
    numMemCells = int(sys.argv[argcnt])
    argcnt += 1
    moduleRowPixN = int(sys.argv[argcnt])
    argcnt += 1
    moduleClmPixN = int(sys.argv[argcnt])
    argcnt += 1
    runNumDarkHighGain = int(sys.argv[argcnt])
    argcnt += 1
    runNumDarkMediumGain = int(sys.argv[argcnt])
    argcnt += 1
    runNumDarkLowGain = int(sys.argv[argcnt])
    argcnt += 1
    badDataFileName = sys.argv[argcnt]

    runNumbers = [runNumDarkHighGain, runNumDarkMediumGain, runNumDarkLowGain]
    numRuns = len(runNumbers)
    
    ########################## gather 48 files for AGIPD three dark runs into one file ########################    
    #hotFlickMap, coldFlickMap  
    AnalogOffset = numpy.zeros((numRuns, numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='float32')
    AnalogStds = numpy.zeros((numRuns, numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='float32')
    GainLevel = numpy.zeros((numRuns, numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='float32')
    GainLevelStds = numpy.zeros((numRuns, numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='float32')
    
    for moduleCnt in range(numModules):
        moduleCntStr = "%02d" % moduleCnt
        for runCnt, runNumber in enumerate(runNumbers):
            print('module ' +str(moduleCnt) + ' run ' +str(runCnt), flush=True)
            runNumberStr = "%04d" % runNumber
            readDIR = scratchFolder + '/r' + runNumberStr + '/'
            ReadFileName = readDIR + 'pixelModel-AGIPD' + moduleCntStr + '.h5'
            fileh5 = h5py.File(ReadFileName,'r')
            AnalogOffset[runCnt, :, moduleCnt, :, :] = fileh5['AnalogOffset']
            AnalogStds[runCnt, :, moduleCnt, :, :] = fileh5['AnalogStds']
            GainLevel[runCnt, :, moduleCnt, :, :] = fileh5['GainLevel']
            GainLevelStds[runCnt, :, moduleCnt, :, :] = fileh5['GainLevelStds']
            fileh5.close()
    # 3-stages x 250-memorycells x 16-modules x 512-rows x 128-clmns
    DigitalGainLevel = numpy.zeros((numRuns, numMemCells, numModules, moduleRowPixN, moduleClmPixN), dtype='float32')

    GainSTDLambda = 6.0
    DGL1 = GainLevel[0] + GainSTDLambda*GainLevelStds[0]
    DGL2 = GainLevel[1] - GainSTDLambda*GainLevelStds[1]
    DigitalGainLevel[0] = np.maximum(DGL1, GainLevel[0] + 0.2*(GainLevel[1] - GainLevel[0]))
    DigitalGainLevel[1] = np.minimum(DGL2, GainLevel[1] - 0.2*(GainLevel[1] - GainLevel[0]))
    DigitalGainLevel[2] = 32000

    RelativeGain = numpy.zeros((numRuns, numMemCells, numModules, moduleRowPixN, moduleClmPixN), 'uint8')
    RelativeGain[0] = 1
    RelativeGain[1] = 38    #according to Cheetah
    RelativeGain[2] = 180
    
    outputFileNameFull = scratchFolder + '/' + 'Cheetah-AGIPD-calib.h5'
    file_outData = h5py.File(outputFileNameFull, "w")
    file_outData.create_dataset('AnalogOffset', data = AnalogOffset)
    file_outData.create_dataset('AnalogStds', data = AnalogStds)
    file_outData.create_dataset('GainLevel',data = GainLevel)
    file_outData.create_dataset('GainLevelStds',data = GainLevelStds)
    file_outData.create_dataset('DigitalGainLevel', data = DigitalGainLevel)
    file_outData.create_dataset('RelativeGain', data = RelativeGain)
    file_outData.close()

    print("--- Finishing in %s seconds ---" % (time.time() - start_time))
    print("--- Finishing at %s ---" % time.time())
    exit()