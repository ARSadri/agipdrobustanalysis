# 1- Add maximum and minimum values for each stage for each pixel
#   1-1- Find a way to add parameters of the line that connects them
# 2- calibration of the norms of background
#   2-1- remember that above SNR3 can be masked 
import h5py
import numpy as np
import os
import fnmatch
import time
import sys
import gc
from multiprocessing import Process, Queue, cpu_count

import RGFLib.RobustGausFitLibMultiprocPy as RGFLib_multiproc
from textProgBar import textProgBar
import AGIPDCorrectionRoutine

from scipy.stats import norm

np.set_printoptions(suppress=True)
np.set_printoptions(precision=2)

lowestSNR = 3
highestSNR = 8
middleSNR = 5.0

def gkern(winSide):
    lim = winSide//2 + (winSide % 2)/2
    x = np.linspace(-lim, lim, winSide+1)
    kern1d = np.diff(norm.cdf(x))
    kern2d = np.outer(kern1d, kern1d)
    _bellShapedCurve = kern2d/(kern2d.max())
    _bellShapedCurveMask = np.zeros((winSide,winSide), dtype='uint8')
    _bellShapedCurveMask[_bellShapedCurve>0.01]=1
    return (_bellShapedCurve, _bellShapedCurveMask)

def adverserialPeakMaker(f_N, r_N, c_N, bckStd):
    nh_peak = np.zeros((f_N, r_N, c_N), dtype='float32')
    nh_label = np.zeros((f_N, r_N, c_N), dtype='uint8')
    for sampleCnt in range(f_N):
        numPeaks = 1 + np.int(5*np.random.rand(1))
        winSide = 2*((3*np.random.rand(numPeaks)).astype('int'))+1
        
        _nonHits_peak  = np.zeros((r_N, c_N), dtype='float32')
        _nonHits_label = np.zeros((r_N, c_N), dtype='uint8')
        
        for cnt in range(numPeaks):    
            randX = np.random.rand(1)
            randY = np.random.rand(1)
            randLocsRow = int(r_N*np.random.rand(1))
            randLocsClm = int(c_N*np.random.rand(1))
            
            if(randLocsRow < 1 + winSide[cnt]//2):
                randLocsRow = 1 + winSide[cnt]//2
            if(randLocsRow + 1 + winSide[cnt]//2 >= r_N-1):
                randLocsRow = r_N-1 - (1 + winSide[cnt]//2)
            if(randLocsClm < 1 + winSide[cnt]//2):
                randLocsClm = 1 + winSide[cnt]//2
            if(randLocsClm + 1 + winSide[cnt]//2 >= c_N-1):
                randLocsClm = c_N-1 - (1 + winSide[cnt]//2)
            
            bellShapedCurve, bellShapedCurveMask = gkern(winSide[cnt])
            
            peakSNR = lowestSNR + (highestSNR-lowestSNR)*np.random.rand(1)
                             
            winXStart = (randLocsRow - winSide[cnt]//2).astype('int')
            winXEnd = (randLocsRow + 1 + winSide[cnt]//2).astype('int')
            winYStart = (randLocsClm - winSide[cnt]//2).astype('int')
            winYEnd = (randLocsClm + 1 + winSide[cnt]//2).astype('int')
            
            
            bellShapedCurve = peakSNR*bckStd[ winXStart : winXEnd, winYStart : winYEnd ]*bellShapedCurve
            _nonHits_peak[ winXStart : winXEnd, winYStart : winYEnd ] += bellShapedCurve
            if(peakSNR>=middleSNR):
                _nonHits_label[ winXStart : winXEnd, winYStart : winYEnd ] += bellShapedCurveMask
                
        _nonHits_label[_nonHits_label>0] = 1
        nh_peak[sampleCnt, :, :] = _nonHits_peak
                                        
        nh_label[sampleCnt, :, :] = _nonHits_label
    return(nh_peak, nh_label)

if __name__ == '__main__':
    time_time = time.time()
    print('Calibration of AGIPD by the water analysis for each module of AGIPD')
    print('PID ->' + str(os.getpid()))
    argCnt = 0
    ########################## get the inputs from slurm ########################    
    argCnt += 1
    rootFolder = sys.argv[argCnt]
    argCnt += 1
    rootEntry = sys.argv[argCnt]
    argCnt += 1
    WriteDIR = sys.argv[argCnt]
    argCnt += 1
    numMemCells = int(sys.argv[argCnt])
    argCnt += 1
    moduleRowPixN = int(sys.argv[argCnt])
    argCnt += 1
    moduleClmPixN = int(sys.argv[argCnt])
    argCnt += 1
    maxNumberFramesInParts = int(sys.argv[argCnt])
    argCnt += 1
    runCnt = int(sys.argv[argCnt])
    argCnt += 1
    moduleId = int(sys.argv[argCnt])
    argCnt += 1
    number_of_parts_to_process = int(sys.argv[argCnt])
    argCnt += 1
    maxNumCPUs = int(sys.argv[argCnt])
    argCnt += 1
    winX = int(sys.argv[argCnt])
    argCnt += 1
    winY = int(sys.argv[argCnt])
    argCnt += 1
    waterSNR = float(sys.argv[argCnt])
    argCnt += 1
    calibFile = sys.argv[argCnt]
    argCnt += 1
    maskFile = sys.argv[argCnt]
    argCnt += 1
    waterSNR_prob = float(sys.argv[argCnt])
    argCnt += 1
    numAdvers = int(sys.argv[argCnt])
    numImagesToAnalyse = 100
    
    print('rootFolder->' + ' ' + rootFolder)
    print('rootEntry->' + ' ' + rootEntry)
    print('WriteDIR->' + ' ' + WriteDIR)
    print('numMemCells->' + ' ' + str(numMemCells))
    print('moduleRowPixN->' + ' ' + str(moduleRowPixN))
    print('moduleClmPixN->' + ' ' + str(moduleClmPixN))
    print('maxNumberFramesInParts->' + ' ' + str(maxNumberFramesInParts))
    print('runCnt->' + ' ' + str(runCnt))
    print('moduleId->' + ' ' + str(moduleId))
    print('number_of_parts_to_process->' + ' ' + str(number_of_parts_to_process))
    print('maxNumCPUs->' + ' ' + str(maxNumCPUs))
    print('winX->' + ' ' + str(winX))
    print('winY->' + ' ' + str(winY))
    
    ########################## Read a few parts for a module ########################

    runCntStr = "%04d" % runCnt
    moduleIdStr = "%02d" % moduleId
    fileNameTemplate = 'RAW-R*-AGIPD' + moduleIdStr + '-S*.h5'
    folderPrefix = rootFolder + 'r' + runCntStr + '/'
    flist = fnmatch.filter(os.listdir(folderPrefix), fileNameTemplate)
    flist.sort()
    number_of_parts_to_process = np.minimum(len(flist), number_of_parts_to_process)
    
    cellidSet_all = np.zeros((number_of_parts_to_process*maxNumberFramesInParts,1), dtype='uint32')
    dataSet_all = np.zeros((number_of_parts_to_process*maxNumberFramesInParts, 2, moduleRowPixN, moduleClmPixN), dtype='uint16')
    print('There are ' +str(number_of_parts_to_process) + ' parts to read')
    size_so_far = 0
    for cont in range(number_of_parts_to_process):
        rFileName = flist[cont]
        ReadFileName = folderPrefix + rFileName
        fileh5 = h5py.File(ReadFileName,'r')
        lengthEntry = rootEntry + str(moduleId) + 'CH0:xtdf/image/length'
        CellIDEntry = rootEntry + str(moduleId) + 'CH0:xtdf/image/cellId'
        ReadEntry = rootEntry + str(moduleId) + 'CH0:xtdf/image/data'
        cellidSet = fileh5[lengthEntry]
        numFrames = np.count_nonzero(cellidSet[:])
        if(numFrames==0):
            print("file " + ReadFileName + " has no data points")
            continue
        print('Reading: ' + ReadFileName +' with ' +str(numFrames)+ ' frames')
        cellidSet = fileh5[CellIDEntry]
        dataSet = fileh5[ReadEntry]
        cellidSet_all[size_so_far:size_so_far + numFrames] = cellidSet[:numFrames]
        dataSet_all[size_so_far:size_so_far + numFrames,:,:,:] = dataSet[:numFrames,:,:,:]
        size_so_far += numFrames
    
    size_so_far = np.minimum(size_so_far, numImagesToAnalyse)
    
    cellidSet_all = np.squeeze(cellidSet_all[:size_so_far])
    dataSet_all = dataSet_all[:size_so_far,:,:,:]
    
    print("--- reading h5 dataset took %s seconds ---" % (time.time() - time_time))
    print('size of the dataset is ' + str(dataSet_all.nbytes/(1024*1024*1024)) + ' GB')

    print('Preparing pre-calculated statistics...')
    calibFileh5 = h5py.File(calibFile,'r')
    calib_AnalogOffset =  calibFileh5['AnalogOffset']
    AnalogOffset = np.squeeze(calib_AnalogOffset[:,:,moduleId,:,:])
    calib_AnalogStds =  calibFileh5['AnalogStds']
    darkThresholds = np.squeeze(calib_AnalogStds[0,:,moduleId,:,:])
    calib_DigitalGainLevel =  calibFileh5['DigitalGainLevel']
    DigitalGainLevel = np.squeeze(calib_DigitalGainLevel[1,:,moduleId,:,:])     # --> 250x512x128
    calib_RelativeGain =  calibFileh5['RelativeGain']
    RelativeGain = np.squeeze(calib_RelativeGain[:2,:,moduleId,:,:])     # --> 250x512x128
    calibFileh5.close()
    
    print('Preparing mask...')
    maskFileh5 = h5py.File(maskFile,'r')
    badPixelMask =  maskFileh5['badPixelMask']  #stages x cells x modules x rows x clms
    badPixelMask = np.squeeze(badPixelMask[:, :, moduleId, :, :])
    badPixelMask[badPixelMask>0]=1
    badPixelMask = 1 - badPixelMask
    maskFileh5.close()
    
    print('Mask all local backgrounds where a pixel has switched gains', flush=True)
    backMask = np.squeeze(dataSet_all[:, 1, :, :])
    f_N = backMask.shape[0]
    r_N = backMask.shape[1]
    c_N = backMask.shape[2]
    r_nSeg = 8
    c_nSeg = 2
    r_winL = int(r_N/r_nSeg)
    c_winL = int(c_N/c_nSeg)
    
    print('Stages: 1', end='', flush=True)
    DGL1 = np.squeeze(DigitalGainLevel[cellidSet_all, :, :])
    darkThresholds = np.squeeze(darkThresholds[cellidSet_all, :, :])
    backMask[backMask <  DGL1] = 0
    backMask[backMask >= DGL1] = 1
    gainStageTensor = backMask.copy()
    print(', 2', end='', flush=True)
    backMask = backMask.reshape(f_N, r_nSeg, r_winL, c_N)
    print(', 3', end='', flush=True)
    backMask = backMask.reshape(f_N, r_nSeg, r_winL, c_nSeg, c_winL)
    print(', 4', end='', flush=True)
    backMask = np.swapaxes(backMask, 2, 3)
    print(', 5', end='', flush=True)
    backMask = backMask.reshape(f_N * r_nSeg * c_nSeg, r_winL, c_winL)
    print(', 6', end='', flush=True)
    backMask[backMask.max(1).max(1)==1, :, :] = 1
    print(', 7', end='', flush=True)
    backMask = backMask.reshape(f_N, r_nSeg, c_nSeg, r_winL, c_winL)
    print(', 8', end='', flush=True)
    backMask = np.swapaxes(backMask, 2, 3)
    print(', 9', end='', flush=True)
    backMask = backMask.reshape(f_N, r_nSeg, r_winL, c_N)
    print(', 10', end='', flush=True)
    backMask = 1-backMask.reshape(f_N, r_N, c_N)
    
    print('\nCalculating Proc of water', flush=True)
    proc_time = time.time()
    Proc, inMask  = AGIPDCorrectionRoutine.AGIPDCorrectTensor_multiprocessing(dataSet_all, 
                        cellidSet_all, DigitalGainLevel, AnalogOffset, RelativeGain, badPixelMask)
    print('Calculating Proc took ' + str(time.time() - proc_time) + ' seconds for ' + str(f_N) + ' frames')
    print('That is ' + str(int(f_N/(time.time() - proc_time))) + ' frames per second')
    Proc = Proc*inMask
    
    del AnalogOffset
    del DigitalGainLevel
    del RelativeGain
    gc.collect()
    print('Calculating Proc background', flush=True)
    bckMP = RGFLib_multiproc.RSGImage_by_Image_TensorPy_multiproc(Proc, 
                                                inMask = inMask,
                                                winX = 64,
                                                winY = 64)

    ################################################################################
    print('Calculating pixels separabilities', flush=True)
    SNRs_ALL = inMask*backMask*(Proc - bckMP[0])/(bckMP[1]+(np.finfo(np.float32).eps))
    print('Find those that have SNR above 8 because there might be a crystal', flush=True)
    SNRs_ALL[np.fabs(SNRs_ALL) <  8.0] = 0
    SNRs_ALL[np.fabs(SNRs_ALL) >= 8.0] = 1
    print('shorten Proc', flush=True)
    Proc = Proc[SNRs_ALL.max(1).max(1)==0]
    backMask = backMask[SNRs_ALL.max(1).max(1)==0]
    bckAVG = bckMP[0,SNRs_ALL.max(1).max(1)==0]
    bckStd = bckMP[1,SNRs_ALL.max(1).max(1)==0]
    darkThresholds = darkThresholds[SNRs_ALL.max(1).max(1)==0]
    print('towards sorting images by background intensity', flush=True)
    bckAVG_Medians = np.median(np.median(bckAVG, axis=1), axis=1)
    darkThresholds_Max = darkThresholds.max(1).max(1)
    print('remove dark images', flush=True)
    Proc = Proc[bckAVG_Medians>darkThresholds_Max, :, :]
    bckAVG = bckAVG[bckAVG_Medians>darkThresholds_Max, :, :]
    bckStd = bckStd[bckAVG_Medians>darkThresholds_Max, :, :]
    backMask = backMask[bckAVG_Medians>darkThresholds_Max, :, :]
    bckAVG_Medians = bckAVG_Medians[bckAVG_Medians>darkThresholds_Max]
    print('Do the sorting', flush=True)
    bckAVG_Medians_sortInds = np.argsort(bckAVG_Medians)
    Proc = Proc[bckAVG_Medians_sortInds[::int(bckAVG_Medians_sortInds.shape[0]/100)]]
    bckAVG = bckAVG[bckAVG_Medians_sortInds[::int(bckAVG_Medians_sortInds.shape[0]/100)]]
    bckStd = bckStd[bckAVG_Medians_sortInds[::int(bckAVG_Medians_sortInds.shape[0]/100)]]
    backMask = backMask[bckAVG_Medians_sortInds[::int(bckAVG_Medians_sortInds.shape[0]/100)]]
    
    f_N = Proc.shape[0]
    print('Reshape proc to ASICs', flush=True)
    Proc = Proc.reshape(f_N, r_nSeg, r_winL, c_N)
    Proc = Proc.reshape(f_N, r_nSeg, r_winL, c_nSeg, c_winL)
    Proc = np.swapaxes(Proc, 2, 3)
    Proc = Proc.reshape(f_N * r_nSeg * c_nSeg, r_winL, c_winL)
    print('Reshape background STDs to ASICs', flush=True)
    bckStd = bckStd.reshape(f_N, r_nSeg, r_winL, c_N)
    bckStd = bckStd.reshape(f_N, r_nSeg, r_winL, c_nSeg, c_winL)
    bckStd = np.swapaxes(bckStd, 2, 3)
    bckStd = bckStd.reshape(f_N * r_nSeg * c_nSeg, r_winL, c_winL)
    print('Reshape mask to ASICs', flush=True)
    backMask = backMask.reshape(f_N, r_nSeg, r_winL, c_N)
    backMask = backMask.reshape(f_N, r_nSeg, r_winL, c_nSeg, c_winL)
    backMask = np.swapaxes(backMask, 2, 3)
    backMask = backMask.reshape(f_N * r_nSeg * c_nSeg, r_winL, c_winL)

    Proc = Proc[backMask.max(1).max(1)==0]
    bckStd = bckStd[backMask.max(1).max(1)==0]
    print('If Proc is heavily zero in an ASIC, remove it', flush=True)
    procZeros = Proc.copy()
    procZeros[procZeros!=0] = 1
    Proc = Proc[procZeros.mean(1).mean(1)>0.8]
    bckStd = bckStd[procZeros.mean(1).mean(1)>0.8]
    print('creating many adverserial examples for ' + str(Proc.shape[0]) + ' images in Proc.', flush=True)
    
    nonHits_Back = np.zeros((numAdvers*Proc.shape[0], Proc.shape[1], Proc.shape[2]))
    nonHits_label = np.zeros((numAdvers*Proc.shape[0], Proc.shape[1], Proc.shape[2]))
    nonHits_peak = np.zeros((numAdvers*Proc.shape[0], Proc.shape[1], Proc.shape[2]))
    pBar = textProgBar(Proc.shape[0])
    for frmCnt in range(Proc.shape[0]):
        nh_peak, nh_label = adverserialPeakMaker(numAdvers, 64, 64, bckStd[frmCnt])
        nonHits_Back[numAdvers*frmCnt:numAdvers*(frmCnt+1), :, :] = np.tile(np.squeeze(Proc[frmCnt,:,:]), (numAdvers, 1, 1))
        nonHits_label[numAdvers*frmCnt:numAdvers*(frmCnt+1), :, :] = nh_label
        nonHits_peak[numAdvers*frmCnt:numAdvers*(frmCnt+1), :, :] = nh_peak
        pBar.go()
    del pBar
    nonHits_adverse = nonHits_Back + nonHits_peak
    print('Adveserial examples are ' + str(nonHits_adverse.shape[0]) +' images.', flush=True)
    
    outputFileName = WriteDIR + 'nonHits_Back' + moduleIdStr + '.npy'
    print('writing to ' + outputFileName)
    np.save(outputFileName, nonHits_Back)
    
    outputFileName = WriteDIR + 'nonHits_label' + moduleIdStr + '.npy'
    print('writing to ' + outputFileName)
    np.save(outputFileName, nonHits_label)

    outputFileName = WriteDIR + 'nonHits_peak' + moduleIdStr + '.npy'
    print('writing to ' + outputFileName)
    np.save(outputFileName, nonHits_peak)

    outputFileName = WriteDIR + 'nonHits_adverse' + moduleIdStr + '.npy'
    print('writing to ' + outputFileName)
    np.save(outputFileName, nonHits_adverse)

    print("--- Finishing in %s seconds ---" % (time.time() - time_time))
    print('Finished at ' + str(time.time()), flush=True)
    exit()