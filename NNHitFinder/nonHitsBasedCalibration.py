import os
import sys
import fnmatch
import time
import os
import numpy as np
import h5py
from RobustGaussianFittingLibrary import textProgBar


if __name__ == '__main__':
    time_time = time.time()
    inFolder = sys.argv[1]
    outputFolder = sys.argv[2]
    nonHit_nPeaks_threshold = int(sys.argv[3])
    
    print('inFolder ->' + inFolder)
    print('outputFolder ->' + outputFolder)
    print('nonHit_nPeaks_threshold -> ' + str(nonHit_nPeaks_threshold))
    
    if(not os.path.isdir(outputFolder)):
        print('Making the output folder')
        os.system('mkdir -p ' + outputFolder)
    
    flist = []
    for subdir, dirs, files in os.walk(inFolder):
        for file in files:
            filepath = subdir + file
            if filepath.endswith('.cxi'):
                flist.append(filepath)
    
    flist.sort()
    print(flist)
    numFiles = len(flist)
    print('There are ' + str(numFiles) + ' files to stitch')

    dataEntry = 'entry_1/data_1/data'
    maskEntry = 'entry_1/data_1/mask'
    CIDEntry = 'instrument/cellID'
    PIDEntry = 'instrument/pulseID'
    TIDEntry = 'instrument/trainID'
    nPeaksEntry = 'entry_1/result_1/nPeaks'
    
    imgTotalIntensities = np.zeros((numFiles*2000, 6), dtype='float64') - 1
    nImages_soFar = 0
    pBar = textProgBar(numFiles)
    for idx, cxiName in enumerate(flist):
        cxiFile = h5py.File(cxiName,'r')
        data = cxiFile[dataEntry]
        nPeaks = cxiFile[nPeaksEntry]
        CID = cxiFile[CIDEntry]
        PID = cxiFile[PIDEntry]
        TID = cxiFile[TIDEntry]
        nImages = nPeaks.shape[0]
        nPeaks = nPeaks[...]
        
        inds = nPeaks<nonHit_nPeaks_threshold
        
        totIntensity = np.zeros(nImages)
        totIntensity[inds] = np.median(np.median(data[inds, :, :], axis=1), axis=1)
        
        CID = CID[...]
        PID = PID[...]
        TID = TID[...]
        imgTotalIntensities[nImages_soFar:nImages_soFar + nImages, 0] = idx
        imgTotalIntensities[nImages_soFar:nImages_soFar + nImages, 1] = np.arange(nImages)
        imgTotalIntensities[nImages_soFar:nImages_soFar + nImages, 2] = CID
        imgTotalIntensities[nImages_soFar:nImages_soFar + nImages, 3] = PID
        imgTotalIntensities[nImages_soFar:nImages_soFar + nImages, 4] = TID
        imgTotalIntensities[nImages_soFar:nImages_soFar + nImages, 5] = totIntensity
        pBar.go()
    del pBar
    np.save(outputFolder + 'imgTotalIntensities.npy', imgTotalIntensities)
    
    '''    
        for imgCnt in range(nImages):
            if(nPeaks[imgCnt] < nonHit_nPeaks_threshold):
                _CID = CID[imgCnt]
                _PID = PID[imgCnt]
                _TID = TID[imgCnt]
                _data = data[imgCnt, :, :]
                #_mask = mask[imgCnt]
                inVec = _data#*_mask
                totIntensity = np.median(np.median(inVec, axis=1), axis=1)
                imgTotalIntensities[idx*2000 + imgCnt, 0] = idx
                imgTotalIntensities[idx*2000 + imgCnt, 1] = imgCnt
                imgTotalIntensities[idx*2000 + imgCnt, 2] = _CID
                imgTotalIntensities[idx*2000 + imgCnt, 3] = _PID
                imgTotalIntensities[idx*2000 + imgCnt, 4] = _TID
                imgTotalIntensities[idx*2000 + imgCnt, 5] = totIntensity
    
        cxiFile.close()
    np.save(outputFolder + 'imgTotalIntensities.npy', imgTotalIntensities)
    
    
    for cellCnt in list([np.sort(np.unique(imgTotalIntensities[:, 2]))]):
        inVec = imgTotalIntensities[imgTotalIntensities[:, 2]==cellCnt , 5]
        
    
        
    for idx, cxiName in enumerate(flist):
        cxiFile = h5py.File(cxiName,'r')
        
        data = cxiFile[dataEntry], data = data[...]
        nImages = data.shape[0]
        mask = cxiFile[maskEntry], mask = mask[...]
        
        CID = cxiFile[CIDEntry], CID = CID[...]
        PID = cxiFile[PIDEntry], PID = PID[...]
        TID = cxiFile[TIDEntry], TID = TID[...]
        nPeaks = cxiFile[nPeaksEntry], nPeaks = nPeaks[...]
        
        if(nPeaks < nonHit_nPeaks_threshold):
            for imgCnt in range(nImages):
                inVec = data[imgCnt]*mask[imgCnt]
                imgTotalIntensities[idx, imgCnt] = np.median(inVec[inVec>0])
    
        cxiFile.close()        
    '''