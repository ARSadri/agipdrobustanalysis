import h5py
import time
import sys
import numpy as np

if __name__ == '__main__':
    start_time = time.time()
    print('Gatherer 48 files together for AGIPD-1M ')
    ########################## get the inputs from slurm ########################    
    argcnt = 0
    argcnt += 1
    readDIR = sys.argv[argcnt]
    argcnt += 1
    numModules = int(sys.argv[argcnt])
    argcnt += 1
    numMemCells = int(sys.argv[argcnt])
    argcnt += 1
    moduleRowPixN = int(sys.argv[argcnt])
    argcnt += 1
    moduleClmPixN = int(sys.argv[argcnt])
    argcnt += 1
    waterRun = int(sys.argv[argcnt])
    argcnt += 1
    maskFile = sys.argv[argcnt]
    
    nonHits_Back = np.array([])
    nonHits_label = np.array([])
    nonHits_peak = np.array([])
    nonHits_adverse = np.array([])

    runNumberStr = "%04d" % waterRun

    moduleCnt = 0
    moduleCntStr = "%02d" % moduleCnt
    print('module ' +str(moduleCnt) + ' run ' + runNumberStr, flush=True)
    FileName = readDIR + 'nonHits_Back' + moduleCntStr + '.npy'
    nonHits_Back = np.load(FileName)
    FileName = readDIR + 'nonHits_label' + moduleCntStr + '.npy'
    nonHits_label = np.load(FileName)
    FileName = readDIR + 'nonHits_peak' + moduleCntStr + '.npy'
    nonHits_peak = np.load(FileName)
    FileName = readDIR + 'nonHits_adverse' + moduleCntStr + '.npy'
    nonHits_adverse = np.load(FileName)

    for moduleCnt in range(1, numModules):
        moduleCntStr = "%02d" % moduleCnt
        print('module ' +str(moduleCnt) + ' run ' + runNumberStr, flush=True)
        FileName = readDIR + 'nonHits_Back' + moduleCntStr + '.npy'
        tmp = np.load(FileName)
        nonHits_Back = np.vstack([nonHits_Back, tmp])
        FileName = readDIR + 'nonHits_label' + moduleCntStr + '.npy'
        tmp = np.load(FileName)
        nonHits_label = np.vstack([nonHits_label, tmp])
        FileName = readDIR + 'nonHits_peak' + moduleCntStr + '.npy'
        tmp = np.load(FileName)
        nonHits_peak = np.vstack([nonHits_peak, tmp])
        FileName = readDIR + 'nonHits_adverse' + moduleCntStr + '.npy'
        tmp = np.load(FileName)
        nonHits_adverse = np.vstack([nonHits_adverse, tmp])
    
    print('There are ' + str(nonHits_Back.shape[0]) + ' images in the dataset')
    
    np.save(readDIR + 'nonHits_Back.npy', nonHits_Back)
    np.save(readDIR + 'nonHits_label.npy', nonHits_label)
    np.save(readDIR + 'nonHits_peak.npy', nonHits_peak)
    np.save(readDIR + 'nonHits_adverse.npy', nonHits_adverse)
    
    print("--- Finishing in %s seconds ---" % (time.time() - start_time))
    print("--- Finishing at %s ---" % time.time())
    exit()