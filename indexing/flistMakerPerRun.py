import os
import sys
import fnmatch
import time

if __name__ == '__main__':
    
    folderPrefix = sys.argv[1]
    oneFListForEachCXIFlag = sys.argv[2]
    
    fileNameTemplate = '*.cxi'
    flist = fnmatch.filter(os.listdir(folderPrefix), fileNameTemplate)
    flist.sort()
    print(flist)
    if(oneFListForEachCXIFlag == 'True'):
        for cnt in range(len(flist)):
            _flist = flist[cnt]
            cntStr = "%03d" % cnt
            File_object = open(folderPrefix+'flist'+ cntStr +'.lst',"w")
            File_object.writelines(folderPrefix+_flist+'\n')
            File_object.close()
    else:
        File_object = open(folderPrefix+'flist.lst',"w")
        for L in flist:
            File_object.writelines(folderPrefix+L+'\n')
        File_object.close()

    print('flist in ' + folderPrefix + ' is made')
