import os
import sys
import fnmatch
import time
import os

if __name__ == '__main__':
    time_time = time.time()
    inFolder = sys.argv[1]
    print('inFolder ->' + inFolder)
    
    flist = []
    for subdir, dirs, files in os.walk(inFolder):
        for file in files:
            filepath = subdir + os.sep + file
            if filepath.endswith('.stream'):
                flist.append(filepath)
    
    flist.sort()
    numFiles = len(flist)
    print('There are ' + str(numFiles) + ' files to stitch')
    File_object = open(flist[0], "r")
    noneIndexed = 0
    totalHits = 0
    
    # read and write down to the end of geometry, once
    for line in File_object:
        if('----- End geometry file -----' in line):
            break

    inFile_hits = 0
    inFile_noneIndexed = 0
    for idx in range(numFiles):
        print(flist[idx], flush=True)
        File_object = open(flist[idx], "r")
        stitchStart = 0
        for line in File_object:
            if(stitchStart==0):
                if('----- End geometry file -----' in line):
                    stitchStart=1
                    print('non-indexables out of hits: ' + \
                                str(inFile_noneIndexed) + ' / ' + str(inFile_hits), flush=True)
                    inFile_hits = 0
                    inFile_noneIndexed = 0
            else:
                if( "indexed_by = " in line):
                    totalHits += 1
                    inFile_hits += 1
                if( "indexed_by = none" in line):
                    noneIndexed +=1      
                    inFile_noneIndexed +=1                    
    
    print('Indexed are: ' + str(totalHits-noneIndexed) + \
                ' out of ' + str(totalHits) + ' number of hits')
    print('The indexing rate would be: ' + \
                str(int(10000*(1-noneIndexed/totalHits))/100)+'%')    
    print('outputfileFull-> ' + outputfileFull)
    print('streams are stiched together after ' + \
                str(time.time()- time_time))
    print('script finished at ' + str(time.time()))