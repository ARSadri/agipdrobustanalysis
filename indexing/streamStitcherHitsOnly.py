#./../indexing/streamStitcher.py /gpfs/exfel/u/scratch/SPB/202001/p002450/yefanov/indexing-lyso/streams/ /gpfs/exfel/exp/SPB/202001/p002450/scratch/alireza/streams/Alex_withoutMask_peakSNR_8.0_hitSNT8.0.stream
import os
import sys
import fnmatch
import time
import os
from textProgBar import textProgBar

if __name__ == '__main__':
    
    time_time = time.time()
    inFolder = sys.argv[1]
    outputfileFull = sys.argv[2]
    print('inFolder ->' + inFolder)
    print('outputfileFull ->' + outputfileFull)
    
    flist = []
    for subdir, dirs, files in os.walk(inFolder):
        for file in files:
            filepath = subdir + os.sep + file
            if filepath.endswith('.stream'):
                flist.append(filepath)

    flist.sort()
    numFiles = len(flist)
    File_object = open(flist[0], "r")
    noneIndexed = 0
    totalHits = 0
    
    File_output = open(outputfileFull, "w")
    crystStream = []
    # read and write down to the end of geometry, once
    for line in File_object:
        crystStream.append(line)
        if('----- End geometry file -----' in line):
            break

    inFile_hits = 0
    inFile_noneIndexed = 0
    print('numFiles-> ' + str(numFiles))
    for idx in range(numFiles):
        for item in crystStream:
            File_output.write(item)
        crystStream = []
        File_object = open(flist[idx], "r")
        stitchStart = 0
        lCnt = 0
        num_ChunkToWrite = 0
        for line in File_object:
            if(stitchStart==0):
                if('----- End geometry file -----' in line):
                    stitchStart=1
                    inFile_hits = 0
                    inFile_noneIndexed = 0
                    beginChunkFlag = 0
            else:
                if(beginChunkFlag==0):
                    if('----- Begin chunk -----' in line):
                        beginChunkFlag = 1
                        currentChunk = []
                        nonHitFlag = 0
                    else:
                        crystStream.append(line)
                if(beginChunkFlag==1):
                    currentChunk.append(line)
                    if( "indexed_by = " in line):
                        totalHits += 1
                        inFile_hits += 1
                    if( "indexed_by = none" in line):
                        noneIndexed +=1      
                        inFile_noneIndexed +=1
                        nonHitFlag = 1
                    if('----- End chunk -----' in line):
                        num_ChunkToWrite += 1
                        beginChunkFlag = 0
                        if(nonHitFlag==0):
                            crystStream.append(currentChunk)    
            lCnt += 1            
            if(num_ChunkToWrite==100000):
                num_ChunkToWrite = 0
                for item in crystStream:
                    for _item in item:
                        File_output.write(_item)
                crystStream = []

        for item in crystStream:
            for _item in item:
                File_output.write(_item)
        print(flist[idx])
        print('non-indexables out of hits: ' + str(inFile_noneIndexed) + ' / ' + str(inFile_hits), flush=True)

    File_output.close()

    print('Indexed are: ' + str(totalHits-noneIndexed) + ' out of ' + str(totalHits) + ' number of hits')
    print('The indexing rate would be: ' + str(int(10000*(1-noneIndexed/totalHits))/100)+'%')
    print('outputfileFull-> ' + outputfileFull)
    print('streams are stiched together after ' + str(time.time()- time_time))
    print('script finished at ' + str(time.time()))
    